include ./Config

LIBDIR = $(INSTALLDIR)/lib

.PHONY: app clean again install uninstall install_all uninstall_all

all:
	$(MAKE) -C widgets tools
	$(MAKE) -C widgets lib
	$(MAKE) app

app:
	$(MAKE) -C app


clean:
	$(MAKE) clean -C app
	$(MAKE) clean -C widgets

again:
	$(MAKE) clean
	$(MAKE) all

install:
	$(MAKE) install -C app
	$(MAKE) install -C widgets

uninstall:
	rm /usr/bin/wildcad
	rm /usr/share/applications/wildcad.desktop
	rm /usr/share/mime/packages/wildcad.xml

install_all:
	$(MAKE) install
	$(MAKE) install -C widgets

uninstall_all:
	-$(MAKE) uninstall
	-$(MAKE) uninstall -C widgets
