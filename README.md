**WildCAD** is a very simple 2D drawing application (currently only) for X11 written in D. It features basic DXF im- and export, simple drawing elements like line, circle and arc, layers and blocks. WildCAD uses it's own widget library called WildWidgets, which is a subproject of WildCAD.

![](app/screenshot.png)

**Clone** this project (note the --recursive switch):
`git clone --recursive https://gitlab.com/elvin.eu/wildcad.git`

**Libraries:** X11, Cairo, Freetype2, Fontconfig, Cups

**Tools:** DMD, GNU Make

**Compile:** Cd into the WildCAD directory and call 'make'.

**Install:** As root, call 'make install_all'. This installs WildCad and the WildWidgets library in one go 

**Test:** there's a little script called `wildcad.sh` in the root directory
which can be used to start WildCAD without installing the WildWidgets
library.

**Email:** you can contact me at wildcad@elvin.eu

