import cairo;
import wcw;

import auxiliary;
import enums;
import painter;
import item;

class WDArbitrary : WDItem
{
private:
	WDItem[] _items;

public:
	this (WDItem[] i)
	{
		_items = i;
	}

	this (WDArbitrary a)
	{
		foreach (i; a._items)
		{
			WDItem clone = i.clone();
			if (clone) _items ~=  clone;
		}
	}

	override WDItem clone()
	{
		return new WDArbitrary (this);
	}

	override string toString () const
	{
		string str  = "arbitrary";
		foreach (i; _items)	str ~= " " ~ i.toString ~ " ;";
		return str;
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		foreach (i; _items) i.draw (p, matrix, density);
	}


	override bool snapsTo (WDItemSnap s) const
	{
		foreach (i; _items) if (i.snapsTo (s)) return true;
		return false;
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pos, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;
		return wdTuple(snap, dist);
	}

	override bool inWindow (DRect window, bool entirely) const
	{
		if (entirely) // all items must be entirely in window
		{
			foreach (i; _items) if (!i.inWindow (window, entirely)) return false;
			return true;
		}
		else // at least one item must touch the window
		{
			foreach (i; _items) if (i.inWindow (window, entirely)) return true;
			return false;
		}
	}

	override DRect extents () const
	{
		DRect e;

		foreach (i; _items) e = e.united (i.extents);
		return e;
	}

	override bool encloses (DPair p) const
	{
		foreach (i; _items) if (i.encloses(p)) return true;
		return false;

	}

//	DPair[] intersect (WDItem c) const {return [];}

//	DPair[] trimmingPoints() const {return [];}

	override void move(DPair d)
	{
		foreach (i; _items) i.move (d);
    }

	override void mirror (DPair a, DPair d)
	{
		foreach (i; _items) i.mirror (a, d);
    }

	override void rotate (DPair c, double a)
	{
		foreach (i; _items) i.rotate (c, a);
    }

	override void scale  (DPair c, double s)
	{
		foreach (i; _items) i.scale (c, s);
    }

//	bool offset  ( double d, DPair s) {return false;}

//	bool fit (DPair from, DPair to) {return false;}

//	bool checkForCircular (int ttl) {return false;}
}