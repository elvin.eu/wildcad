import std.conv;
import std.math;
import std.algorithm: min, max;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDArc : WDItem
{
private:
	DPair _center;
	double _radius, _startangle, _endangle;

	DPair _start, _end, _mid;

public:

	this (DPair center, double radius, double start, double end)
	{
		init (center, radius, start, end);
	}

	this (DPair start, DPair middle, DPair end)
	{
		auto mid1 = DPair (mid (start, middle));
		auto mid2 = DPair (mid (end, middle));

		auto per1 = DPair (perpendicular (start, middle) + mid1);
		auto per2 = DPair (perpendicular (end, middle) + mid2);

		auto center = intersect_xx (mid1, per1, mid2, per2);

		if (center.notReal)
		{
			// when all 3 points are on a line, we draw a 180° arc
			// This is a failsafe option

			this (mid (start, end), pdist (start, end)/2, 0.0, 180.0);
		}
		else
		{
			auto radius = pdist (start, center);

			DPair cs = vector(center, start);
			DPair cm = vector(center, middle);
			DPair ce = vector(center, end);

			double s = deg(atan2(cs.y, cs.x));
			double m = deg(atan2(cm.y, cm.x));
			double e = deg(atan2(ce.y, ce.x));

			if ((s < m && m < e) ||
				(s < m && e < s) ||
				(m < e && e < s))
			{
				this (center, radius, s, e);
			}
			else
			{
				this (center, radius, e, s);
			}
		}
	}

	this (const WDArc a)
	{
		_center     = a._center;
		_start      = a._start;
		_end        = a._end;
		_mid        = a._mid;
		_radius     = a._radius;
		_startangle = a._startangle;
		_endangle   = a._endangle;
	}

	void init (DPair center, double radius, double start, double end)
	{
		_center = center.isReal ? center : DPair(0.0, 0.0);
		_radius = radius.isReal ? radius : 1.0;

		if (start.isReal && end.isReal)
		{
			// normalize angles (reduce them to -180 .. +180)
			_startangle = normalize!180 (start);
			_endangle   = normalize!180 (end);
		}
		else
		{
			_startangle = 0.0;
			_endangle = 180.0;
		}

		auto rs = rad(_startangle);
		auto re = rad (_endangle);
		auto rm = normalize!PI ((rs+(re<rs?re+2*PI:re))/2);

		_start = center + radius*DPair(cos(rs), sin(rs));
		_end   = center + radius*DPair(cos(re), sin(re));
		_mid   = center + radius*DPair(cos(rm), sin(rm));
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDArc (this);
	}

	DPair center () const {return _center;}
	double radius() const {return _radius;}
	double startAngle () const {return _startangle;}
	double endAngle () const {return _endangle;}

	override string toString () const
	{
		return "arc @cc " ~ pretty(_center) ~
			" @fr " ~ pretty(_radius) ~
			" @a1 " ~ pretty(_startangle) ~
			" @a2 " ~ pretty(_endangle);
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		p.drawArc (_center, _radius, _startangle/180.0*PI, _endangle/180.0*PI, matrix);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST|WDItemSnap.INTERSECTION|WDItemSnap.END|WDItemSnap.MIDDLE|WDItemSnap.CENTER)) != 0);
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapmode & (WDItemSnap.NEAREST | WDItemSnap.INTERSECTION))
		{
			double a = arg (vector (_center, pointer));

			if (angle_between(a, rad(_startangle), rad(_endangle)))
			{
				snap = _center + vector (a, _radius);
				dist = pdist (snap, pointer);
			}
			else
			{
				double pd;
				if ((pd=pdist (_start, pointer)) < dist) {dist=pd; snap=_start;}
				if ((pd=pdist (_end,   pointer)) < dist) {dist=pd; snap=_end;}
			}
		}
		else
		{
			if (snapmode & WDItemSnap.CENTER)
			{
				snap = _center;
				dist = pdist (pointer, _center);
			}
			if (snapmode & WDItemSnap.END)
			{
				double pd;
				if ((pd=pdist(_start, pointer)) < dist) {dist = pd; snap = _start;}
				if ((pd=pdist(_end,   pointer)) < dist) {dist = pd; snap = _end;  }
			}
			if (snapmode & WDItemSnap.MIDDLE)
			{
				double pd;
				if ((pd=pdist(_mid, pointer)) < dist) {dist = pd; snap = _mid;}
			}
		}

		return wdTuple(snap, dist);
	}

	override bool inWindow (const DRect w, bool fi) const
	{
		double start = rad(_startangle);
		double end   = rad(_endangle);

		double ax = _center.x + cos(rad(_startangle))*_radius;
		double ay = _center.y + sin(rad(_startangle))*_radius;
		double ex = _center.x + cos(rad(_endangle))*_radius;
		double ey = _center.y + sin(rad(_endangle))*_radius;


		double r = max (ax, ex);
		double t = max (ay, ey);
		double l = min (ax, ex);
		double b = min (ay, ey);

		if (angle_between (0.5*PI, start, end)) t = _center.y+_radius;
		if (angle_between (    PI, start, end)) l = _center.x-_radius;
		if (angle_between (1.5*PI, start, end)) b = _center.y-_radius;
		if (angle_between (2.0*PI, start, end)) r = _center.x+_radius;

		if (w.contains (DPair(r, b)) && w.contains (DPair(l, t))) return true;

		if (fi == false)
		{
			auto lb = DPair (w.x0, w.y0);
			auto lt = DPair (w.x0, w.y1);
			auto rt = DPair (w.x1, w.y1);
			auto rb = DPair (w.x1, w.y0);

			auto i1 = intersect_lc (lt, rt, _center, _radius);
			auto i2 = intersect_lc (lb, rb, _center, _radius);
			auto i3 = intersect_lc (lt, lb, _center, _radius);
			auto i4 = intersect_lc (rt, rb, _center, _radius);

			bool is_on_arc (DPair p)
			{
				return (p.isReal) && (angle_between (atan2(p.y-_center.y, p.x-_center.x), start, end));
			}

			if (w.contains(i1[0]) && is_on_arc(i1[0])) return true;
			if (w.contains(i1[1]) && is_on_arc(i1[1])) return true;
			if (w.contains(i2[0]) && is_on_arc(i2[0])) return true;
			if (w.contains(i2[1]) && is_on_arc(i2[1])) return true;
			if (w.contains(i3[0]) && is_on_arc(i3[0])) return true;
			if (w.contains(i3[1]) && is_on_arc(i3[1])) return true;
			if (w.contains(i4[0]) && is_on_arc(i4[0])) return true;
			if (w.contains(i4[1]) && is_on_arc(i4[1])) return true;
		}
		return false;
	}

	override DRect extents () const
	{
		auto c0 = DPair (_center + DPair (_radius*cos(rad(_startangle)), _radius*sin(rad(_startangle))));
		auto c1 = DPair (_center + DPair (_radius*cos(rad(_endangle)), _radius*sin(rad(_endangle))));
		DRect e = DRect (c0, c1);

		if (angle_between (0, rad(_startangle), rad(_endangle)))
			e = e.united (DPair (_center + DPair(_radius ,0)));
		if (angle_between (PI_2, rad(_startangle), rad(_endangle)))
			e = e.united (DPair (_center + DPair(0, _radius)));
		if (angle_between (PI, rad(_startangle), rad(_endangle)))
			e = e.united (DPair (_center + DPair(-_radius ,0)));
		if (angle_between (-PI_2, rad(_startangle), rad(_endangle)))
			e = e.united (DPair (_center + DPair(0, -_radius)));

		return e;
	}

//	override bool encloses (DPair p) const
//	{
//	}


//	override DPair[] trimmingPoints() const


	override void move (DPair d)
	{
		init (center+d, radius, _startangle, _endangle);
	}

//	override void drag (DPair d)
//	{
//	}

	override void mirror (DPair a, DPair d)
	{
		init (2*xnear(a,d,_center)-_center,
			_radius,
			deg(2*arg(d))-_endangle,
			deg(2*arg(d))-_startangle);
	}

	override void rotate (DPair c, double a)
	{
		init (c + .rotate(vector(c,_center), rad(a)),
			_radius,
			a+_startangle,
			a+_endangle);
	}

	override void scale (DPair c, double s)
	{
		init ((_center-c)*s + c, _radius*s, _startangle, _endangle);
	}

	override bool offset (double d, DPair s)
	{
		DPair i = rnear (_center, vector (rad(_startangle)), s);
		DPair j = rnear (_center, vector (rad(_endangle)), s);

		if ((pdist (_center, i) < _radius) && (pdist (_center, j) < _radius))
		{
			if (_radius > d)
			{
				init (_center, _radius-d, _startangle, _endangle);
				return true;
			}
		}
		else if (angle_between (arg(vector(_center, s)), rad(_startangle), rad(_endangle)) &&
				 (pdist (_center, s) < _radius))
		{
			if (_radius > d)
			{
				init (_center, _radius-d, _startangle, _endangle);
				return true;
			}
		}
		else
		{
			if (_radius > -d)
			{
				init (_center, _radius+d, _startangle, _endangle);
				return true;
			}
		}
		return false;
	}


//	override bool fit (DPair from, DPair to)
//  override bool checkForCircular (int ttl) {return false;}
}
