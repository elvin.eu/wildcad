import std.conv;
import std.math;
import std.typecons;
import std.format;
import std.stdio;
import std.string;
import std.path;
import std.uni;

import cairo.matrix;

import wcw.auxiliary;
import wcw.enums;
import wcw.color;

import enums;
import item;
import settings;

alias DRect=Rect!double;
alias DPair=Pair!double;

alias cstring=const(char)[];
alias istring=immutable(char)[];
alias mstring=char[];

string pretty (DPair n)
{
	auto p = Settings.precision;
	return format ("(%.*s %.*s)", p, n.x, p, n.y);
}

string pretty (DRect n)
{
	auto p = Settings.precision;
	return format ("(%.*s %.*s %.*s %.*s)", p, n.x0, p, n.y0, p, n.width, p, n.height);
}

string pretty (double d)
{
	auto p = Settings.precision;
	return format ("%.*s", p, d);
}

string pretty (Color c)
{
	return format ("%06x", c.rgb);
}

string pretty (cstring t)
{
	return "\"" ~ t.idup ~ "\"";
}


string pretty (WDTextAlign a)
{
	string s;

	auto h = a&WDTextAlign.HMASK;
	auto v = a&WDTextAlign.VMASK;

	if      (h == WDTextAlign.RIGHT)   s ~= "r";
	else if (h == WDTextAlign.HCENTER) s ~= "h";
	else                               s ~= "l";

	if      (v == WDTextAlign.TOP)     s ~= "t";
	else if (v == WDTextAlign.BASE)    s ~= "s";
	else if (v == WDTextAlign.VCENTER) s ~= "v";
	else                               s ~= "b";

	return "[" ~ s ~ "]";
}


string pretty (WDDimensionType dt)
{
	if      (dt == WDDimensionType.HORIZONTAL) return "[h]";
	else if (dt == WDDimensionType.VERTICAL)   return "[v]";
	else if (dt == WDDimensionType.ALIGNED)    return "[a]";
	else                                       return "[]";
}

string pretty (WDPolylineType pt)
{
	if      (pt == WDPolylineType.OPEN)   return "[o]";
	else if (pt == WDPolylineType.CLOSED) return "[c]";
	else                                  return "[]";
}

string pretty (WDLayerAttribute l)
{
	string s;

	if (l & WDLayerAttribute.HIDDEN) s ~= "h";
	if (l & WDLayerAttribute.LOCKED) s ~= "f";

	return "[" ~ s ~ "]";
}

string pretty (WDItemSnap m)
{
	string s;

	if (m & WDItemSnap.CARDINAL)      s ~= "cl";
	if (m & WDItemSnap.CENTER)        s ~= "cr";
	if (m & WDItemSnap.END)           s ~= "ed";
	if (m & WDItemSnap.GRIP)          s ~= "gp";
	if (m & WDItemSnap.MIDDLE)        s ~= "me";
	if (m & WDItemSnap.NEAREST)       s ~= "nt";
	if (m & WDItemSnap.INTERSECTION)  s ~= "in";
	if (m & WDItemSnap.PERPENDICULAR) s ~= "pr";

	return "[" ~ s ~ "]";
}


/*******************************************************************************

indent only used for debugging messages

*******************************************************************************/

private static int _indent=0;
debug (inpr) string indent (int level=0)
{
	string str;
	if (level < 0) if (_indent >= 2) _indent += 2;
	foreach(i; 0.._indent) str ~= ' ';
	if (level < 0) if (_indent >= 2) _indent -= 2;
	return str;
}

/*******************************************************************************

some missing functions

*******************************************************************************/

cstring strip_quotes (cstring s)
{
	if ((s.length > 2) && s.startsWith("\"") && s.endsWith("\""))
	{
		return s[1..$-1];
	}
	else
	{
		return s;
	}
}

void append (Arg...)  (ref cstring str, Arg more)
{
	foreach (m; more) str ~= m.to!(const(char)[]);
}

void appendln (Arg...)  (ref cstring str, Arg more)
{
	foreach (m; more) str ~= m.to!(const(char)[]);
	str ~= "\n";
}

cstring nextWord (ref cstring line, ref size_t pos)
{
	if (pos >= line.length) return "";		// eol

	cstring word;
	char delimiter = ' ';
	int state = 0;

	foreach (i, dchar c; line[pos..$])
	{
		if (state == 0) // skip whitespaces and determine start delimiter
		{
			if (!std.uni.isWhite(c))
			{
				if      (c == '"') delimiter = '"';
				else if (c == '(') delimiter = ')';
				else if (c == '[') delimiter = ']';
				else if (c == '{') delimiter = '}';
				else               word ~= c;

				state = 1;
			}
		}
		else if (state == 1) // find delimiter
		{
			if (delimiter == ' ')
			{
				if (!std.uni.isWhite(c)) word ~= c;
				else                     state = 2;
			}
			else
			{
				if (c != delimiter) word ~= c;
				else                state = 2;
			}
		}
		else // state == 2
		{
			pos += i;
			return word;
		}
	}

	pos = line.length;
	return word;
}

cstring simplified (cstring s)
{
	if (s is null) return s;
	else           return s.strip.tr("\r\t\n\v\f", " ");
}

int int_round(T)(T t) {return lround(t).to!int;}
int uint_round(T)(T t) {return lround(t).to!uint;}

Color inverted (Color c) {return Color(c.argb ^ 0x00ffffff);}

double sqr (double d) {return d*d;}

bool circa (double l, double r, double p=1e-10)
{
	return (abs(r-l) < p);
}

bool circa (DPair l, DPair r, double p=1e-10)
{
	return (circa(l.x, r.x, p) && circa(l.y, r.y, p));
}

// find out, if value is a genuine real number, pair or rectangle
// can't uns isReal for DPair and DRect, because isReal from std.math
// interferes, therefore I use isReal ()

bool areReal (DPair[] pairs...)
{
	bool yes = true;
	foreach (pair; pairs) yes = yes && pair.isReal;
	return yes;
}

bool areReal (double[] nums...)
{
	bool yes = true;
	foreach (num; nums) yes = yes && num.isReal;
	return yes;
}

bool isReal(DPair p)
{
	return std.math.isFinite(p.x) &&
	       std.math.isFinite(p.y);
}

bool isReal(DRect r)
{
	return std.math.isFinite(r.x0) &&
	       std.math.isFinite(r.y0) &&
	       std.math.isFinite(r.width) &&
	       std.math.isFinite(r.height);
}

bool isReal (double d)
{
	return isFinite (d);
}

// and the inverse just to beautify some source code:

bool notReal (DPair p) {return !isReal(p);}
bool notReal (DRect r) {return !isReal(r);}
bool notReal (double d) {return !isReal(d);}

/*******************************************************************************

A simplified definition of Tuple. Has no problems with const like
Phobos' Tuple implementation

*******************************************************************************/

// 2 values
struct WDTuple (A,B)
{
	A first;
	B second;
}

WDTuple!(A,B) wdTuple(A,B)(A a, B b)
{
	return WDTuple!(A,B)(a,b);
}

// 3 values
struct WDTuple (A,B,C)
{
	A first;
	B second;
	C third;
}

WDTuple!(A,B,C) wdTuple(A,B,C)(A a, B b, C c)
{
	return WDTuple!(A,B,C)(a,b,c);
}

// 4 values
struct WDTuple (A,B,C,D)
{
	A first;
	B second;
	C third;
	D fourth;
}

WDTuple!(A,B,C,D) wdTuple(A,B,C,D)(A a, B b, C c, D d)
{
	return WDTuple!(A,B,C,D)(a,b,c,d);
}

/*******************************************************************************

Dpair and DRect related functions

*******************************************************************************/

bool contains (DRect r, DPair p)
{
	if (r.x0 <= p.x && p.x <= r.x1 &&
	    r.y0 <= p.y && p.y <= r.y1) return true;
	return false;
}

DRect united (DRect r, DPair p)
{
	return united (r, DRect (p, 0, 0));
}

DRect united (DRect q, DRect r)
{
	// min and max in std.algorithm return NaN, when one operand is NaN
	double min (double a, double b)
	{
		if      (a.isNaN) a = b;
		else if (b.isNaN) b = a;
		return a<b?a:b;
	}

	double max (double a, double b)
	{
		if      (a.isNaN) a = b;
		else if (b.isNaN) b = a;
		return a>b?a:b;
	}

	double xa, ya, xb, yb;
	xa = min (q.x0, r.x0);
	ya = min (q.y0, r.y0);
	xb = max (q.x1, r.x1);
	yb = max (q.y1, r.y1);
	return DRect (DPair(xa, ya), xb-xa, yb-ya);
}

/+
	bool intersects (T[4] r)
	{
		if (x >= (r[0]+r[2])) return false;
		if (y >= (r[1]+r[3])) return false;
		if (r[0] >= x+w) return false;
		if (r[1] >= y+h) return false;

		return true;
	}

	Rect!T intersected (T[4] r)
	{
		T x0, y0, x1, y1;

		x0 = max (x, r[0]);
		x1 = min (x+w, r[0]+r[2]);

		y0 = max (y, r[1]);
		y1 = min (y+h, r[1]+r[3]);

		T w = x1-x0;
		T h = y1-y0;

		if (w <= 0 || h <= 0) return Rect!T(0, 0, 0, 0);
		else                  return Rect!T(x0, y0, w, h);
	}
+/
/+
	Rect!T united (Rect!T r)
	{
		static if (isFloatingPoint!T)
		{
			T min (T a, T b)
			{
				T m;
				if      (a.isNaN) m = b;
				else if (b.isNaN) m = a;
				else              m = a<b?a:b;
				return m;
			}

			T max (T a, T b)
			{
				T m;
				if      (a.isNaN) m = b;
				else if (b.isNaN) m = a;
				else              m = a>b?a:b;
				return m;
			}
		}

		T xa, ya, xb, yb;

		xa = min (x, r.x);
		ya = min (y, r.y);

		xb = max (x+w, r.x1);
		yb = max (y+h, r.y1);

		return Rect!T(xa, ya, xb-xa, yb-ya);
	}

	Rect!T united (Pair!T p)
	{
		static if (isFloatingPoint!T)
		{
			T min (T a, T b)
			{
				T m;
				if      (a.isNaN) m = b;
				else if (b.isNaN) m = a;
				else              m = a<b?a:b;
				return m;
			}

			T max (T a, T b)
			{
				T m;
				if      (a.isNaN) m = b;
				else if (b.isNaN) m = a;
				else              m = a>b?a:b;
				return m;
			}
		}

		T xa, ya, xb, yb;

		xa = min (x, p.x);
		ya = min (y, p.y);

		xb = max (x+w, p.x);
		yb = max (y+h, p.y);

		return Rect!T(xa, ya, xb-xa, yb-ya);
	}
+/


/*******************************************************************************

geometry related functions

*******************************************************************************/

// clip a line from a to b into window with left/bottm v right/top w

bool clip_line (ref double a_x, ref double a_y, ref double b_x, ref double b_y,
                double v_x, double v_y, double w_x, double w_y)
{
	if (a_x<v_x && b_x<v_x) return false;
	if (a_x>w_x && b_x>w_x) return false;
	if (a_y<v_y && b_y<v_y) return false;
	if (a_y>w_y && b_y>w_y) return false;

	double ax = a_x;
	double ay = a_y;
	double bx = b_x;
	double by = b_y;

	double m = (by-ay) / (bx-ax);
	double n = (bx-ax) / (by-ay);

	if      (a_x < v_x) {a_y = (by-ay) / (bx-ax) * (v_x-ax) + ay; a_x = v_x;}
	else if (a_x > w_x) {a_y = (by-ay) / (bx-ax) * (w_x-ax) + ay; a_x = w_x;}

	if      (a_y < v_y) {a_x = (bx-ax) / (by-ay) * (v_y-ay) + ax; a_y = v_y;}
	else if (a_y > w_y) {a_x = (bx-ax) / (by-ay) * (w_y-ay) + ax; a_y = w_y;}

	if      (b_x < v_x) {b_y = (by-ay) / (bx-ax) * (v_x-ax) + ay; b_x = v_x;}
	else if (b_x > w_x) {b_y = (by-ay) / (bx-ax) * (w_x-ax) + ay; b_x = w_x;}

	if      (b_y < v_y) {b_x = (bx-ax) / (by-ay) * (v_y-ay) + ax; b_y = v_y;}
	else if (b_y > w_y) {b_x = (bx-ax) / (by-ay) * (w_y-ay) + ax; b_y = w_y;}

	if ((a_x<v_x) && (b_x<v_x)) return false;
	if ((a_x>w_x) && (b_x>w_x)) return false;

	return true;
}

// convert rad to degree and vice versa
double deg (double r) {return r*180.0/PI;}
double rad (double d) {return d*PI/180.0;}

// vector from a to b
DPair vector (DPair a, DPair b)
{
	return (b-a);
}

// normalized vector with angle a
DPair vector (double a) {return DPair (cos(a), sin(a));}

// vector with angle a and length l
DPair vector (double a, double l) {return vector (a)*l;}

// length of vector
double norm (DPair v) {return sqrt (sqr(v.x) + sqr(v.y));}

// argument (angle) of vector <v>
double arg (DPair v) {return atan2(v.y, v.x);}

// returns the normalized vector a (the unit vector)
DPair unit (DPair a) {return a/norm(a);}

// length of line from <a> to <b>
double pdist (DPair a, DPair b) {return norm(b-a);}

// return the nearest point on line <a, b> as seen from <p>
DPair  lnear (DPair a, DPair b, DPair p)
{
	DPair i = lsquare (a, b, p);
	if (i.isReal) return i;

	// else return minimum of distances <pa> and <pb>
	if (pdist (p, a) < pdist (p, b)) return a;
	else                             return b;

}

// return square point on line <a, b> as seen from <p>
DPair  lsquare (DPair a, DPair b, DPair p)
{
	// find intersection point of perpendicular from <p> to <ab>
	DPair i = intersect_xx (a, b, p, p+perpendicular (a, b));
	// if is <i> on line <ab>, return length <ip>
	if ((i.x>a.x && i.x<b.x)  ||  (i.x>b.x && i.x<a.x)  ||
		(i.y>a.y && i.y<b.y)  ||  (i.y>b.y && i.y<a.y))
	{
		return i;
	}
	else
	{
		return DPair();
	}
}

// return the nearest point on ray <a, v> as seen from <p>
DPair rnear (DPair a, DPair v, DPair p)
{
	DPair i = rsquare (a, v, p);
	if (i.isReal) return i;
	else          return a;
}

// return square point on ray <a, v> as seen from <p>
DPair rsquare (DPair a, DPair v, DPair p)
{
	// find intersection point of perpendicular from <p> to <a,v<
	DPair i = intersect_xx (a, a+v, p, p+perpendicular (v));
	// if is <i> on ray <ab>, return length <ip>
	if ((v.x>0) && (i.x>a.x)  ||  (v.x<0) && (i.x<a.x)  ||
		(v.y<0) && (i.y<a.y)  ||  (v.y>0) && (i.y>a.y))
	{
		return i;
	}
	else
	{
		return DPair();
	}
}

// return the nearest point on circle (c, r) as seen from <p>
DPair cnear (DPair c, double r, DPair p)
{
	double a = arg(vector(c, p));
	return (c + (r * DPair (cos(a), sin(a))));
}

// return the nearest point on circle (c, r) as seen from <p>
// and it's opposite point on the circle
DPair[2] csquare (DPair c, double r, DPair p)
{
	DPair a = cnear (c, r, p);
	DPair b = c + c - a;
	return [a, b];
}

// return the nearest point on xline <a, v> as seen from <p>
DPair xnear (DPair a, DPair v, DPair p)
{
	// find intersection point of perpendicular from <p> to >a,v<
	return  intersect_xx (a, a+v, p, p+perpendicular (v));
}

// point in the middle between a and b
DPair mid (DPair a, DPair b) {return DPair ((a.x+b.x)/2, (a.y+b.y)/2);}

// vector <v> rotated by 90 degree counterclockwise
DPair perpendicular (DPair v) {return DPair (-v.y, v.x);}

// vector <ab> rotated by 90 degree counterclockwise
DPair perpendicular (DPair a, DPair b)
{
	// simply create the vector from <ab> and rotate it 90°
	DPair v = vector (a, b);
	return DPair (-v.y, v.x);
}

// rotate p by angle a around 0,0
DPair rotate (DPair p, double a) {return  vector (arg(p)+a, sqrt(sqr(p.x)+sqr(p.y)));}


// reduces the angle to the interval [-180°, +180°]  resp. [-PI, +PI]
double normalize (double norm)(double angle)
{
	static assert (norm == PI || norm == 180.0, "angle must be 180 or PI");
	return remainder(angle, 2*norm);
}

bool  angle_between (double mid, double start, double end)
{
	double m = normalize!PI(mid);
	double s = normalize!PI(start);
	double e = normalize!PI(end);

	if      ((e > m) && (m > s)) return true;
	else if ((m > s) && (s > e)) return true;
	else if ((s > e) && (e > m)) return true;
	else                         return false;
}

DPair intersect_lx (DPair a1, DPair a2, DPair b1, DPair b2)
{
	auto p = intersect_xx (a1, a2, b1, b2);

	if (p.x > a1.x && p.x > a2.x) return DPair();
	if (p.x < a1.x && p.x < a2.x) return DPair();
	if (p.y > a1.y && p.y > a2.y) return DPair();
	if (p.y < a1.y && p.y < a2.y) return DPair();

	return p;
}

DPair intersect_ll (DPair a1, DPair a2, DPair b1, DPair b2)
{
	auto p = intersect_xx (a1, a2, b1, b2);

	if (p.x > a1.x && p.x > a2.x) return DPair();
	if (p.x < a1.x && p.x < a2.x) return DPair();
	if (p.y > a1.y && p.y > a2.y) return DPair();
	if (p.y < a1.y && p.y < a2.y) return DPair();

	if (p.x > b1.x && p.x > b2.x) return DPair();
	if (p.x < b1.x && p.x < b2.x) return DPair();
	if (p.y > b1.y && p.y > b2.y) return DPair();
	if (p.y < b1.y && p.y < b2.y) return DPair();

	return p;
}


DPair intersect_xx (DPair A, DPair A1, DPair B, DPair B1)
{

	DPair a = vector (A, A1);
	DPair b = vector (B, B1);

	if (a.x == 0.0) // a is vertical
	{
		if      (b.x == 0.0) return DPair (); 									// a and b vertical
		else if (b.y == 0.0) return DPair (A.x, B.y);							// a vertical, b horizontal
		else                 return DPair (A.x, B.y+(A.x-B.x)*(b.y/b.x));		// a vertical, b slanted
	}
	else if (a.y == 0.0) // a is horizontal
	{
		if      (b.y == 0.0) return DPair (); 									// a and b horizontal
		else if (b.x == 0.0) return DPair (B.x, A.y);							// a horizontal, b vertical
		else                 return DPair (B.x+(A.y-B.y)*(b.x/b.y), A.y);		// a horizontal, b slanted
	}
	else if (b.x == 0.0)														// b vertical, a slanted
	{
		return DPair (B.x, A.y+(B.x-A.x)*(a.y/a.x));
	}
	else if (b.y == 0.0)														// b horizontal, a slanted
	{
		return DPair (A.x+(B.y-A.y)*(a.x/a.y), B.y);
	}
	else
	{
		double ma = a.y/a.x;
		double mb = b.y/b.x;

		// both lines are parallel --> no intersection
		if (ma == mb) return DPair ();

		double x = (ma*A.x-mb*B.x+B.y-A.y)/(ma-mb);
		double y = ma*(x-A.x)+A.y;
		return DPair (x, y);
	}
}


DPair[2] intersect_lc (DPair a, DPair b, DPair c, double r)
{
	DPair p, q;

	// formula works for line: y=0*x
	// so, move circle by -a and rotate by -angle(line) and do calculations on transformed circle

	double an = arg(vector(a, b));   // this is the line's angle
	DPair  c0 = rotate (c-a, -an);   // this is the transformed circle's center

	// if transformed circles y coordinate is greater than it's radius, then no intersection
	if (abs(c0.y) > r) return [p, q];

	// calculate intersections
	double s = sqrt(sqr(r) - sqr(c0.y));
	auto i1 = DPair (c0.x-s, 0);
	auto i2 = DPair (c0.x+s, 0);

	// and transform back
	p = a + rotate (i1, an);
	q = a + rotate (i2, an);

	return [p, q];
}

DPair[2] intersect_cc (DPair c0, double r0, DPair c1, double r1)
{
	DPair p, q;

	// if distance of the circles is greater than r0 + r1, there's no intersection
	if ((pdist (c0, c1) > (r0 + r1)) || (c0 == c1)) return [p, q];

	// the formula works with center of fist circle in 0,0 and
	// center of second circle on x-axis
	// therefore we move both circles by -c0 and rotate then c1 by -arg(c0-c1)

	DPair i1, i2;

	double d = pdist (c0, c1); // x-value of 2nd center after transformation
	double a = arg (c1-c0);    // negative angle of transformation

	// transformation is simple: just put first circle on 0,0 and second circle on d,0
	// then the intersection points are:
	i1.x = ((sqr(r0) + sqr(d) - sqr(r1)) / (2*d));
	i1.y = (sqrt (sqr(r0) - sqr(i1.x)));

	// second point is just the first mirrored at the x axis
	i2.x = ( i1.x);
	i2.y = (-i1.y);

	// rotate and move back
	p = c0 + rotate (i1, a);
	q = c0 + rotate (i2, a);

	return [p, q];
}

/*******************************************************************************



*******************************************************************************/

WDFileType stringType (cstring filename)
{
    string ext = filename.extension.toLower.idup;

    if      (ext == ".wdf") return WDFileType.WDFTYPE;
    else if (ext == ".dxf") return WDFileType.DXFTYPE;
    else                    return WDFileType.NOTYPE;
}

WDFileType fileType (File file)
{
	file.seek(0);	// guarantee, that we look at the 1st line
    string header = file.readln();
	file.seek(0);	// 1st line might be valid date

    if (header.startsWith ("# WDF")) return WDFileType.WDFTYPE;
    else return stringType (file.name);
}

/*******************************************************************************

my own implementation of a doubly-linked list

*******************************************************************************/

class WDElement(T)
{
	T payload;
	alias payload this;

	WDElement!T next;
	WDElement!T prev;

	this (T p)
	{
		next = prev = this;
		payload = p;
	}

	this ()
	{
		next = prev = this;
	}

	void insertAfter (T t)
	{
		auto n = new WDElement!T (t);

		// and insert new element before this
		n.next = next;
		n.prev = this;
		next.prev = n;
		next = n;
	}

	void insertBefore (T t)
	{
		auto n = new WDElement!T (t);

		// and insert new element after this
		n.prev = prev;
		n.next = this;
		prev.next = n;
		prev = n;
	}


	T remove ()
	{
		next.prev = prev;
		prev.next = next;
		next = prev = this;
		return this.payload;
	}

	override string toString () const
	{
		return payload.to!string;
	}
}

class WDList(T)
{
	WDElement!(T) head;

	this ()
	{
		head = new WDElement!(T);
		head.next = head.prev = head;
	}

	~this ()
	{
		clear;
	}

	void clear ()
	{
		head.next.prev = head.prev;
		head.prev.next = head.next;
		head.next = head.prev = head;
	}

	bool empty () const {return head.next == head;}

	T front ()
	{
		assert (!empty);
		return head.next.payload;
	}

	T back ()
	{
		assert (!empty);
		return head.prev.payload;
	}

	T popFront()
	{
		assert (!empty);
		return head.next.remove ();
	}

	T popBack ()
	{
		assert (!empty);
		return head.prev.remove ();
	}

	void pushFront (T t) {head.insertAfter  (t);}
	void pushBack  (T t) {head.insertBefore (t);}

	void opOpAssign(string op)(T t) if (op=="~") {pushBack (t);}


	// the [] operator, for foreach
	WDRange!T opSlice () inout
	{
		return WDRange!T(this);
	}

	size_t count ()
	{
		size_t c;
		foreach (e; this) ++c;
		return c;
	}

	void splice (WDList!T list)
	{
		if (list.empty) return;

		auto head1 = head;
		auto head2 = list.head;
		auto prev1 = head1.prev;
		auto prev2 = head2.prev;
		auto next2 = head2.next;

		prev1.next = next2;
		next2.prev = prev1;
		prev2.next = head1;
		head1.prev = prev2;

		head2.next = head2.prev = head2;
	}

	override string toString ()
	{
		return format ("%s", cast (const) this[]);
	}
}

struct WDRange(T)
{
	const WDList!T list;

	WDElement!T first;
	WDElement!T last;

	this (const WDList!T h)
	{
		list = h;
		first = cast (WDElement!T) list.head.next;
		last  = cast (WDElement!T) list.head.prev;
	}

	bool empty () const
	{
		return (first == list.head) ||
		       (last  == list.head);
	}

	inout (WDElement!T) front () inout
	{
		assert (!empty);
		return first;
	}

	inout (WDElement!T) back () inout
	{
		assert (!empty);
		return last;
	}

	void popFront ()
	{
		assert (!empty);
		first = first.next;
	}

	void popBack ()
	{
		assert (!empty);
		last = last.prev;
	}
/*
	void remove ()
	{
		first.prev.next = last.next;
		last.next.prev = first.prev;
	}
*/
}
