import std.stdio;

import auxiliary;
import layer;
import layertable;
import namedobject;
import enums;

import wcw.auxiliary;

class WDBlockTable : WDNamedList!WDBlock
{
	mixin nt!WDBlock;

	WDBlock[] _list;
	cstring _defaultname;		// default name for new NamedObjects

	this ()
	{
		nt_init ("new blocktable");
	}

	cstring info ()
	{
		cstring s = "Blocks: ";
		foreach (block;  _list) s ~= "  " ~ block.name;
		return s;
	}

	void updateLayer  (cstring name, const WDLayer from)
	{
		foreach (block; _list)
		{
			block.updateLayer (name, from);
		}
	}

	bool checkForCircular ()
	{
		bool rc = false;

		foreach (block; _list)
		{
			if (block.checkForCircular(20))
			{
				stderr.writeln ("circularity in block " ~ block.name);

				block.erase();
				rc = true;
			}
		}
		return rc;
	}
}
