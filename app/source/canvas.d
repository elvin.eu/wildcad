// canvas.d

import std.math;
import std.algorithm;
import std.signals;
import std.conv;
import std.path;
import std.stdio;
import std.range;

import cairo;
import wcw;

import block;
import enums;
import auxiliary;
import layer;
import layertable;
import settings;
import view;
import item;
import painter;
import wdfreader;
import wdfwriter;
import dxfreader;
import dxfwriter;
import reference;
import intersection;

import cups;

class WDCanvas : Widget
{
private:
	cstring _filename;         // current path
	cstring _canvasname;

	// drawing settings overwrite global default settings

	double _textSize;
	double _dimTextSize;
	int _dimLengthPrecision;
	int _dimAnglePrecision;

	WDLayerTable _layertable;  // table of drawing layers
	WDBlockTable _blocktable;  // table of block templates (a list of layertables)
	WDViewTable  _viewtable;   // table of views

	WDLayerTable _activeblock=null; // points either to a block in _blocktable or to _layertable

	WDLayer _temporarylayer;   // used for interactive drawing


	DRect  _cwindow; // Backup for canvas view setting while a block is active

	WDItem _pickedItem;  // used for trimming
	DPair  _pickedThere; // and extending

	CairoMatrix _matrix;    // current transformation matrix (from real world to screen pixels)

	IPair _mousepos;    				// Position of mouse in widget in widget coordinates
	DPair _worldpos = DPair(0.0, 0.0);	// Position of mouse in drawing in real world coordinates
	DPair _worldfrom;					// for drawing selection windows
	DPair _basepoint;					// for square snapping

	WDMouseMode _mousehint = WDMouseMode.NONE;	// this is the default hint
	WDMouseMode _mousemode = WDMouseMode.NONE;	// this is the default hint

	int  _inhibited;      // if >0, drawing is cancelled
	bool _modified;       // true if drawing is modified but not saved
	bool _gripmode;       // grip mode for selected items
	bool _orthogonal;     // flag not used by WDCanvas but needed by WDInterpreter
	bool _pageshidden;    // true, if pages are hidden on screen

	bool  _snapon = true; // snap enabled?

	DRect _czoom;

	char[] _undoBuffer;

	enum MAXCOPYCOUNT = 1000;		// this is an arbitrary value, could also be 100 or 1000000

public:
	mixin Signal!(DPair) mouseMoved;
	mixin Signal!(DPair) mousePressed;
	mixin Signal!(DPair) gridChanged;

	mixin Signal!()rightButtonPressed;
//	mixin Signal!()middleButtonPressed;
//	mixin Signal!()leftButtonPressed;

	mixin Signal!(const(char)[]) fileNameChanged;
	mixin Signal!(bool) drawingModified;
	mixin Signal!(WDItemSnap) snapModeChanged;
	mixin Signal!(bool) gripModeChanged;
	mixin Signal!(bool) orthogonalChanged;

	mixin Signal!() layerTableChanged;
	mixin Signal!() viewTableChanged;
	mixin Signal!() blockTableChanged;

	mixin Signal!(bool) changingTables;
	mixin Signal!(cstring)recentlyOpened;
	mixin Signal!() userWantsToQuit;

	this (Widget parent, int[] i...)
	{
		_viewtable = new WDViewTable;
		_blocktable = new WDBlockTable;
		_layertable = new WDLayerTable (_canvasname);
		_temporarylayer = new WDLayer ("__temporary");

		super (parent, i);
		setMinSize (IPair (100, 100));
		setNomSize (IPair (400, 400));
		setMaxSize (IPair (-1, -1));
		_canvasname = "CANVAS";
		init();
	}

	void init ()
	{
		activeLayer();	// create a new Layer and set it as active;

		_textSize = Settings.textSize;

		_dimTextSize = Settings.dimTextSize;
		_dimLengthPrecision = Settings.dimLengthPrecision;
		_dimAnglePrecision = Settings.dimAnglePrecision;

		_layertable.erase ();
		_viewtable.erase ();
		_blocktable.erase ();

		setFileName ("");
		setModified (false);
	}

	double textSize () const {return _textSize;}
	void setTextSize (double s) {_textSize = s;}

	double dimensionTextSize () const {return _dimTextSize;}
	void setDimensionTextSize (double s) {_dimTextSize = s;}

	int dimensionLengthPrecision () const {return _dimLengthPrecision;}
	void setDimensionLengthPrecision (int p) {_dimLengthPrecision = p;}

	int dimensionAnglePrecision () const {return _dimAnglePrecision;}
	void setDimensionAnglePrecision (int p) {_dimAnglePrecision = p;}


    WDLayerTable layerTable() {return _layertable;}
    WDBlockTable blockTable() {return _blocktable;}
    WDViewTable  viewTable()  {return _viewtable;}

	WDLayer activeLayer()
	{
		return activeBlock().activeLayer();
	}

	WDLayerTable activeBlock()
	{
		if (_blocktable.find (_activeblock)) return _activeblock;
		// else
		_activeblock = null;
		return _layertable;
	}

	WDView activeView()
	{
		return _viewtable.activeView();
	}

	void inhibit (bool i)
	{
		if (i)
		{
			++_inhibited;
		}
		else
		{
			// make sure, _inhibited never becomes negative
			if (_inhibited  > 0) --_inhibited;
			// redraw
			if (_inhibited == 0) draw();
		}
	}

	bool modified () {return _modified;}

	void setModified (bool m = true) {drawingModified.emit (_modified = m);}

	bool gripMode () {return _gripmode;}

	void setGripMode (bool b)
	{
		if (_gripmode == b) return;

		bool not_empty = false;

		_gripmode = b;
		// go through all layers:
		foreach (layer; activeBlock.list)
		{
			if (!layer.selected.empty()) not_empty = true;

			foreach (i; layer.selected) i.clearGrips();
		}

		if (not_empty) draw();
		gripModeChanged.emit (b);
	}

	bool orthogonal() const  {return _orthogonal;}

	void setOrthogonal (bool b)
	{
		if (_orthogonal == b) return;

		_orthogonal = b;
		orthogonalChanged.emit (b);
	}

	void setBasepoint (DPair b) {_basepoint = b;}

	DRect extents (bool selected=false)
	{
		return activeBlock().extents(selected);
	}

	void setMouseHint (WDMouseMode m) {_mousehint = m;}

	WDItem pickedItem () {return  _pickedItem;}

//	WDLayer pageLayer ()
//	{
//		WDLayer &page = activeBlock().pageLayer ();
//		// we don't know whether page is newly created,
//		// so if pages are hidden, hide it
//		if (_pageshidden) page.hide (true);
//		return page;
//	}

	void drawOrigin (Painter p)
	{
		if (_inhibited) return;

		int r = Settings.originSize/2;
		int d = r*2;

		if (r == 0) return; // origin visible?

		auto view = activeView();

		double tx, ty;
		_matrix.transform_point (view.origin.x, view.origin.y, tx, ty);

		p.setColor (Settings.originColor);
		p.drawArc (tx, ty, r, PI/2, PI*2);
		p.drawLine (tx-d, ty, tx+d, ty);
		p.drawLine (tx, ty-d, tx, ty+d);
	}

	void drawGrid (Painter p)
	{
		if (_inhibited) return;

		WDView view = activeView();
		DPair raster = view.grid();

		// calculate the gridsize in device coordinates
		// (needs a matrix without translation)
		auto m0 = CairoMatrix (_matrix.xx, _matrix.yx, _matrix.xy, _matrix.yy, 0, 0);

		double rx, ry;
		m0.transform_point (raster.x, raster.y, rx, ry);
		rx = abs(rx);
		ry = abs(ry);

		// draw grid, but only, if it's not too small to print
		if ((rx >= 5.0) && (ry >= 5.0))
		{
			auto vp = IRect (IPair(0, 0), size.x, size.y);
			CairoMatrix invers = _matrix.inverted;

			double l, r, t, b;
			invers.transform_point (vp.x0, vp.y1, l, b);
			invers.transform_point (vp.x1, vp.y0, r, t);

			// now, l, b, r and t are the left bottom and right top
			// corners of the canvas in world coordinates

			// calculate the lower left and upper right points of the grid
			// that are not yet inside the canvas

			double modx = fmod (view.origin().x, raster.x);
			double mody = fmod (view.origin().y, raster.y);

			l -= fmod (l,raster.x) + (raster.x-modx);
			r -= fmod (r,raster.x) - modx;
			t -= fmod (t,raster.y) - mody;
			b -= fmod (b,raster.y) + (raster.y- mody);

			if ((rx >= 40) && (ry >= 40))
			{
				p.setColor (Settings.gridColor.light);

				for (double i=l; i<=r; i+=raster.x) for (double j=b; j<=t; j+=raster.y)
				{
					double tx, ty;
					_matrix.transform_point (i, j, tx, ty);
					p.drawLine (tx-1, ty, tx+1, ty);
					p.drawLine (tx, ty-1, tx, ty+1);
				}
			}
			else
			{
				p.setColor (Settings.gridColor);

				// draw dots
				for (double i=l; i<=r; i+=raster.x) for (double j=b; j<=t; j+=raster.y)
				{
					p.drawPoint (DPair(i,j), _matrix);
				}
			}

		}
	}

	WDSnapType _sv, _sn; // used by drawSnap to determine if aperture has to
	double     _sx, _sy; // be drawn and in which color

	void drawSnap (Painter p)
	{
		if (_inhibited) return;

		int apr = Settings.apertureSize/2;

		p.setOperatorXOR();

		// first, delete aperture (if visible)
		if (_sv != WDSnapType.NOSNAP)
		{
			if      (_sv == WDSnapType.ITEM) p.setColor (Settings.itemSnapColor);
			else if (_sv == WDSnapType.GRID) p.setColor (Settings.gridSnapColor);
			else                             p.setColor (Settings.freeSnapColor);

			p.drawCircle(_sx, _sy, apr);
		}
		// then, draw aperture
		if (_sn != WDSnapType.NOSNAP)
		{
			if      (_sn == WDSnapType.ITEM) p.setColor (Settings.itemSnapColor);
			else if (_sn == WDSnapType.GRID) p.setColor (Settings.gridSnapColor);
			else                             p.setColor (Settings.freeSnapColor);

			// convert world mouse coordinate into device coordinates
			double tx, ty;
			_matrix.transform_point (_worldpos.x, _worldpos.y, tx, ty);
			p.drawCircle (tx, ty, apr);
			_sx = tx; _sy = ty;
		}
		_sv = _sn;
	}

	void drawAllLayers (Painter p)
	{
		if (_inhibited) return;

		auto active = activeBlock();
		active.drawAllItems    (p, _matrix, 1.0);
		active.drawAllSelected (p, _matrix, Settings.selectDensity);
		if (gripMode) active.drawAllGrips (p, _matrix, 1.0);
	}

	void drawTemporary (Painter p)
	{
		if (_inhibited) return;

		p.setOperatorXOR();
		_temporarylayer.setColor (Settings.ribbonColor);
		_temporarylayer.drawAllItems(p, _matrix);
	}

	void draw (bool clear = true)
	{
		if (_inhibited) return;

		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		if (clear)
		{
			painter.setColor (Settings.canvasColor);
			with (rectangle) painter.fillRectangle (0, 0, width, height);
		}

		drawOrigin (painter);
		drawGrid (painter);
		drawAllLayers (painter);
		drawTemporary (painter);

		_sv = WDSnapType.NOSNAP;
		drawSnap (painter);
	}

	override void paint (CairoContext context)
	{
		draw (true);
	}

	void addItemToLayer (WDItem item, WDLayer l)
	{
		l.addItem(item);
		setModified();

		if (_inhibited) return;

		auto p = new Painter(this);
		scope (exit) destroy (p);
		l.drawItem (p, _matrix, item);
	}

	void addItemToActive (WDItem item)
	{
		addItemToLayer (item, activeLayer());
	}

//	void addItemToExtra (AutoPtr<WDItem>& item)
//	{
//		WDLayer page = pageLayer();
//		hidePages (false);
//		addItemToLayer (item, page);
//	}

//	bool addPage (DPair origin, DPair size, double scale)
//	{
//		WDItem *page = new WDPage (origin, size, scale);
//		AutoPtr<WDItem> aptr (page);
//		addToExtra (aptr);
//		return true;
//	}


	void addItemToTemporary(WDItem item)
	{
		auto p = new Painter (this);
		scope (exit) destroy (p);
		p.setOperatorXOR;
		_temporarylayer.addItem (item);
		_temporarylayer.setColor (Settings.ribbonColor);
		_temporarylayer.drawItem (p, _matrix, item);
	}

	void eraseTemporary ()
	{
		auto p = new Painter(this);
		scope (exit) destroy (p);
		drawTemporary (p);
		_temporarylayer.clearItems ();
	}

	double realWorldApertureRadius ()
	{
		return (Settings.apertureSize/_matrix.xx)/2;
	}

	void zoomBy (double scale, DPair pointer=DPair())
	{
		auto v = activeView.window;
		alias w = _worldpos;

//		if (pointer.notReal) pointer = DPair (v.x + v.w/2, v.h + v.h/2);

		auto e = DRect (DPair(w.x-((w.x-v.x0)/scale), w.y-((w.y-v.y0)/scale)), v.width/scale, v.height/scale);

		activeView.setWindow (e);
		recalcMatrix;
		snap ();
		draw (true);
	}

	void zoomAll ()
	{
		// zoom only to selected items:
		bool selected = false;
		foreach (layer; activeBlock().list) if (!layer.selected.empty) selected = true;

		auto e = extents(selected);

		// make e 10% bigger
		e.x0 -= e.width*0.05;
		e.y0 -= e.height*0.05;
		e.width *= 1.1;
		e.height *= 1.1;

		activeView.setWindow (e);
		recalcMatrix;
		snap ();
		draw (true);
	}

	void zoomRect (DRect w)
	{
		assert (w.notReal == false);

		activeView.setWindow (w);
		recalcMatrix;
		snap ();
		draw (true);
	}

	void screenPan (double x, double y) {screenPan (DPair(x, y));}
	void screenPan (DPair p)
	{
		assert (p.notReal == false);

		WDView v = activeView;
		with (v.window) v.setWindow (DRect (DPair(x0+p.x*width, y0+p.y*height), width, height));
		recalcMatrix;
		snap ();
		draw (true);
	}

	void screenPan (DPair s, DPair e)
	{
		assert (s.notReal == false);
		assert (e.notReal == false);

		WDView v = activeView;

		with (v.window) v.setWindow (DRect (DPair(x0+s.x-e.x, y0+s.y-e.y), width, height));
		recalcMatrix;
		snap ();
		draw (true);
	}

	void recalcMatrix ()
	{
		auto w = activeView.window;

		double sc = min (size.width/w.width, size.height/w.height);
		double tx = -w.x0;
		double ty = -w.y0;

		_matrix = CairoMatrix();

		// scale view.window to widget size and move to lower left corner
		_matrix.translate (0, size.height);
		_matrix.scale (sc, -sc);
		_matrix.translate (tx, ty);

		// centering the window in widget
		double dx, dy;
		_matrix.transform_distance (w.width, w.height, tx, ty);
		_matrix.translate ((size.width-tx)/sc/2, (size.height+ty)/sc/2);
	}


	override void transformHandler (in TransformEvent event)
	{
		recalcMatrix;
	}

	override void buttonPressHandler (in ButtonPressEvent event)
	{
		// copy mousehint to mousemode except when middle button is pressed!
		_mousemode = _mousehint;
		_worldfrom = _worldpos;

		if (event.button == MouseButton.LEFT)
		{
			if (_mousehint == WDMouseMode.NONE ||
			    _mousehint == WDMouseMode.SELECT ||
				(event.modifier & ModifierKey.SHIFT))
			{
				_mousemode = WDMouseMode.SELECT;

				drawSelectWindow(_worldfrom, _worldpos);
				// dynamic resizing of the selection window will be done by
				// mouseMoveHandler and the items will be selected by
				// buttonReleaseHandler
			}
			else if (_mousehint == WDMouseMode.COORD)
			{
				_mousemode = WDMouseMode.COORD;
				mousePressed.emit (_worldpos - activeView ().origin());
			}
			else if (_mousehint == WDMouseMode.PICK)
			{
				WDItem nearest;
				WDTuple!(WDElement!WDItem, double) n1, n2, n0;
				n0 = WDTuple!(WDElement!WDItem, double)(new WDElement!WDItem(null), double.infinity);

				// check all layers for the nearest item that supports snapmode <snap>
				foreach  (layer; activeBlock.list)
				{
					n1 =  layer.items.findNearestItem (_worldpos, realWorldApertureRadius(), WDItemSnap.INTERSECTION);
					n2 =  layer.selected.findNearestItem (_worldpos, realWorldApertureRadius(), WDItemSnap.INTERSECTION);

					if (n1.second < n0.second) n0 = n1;
					if (n2.second < n0.second) n0 = n2;
				}

				if (n0.second <=  realWorldApertureRadius()) nearest = n0.first;

				if (nearest !is null)
				{
					// needed for trimming
					_pickedItem = nearest;
					_pickedThere = _worldpos;
					_mousemode = WDMouseMode.PICK;
					snap (WDSnapType.FREEHAND);
					// emit position, so that the interpreter starts another parsing cycle
					mouseMoved.emit (_worldpos - activeView.origin);
				}
			}
		}
		else if (event.button == MouseButton.MIDDLE)
		{
			_mousemode = WDMouseMode.PAN;
			whereami ();
		}
		else if (event.button == MouseButton.RIGHT)
		{
			rightButtonPressed.emit();
		}
		else if (event.button == MouseButton.SCROLL_FORWARD)
		{
			zoomBy (2, _worldpos);
		}
		else if (event.button == MouseButton.SCROLL_REVERSE)
		{
			zoomBy (0.5, _worldpos);
		}

	}

	override void buttonReleaseHandler (in ButtonReleaseEvent event)
	{
		if (_mousemode == WDMouseMode.SELECT)
		{
			drawSelectWindow(_worldfrom, _worldpos);

			// mouse still more or less where it was when mousebutton was pressed?
			if ((abs(_worldfrom.x-_worldpos.x) <= 1) &&
				(abs(_worldfrom.y-_worldpos.y) <= 1))
			{
				if (event.modifier & ModifierKey.SHIFT)
				{
					toggleGrip (_worldpos);
				}
				else if (event.modifier & ModifierKey.CONTROL)
				{
					deselectItem (_worldpos);
				}
				else
				{
					selectItem (_worldpos, gripMode());
				}
			}
			else  // windowed selection
			{
				bool inc = _worldpos.x > _worldfrom.x;
				DPair size = _worldpos-_worldfrom;

				DRect window = DRect(_worldfrom, size.x, size.y);

				if (event.modifier & ModifierKey.SHIFT)
				{
					toggleGrips (window);
				}
				else if (event.modifier & ModifierKey.CONTROL)
				{
					deselectItems (window, inc);
				}
				else
				{
					selectItems (window, inc, gripMode());
				}
			}

			// show snap point to the world
			auto p = new Painter(this);
			scope (exit) destroy (p);
			drawSnap (p);
		}

		_mousehint = _mousemode = WDMouseMode.NONE;
	}

	override void mouseMoveHandler (in MouseMoveEvent event)
	{
		if (_inhibited) return;

		// save mouse coordinates
		_mousepos = mapFromWindow(event.pointer);
		auto worldto = _worldpos;

		// set _worldpos
		if (_snapon) snap (WDSnapType.ITEM);
		else         snap (WDSnapType.FREEHAND);

		// Coord::parse controls _snapon, so Item snapping is
		// used, when parsing coordinates.
		// snap emits the MouseMoved signal and calculates _worldpos

		if (_mousemode == WDMouseMode.PAN)
		{
			snap (WDSnapType.FREEHAND);
			screenPan (_worldfrom, _worldpos);
			_worldfrom = _worldpos;
		}
		else if (_mousemode == WDMouseMode.SELECT)
		{
			drawSelectWindow(_worldfrom, worldto);		// delete old window
			drawSelectWindow(_worldfrom, _worldpos);	// draw new window
		}
		else
		{
			// show snap point to the world
			auto p = new Painter(this);
			drawSnap (p);
			scope (exit) destroy (p);
		}
	}

	DPair _oldpos = DPair(0.0, 0.0);

	void snap() {snap (_sn);}

	void snap (WDSnapType st)
	{
		// snap is called mainly by mouseMoveHandler()
		// SnapType st is one of {NO, GRID, ITEM, FREEHAND}

		// translate mouse coordinates to next snap point
		// that could be something like the end of a line or the center
		// of a circle (if the corresponding snap mode is active), if the line
		// or circle is near the mouse pointer (nearer than the aperture) or
		// simply the next point on the current grid

		WDView v = activeView ();
		CairoMatrix invers = _matrix.inverted;
		double wx, wy;
		invers.transform_point (_mousepos.x, _mousepos.y, wx, wy);
		_worldpos = DPair(wx, wy);

		if (st == WDSnapType.FREEHAND)
		{
			// no snap at all
			_sn = st;
		}
		else if (st == WDSnapType.ITEM)
		{
			DPair s;
			auto snapmode = gripMode ? v.snapMode() | WDItemSnap.GRIP : v.snapMode();

			// try to snap onto the nearest item that supports v.snapMode
			if (_snapon) s = activeBlock().itemSnap(snapmode, realWorldApertureRadius(),
			                                        _worldpos, _basepoint);

			if (notReal(s.x) || notReal(s.y))
			{
				// no Item found, fallback to GRIDSNAP
				st = WDSnapType.GRID;
			}
			else
			{
				_worldpos = s;
				_sn = WDSnapType.ITEM;
			}
		}

		// either st == GRIDSNAP or ITEMSNAP failed
		if (st == WDSnapType.GRID)
		{
			_worldpos.x = (rint((_worldpos.x-v.origin().x)/v.grid().x)
							* v.grid().x + v.origin().x);
			_worldpos.y = (rint((_worldpos.y-v.origin().y)/v.grid().y)
							* v.grid().y + v.origin().y);
			_sn = WDSnapType.GRID;
		}
		// now emit all three coordinates to the world

		DPair newpos = _worldpos - v.origin;

		if (newpos != _oldpos)
		{
			_oldpos = newpos;
			mouseMoved.emit (newpos);    // relative to origin
		}
	}

	void drawSelectWindow (DPair from, DPair to)
	{
		double x1, x2, y1, y2;
		_matrix.transform_point (from.x, from.y, x1, y1);
		_matrix.transform_point (to.x,   to.y,   x2, y2);

		auto p = new Painter (this);
		scope (exit) destroy (p);

		p.setOperatorXOR;

		if (x1 < x2) p.setColor (Settings.rightPickColor);
		else         p.setColor (Settings.leftPickColor);

		p.drawLine (x1, y1, x2, y1);
		p.drawLine (x2, y1, x2, y2);
		p.drawLine (x2, y2, x1, y2);
		p.drawLine (x1, y2, x1, y1);
	}

	void setOrigin (DPair o)
	{
		activeView().setOrigin (o);
		setModified();
		draw (true);
	}

	DPair grid () {return activeView.grid;}

	void setGrid (DPair g)
	{
		activeView().setGrid (g);
		gridChanged.emit (g);
		draw (true);
	}


//	void hidePages (bool hide)
//	{
//		bool donotemit = (_pageshidden == hide);
//		bool hideblock = hide;

//		_pageshidden = hide;

//		if (&layerTable() == &activeBlock()) hideblock = true;

//		layerTable().hideLayer (WDLayerTable::__PageLayer__, hide);
//		_blocktable.hideEveryLayer (WDLayerTable::__PageLayer__, hideblock);

//		draw(true);
//		if (!donotemit) emit hidePagesChanged  (hide);
//	}

	void addSnapMode (WDItemSnap s, WDFlagOp op = WDFlagOp.ADD)
	{
		WDView v = activeView;
		WDItemSnap vsnap = v.snapMode();

		if      (op == WDFlagOp.ADD) vsnap |=  s;
		else if (op == WDFlagOp.DEL) vsnap &= ~s;
		else                       vsnap  =  s;

		if (vsnap == v.snapMode()) return;

		v.setSnapMode (vsnap);
		snapModeChanged.emit (vsnap);
	}

	void setSnapMode (WDItemSnap sm)
	{
		activeView.setSnapMode (sm);
		snapModeChanged.emit (sm);
	}

	void toggleSnapMode (WDItemSnap s)
	{
		setSnapMode (activeView.snapMode() ^ s);
	}

	void snapOn (bool on) {_snapon = on;} // en/disables snapping

	WDItemSnap snapMode ()
	{
		return activeView.snapMode ();
	}

	cstring listItems ()
	{
		cstring list;
		WDLayerTable block = activeBlock;

		if (layerTable == activeBlock) list ~= "Items on Canvas:\n";
		else                           list ~= "Items on Block \"" ~ block.name ~ "\"\n";

		// go through all layers:
		foreach (layer; block.list)
		{
			if (!layer.empty)
			{
				list ~= "Layer \"" ~ layer.name ~ "\":\n";
				foreach (item; layer.items)    list ~= "  " ~ item.info ~ "\n";
				foreach (item; layer.selected) list ~= "s " ~ item.info ~ "\n";
			}
		}
		return list;
	}

	cstring info ()
	{
		cstring i;
		i ~= "Filename: " ~ _filename ~ "\n";
		i ~= "Extents:   " ~ extents.to!string ~ "\n";
		i ~= "Number of Items: " ~ WDItem.count.to!string ~ "\n";
		return i;
	}


//	WDView createView (cstring name)
//	{
//		if (_viewtable.find (name)) return null;

//		WDView view = _viewtable.create (name);
//		if (view)
//		{
//			view.update (activeView);
//			viewTableChanged.emit ();
//		}
//		return view;
//	}

//	bool activateView (cstring n)
//	{
//		if (_viewtable.activate (n) == false) return false;

//		recalcMatrix ();
//		draw(true);
//		viewTableChanged.emit ();
//		return true;
//	}

//	bool updateView (cstring name)
//	{
//		if (_viewtable.update (name, activeView) == false) return false;

//		viewTableChanged.emit ();
//		return true;
//	}

//	bool renameView (cstring from, cstring to)
//	{
//		if (_viewtable.rename (from, to) is null) return false;

//		viewTableChanged.emit ();
//		return true;
//	}

//	bool removeView (cstring n)
//	{
////		if (_viewtable.remove (n) == false) return false;

//		viewTableChanged.emit ();
//		return true;
//	}

	// activate layer in current active block, which might be either
	// the drawing or a block
	bool activateLayer (cstring n)
	{
		if (activeBlock().activateLayer (n))
		{
			layerTableChanged.emit ();
			return true;
		}

		// did't work?
		if (_layertable == activeBlock)	// is canvas now current?
		{
			return false; // layer doesn't exist
		}
		else // block active and missing a layer
		{
			WDLayer cl = _layertable.find (n);
			if (cl is null) return false;  // layer doesn't exist

			WDLayer bl = activeBlock().create (n);
			bl.setColor (cl.color());

			activeBlock().activateLayer (n);

			layerTableChanged.emit ();
			return true;
		}
	}


	WDLayer findLayer (cstring name) {return layerTable.find (name);}

	/// create a new layer if it doesn't exist yet
	WDLayer createLayer (cstring name)
	{
		if(findLayer (name)) return null;

		// create and activate a new layer on the canvas
		auto layer = layerTable.create (name);
		activateLayer (layer.name());
		return layer;
	}

	bool removeLayer (cstring name)
	{
		// remove layer from active block
		if (activeBlock().remove (name))
		{
			setModified();
			draw(true);
			layerTableChanged.emit ();
			return true;
		}
		return false;
	}

//	bool updateLayer (cstring name, WDLayerAttribute attr, Color color)
//	{
//		// set parameters of a layer on the active block
//		// copy those parameters to all other instances of that layer
//		WDLayer layer;
//		if ((layer = layerTable.updateLayer (name, attr, color)) !is null)
//		{
//			setModified();
//			_blocktable.updateLayer (layer.name, layer);
//			draw(true);
//			layerTableChanged.emit ();
//			return true;
//		}
//		return false;
//	}

	bool setLayerAttribute (cstring name, WDLayerAttribute attr)
	{
		WDLayer layer;
		if ((layer = layerTable().setLayerAttribute (name, attr)) !is null)
		{
			setModified();
			deselectAllItems (layer.name);
			_blocktable.updateLayer (layer.name, layer);
			draw(true);
			layerTableChanged.emit ();
			return true;
		}
		return false;
	}

	bool setLayerColor (cstring name, Color color)
	{
		WDLayer layer;
		if ((layer = layerTable().setLayerColor (name, color)) !is null)
		{
			setModified();
			_blocktable.updateLayer (layer.name, layer);
			draw(true);
			layerTableChanged.emit ();
			return true;
		}
		return false;
	}

	bool renameLayer (cstring name, cstring rename)
	{
		WDLayer layer;
		if ((layer = layerTable().setLayerName (name, rename)) !is null)
		{
			setModified();
			_blocktable.updateLayer (layer.name, layer);
			draw(true);
			layerTableChanged.emit ();
			return true;
		}
		return false;
	}

	bool moveLayer (cstring name, WDDirection d, int count, bool drw=true)
	{
		bool rc = true;;

		if (count < 0) count = 0;
		if (count > 1000) count = MAXCOPYCOUNT;

		for (int i=0; i<count; i++)
		{
			rc = _layertable.moveLayer (name, d) || rc;
		}

		if (rc)
		{
			if (drw) draw();
			layerTableChanged.emit  ();
			return true;
		}
		return false;
	}

	bool activateBlock (cstring name)
	{
		auto oldblock = _activeblock;
		auto newblock = _blocktable.find (name);

		if (newblock is null) return false;

		if (oldblock is null) // save the current view
		{
			_czoom = activeView().window;
		}

		if (newblock !is oldblock)
		{
			_activeblock = newblock;

			inhibit(true);
//			hidePages (_pageshidden);
			zoomAll ();
			inhibit (false);

			blockTableChanged.emit  ();
			layerTableChanged.emit  (); // every block maintains its own active layer
		}
		return true;
	}

	/// create a block if it doesn't exist yet


	WDBlock findBlock (cstring name) {return _blocktable.find(name);}

	// Take the selected elements and create a new block being made up of them
	// delete those elements from the canvas and insert the new block as a reference.
	// Unfortunately, blocks can contain blocks and in some weird circumstances
	// we can have circular dependencies that lead to a CRASH!
	WDBlock createBlock (cstring name, DPair base, bool activate=true)
	{
		// return with an error because a block with the requested name exists already
		if (findBlock(name)) return null;

		// create a new block
		auto block = _blocktable.create (name);
		if (block is null) return null;

		// do this now, because all items will leave _selected
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		activeBlock().drawAllGrips    (painter, _matrix, 0.0); // invisible
		activeBlock().drawAllSelected (painter, _matrix, 1.0); // visible

		// now, move selected objects over to the new block
		// go through all layers, find selected items
		// and move them to block

		int count=0;
		foreach (source_layer; activeBlock().list) // iterate through all layers
		{
			auto target_layer = block.create (source_layer.name);
			if (target_layer is null) continue;

			target_layer.update (source_layer);

			foreach (item; source_layer.selected)
			{
				if (typeid(item.payload) is typeid(WDReference))
				{
					whereami;
				}


				// adapt the position of the items relative to the base point
				item.move (activeView.origin-base);
				target_layer.items ~= item.payload;
				count++;
			}

			source_layer.selected.clear;
		}

		if (count>0) // block has at least one item
		{
			activeLayer.addItem (new WDReference (block, base));
		}

		if (activate)
		{
			// store current zoom and pan
			_czoom = activeView().window;

			// switch to the new block
			_activeblock = block;
			zoomAll ();
			draw ();
		}

		blockTableChanged.emit ();
		return block;
	}

	bool removeBlock (cstring name)
	{
		// remove block only, when it's nowhere referenced
		// so, search for at least one reference to this block

		auto block = _blocktable.find (name);

		if (block)
		{
			foreach (layer; _layertable.list) // iterate through all layers
			{
				foreach (item; layer.items)
				{
					if (cast(WDReference)item.payload)
					{
						writefln ("%s", cast(WDReference)item.payload);
						return false;
					}
				}

				foreach (item; layer.selected)
				{
						writefln ("%s", cast(WDReference)item.payload);
						return false;
				}
			}
		}

		if (_blocktable.remove (name))
		{
			setModified();
			draw(true);
			blockTableChanged.emit();
			return true;
		}

		return false;
	}

//// here the possibility to set <base>
//bool setBlock (cstring n, const WDLayer &t) {return false;}

	bool renameBlock (cstring name, cstring rename)
	{
		if (_blocktable.rename (name, rename))
		{
			setModified();
			draw(true);
			blockTableChanged.emit();
			return true;
		}

		return false;
	}

	bool activateCanvas ()
	{
		if (_activeblock !is null)
		{
			_activeblock = null;

//			inhibit(true);
//			hidePages (_pageshidden);
			zoomRect (_czoom);
//			inhibit (false);

			blockTableChanged.emit();
			layerTableChanged.emit();
		}
		return true;
	}


	bool closeDrawing (bool redraw=true)
	{
		if (_modified)
		{
			auto m = Dialog.Message (window, caption, "You have unsaved data. Do you want to save them?", "ync");

			switch (m)
			{
			case DialogResult.CANCEL:
				return false;
			case DialogResult.NO:
				break;
			default:
				if (saveFile() == false) return false;
			}
		}

		init ();

		if (redraw) draw (true);

		layerTableChanged.emit ();
		viewTableChanged.emit  ();
		blockTableChanged.emit ();

		return true;
	}

	/** will call a dialog
	**/
	bool saveFileDialog ()
	{
		auto path = Dialog.FileSave (window, _filename);
		if (path != "") return saveFile (path);

		Dialog.Message (window, caption, "Could not save file.", "o");
		return false;
	}

	/** will call a dialog only when needed
	**/
	bool saveFile (cstring pathname="")
	{
		bool sorry ()
		{
			Dialog.Message (window, caption, "Sorry: could not save file.", "o");
			return false;
		}

		cstring path = pathname;
		if (path == "") path = _filename;

		if (path == "") return saveFileDialog();

		WDFileType type = stringType (path);

		if (type == WDFileType.NOTYPE)
		{
			path ~= ".wdf";
			type = WDFileType.WDFTYPE;
		}

		try
		{
			auto file = File (expandTilde(path.idup), "w");
			bool rc = true;

			if      (type == WDFileType.WDFTYPE) rc = WDFWriter.write (file, this);
			else if (type == WDFileType.DXFTYPE) rc = DXFWriter.write (file, this);

			if (rc == false) return sorry;

			setFileName (path);
			setModified (false);
			file.close();
			return true;
		}
		catch (Throwable)
		{
			return sorry;
		}
	}

	/** will call a dialog
	**/
	bool openFileDialog ()
	{
		if (closeDrawing() == false) return false;

		auto path = Dialog.FileOpen (window, _filename);
		if (path != "") return openFile (path);

		Dialog.Message (window, caption, "Could not read file.", "o");
		return false;
	}

	bool openFile  (cstring path)
	{
		bool sorry ()
		{
			Dialog.Message (window, caption, "Sorry: could not read file.", "o");
			return false;
		}

		if (closeDrawing() == false) return sorry;

		if (path.empty) return openFileDialog ();

		try
		{
			auto file = File (expandTilde(path.idup), "r");
			auto type = fileType (file); // attn: filetype() swallows 1st line

			if (type == WDFileType.NOTYPE)
			{
				Dialog.Message (window, caption, "Unknown file type", "o");
			}
			else
			{
				inhibit (true);
				changingTables.emit (true);

				if      (type == WDFileType.WDFTYPE) WDFReader.read (file, this);
				else if (type == WDFileType.DXFTYPE) DXFReader.read (file, this);

				changingTables.emit (false);
				// call zoom before inhibit, because both, inhibit and zoom call
				// draw(), but zoom can't because drawing ist still inhibited
				zoomAll ();

				setFileName (path);
				recentlyOpened.emit(path);
				inhibit (false);
			}

			file.close();
			setModified(false);
			return true;
		}
		catch (Throwable)
		{
			return sorry;
		}
	}

	const(char)[] fileName () const {return _filename;}

	void setFileName (const(char)[] name)
	{
		_filename = name;
		fileNameChanged.emit (_filename);
	}


	const(char)[] canvasName () const {return _canvasname;}

	DPair worldPos () const {return _worldpos;}

	bool eraseSelected()
	{
		auto active = activeBlock;
		auto p = new Painter (this);
		scope (exit) destroy (p);

		if (_inhibited == false)
		{
			active.drawAllSelected (p, _matrix, 0.0);
			active.drawAllGrips    (p, _matrix, 0.0);
		}

		selectedToUndo ();
		active.eraseSelected ();

		drawAllLayers (p);
		setModified();
		return true;
	}

	bool offsetSelected (double distance, DPair side, int count, bool temp=false)
	{
		if (!distance.isReal()) return false;
		if (!side.isReal()) return false;
		if (count > 1000) return false;		// this is an arbitrary value, could also be 100 or 1000000

		auto rc = true;
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		for (int i=1; i<=count; i++)
		{
			foreach (layer; activeBlock().list)
			{
				foreach (item; layer.selected[])
				{
					WDItem clone = item.clone();
					if (clone && clone.offset (distance*i, side))
					{
						if (temp) addItemToTemporary (clone);
						else      addItemToLayer (clone, layer);
					}
					else
					{
						rc = false;
					}
				}
			}
		}

		return rc;
	}

	bool moveSelected (DPair distance, int count, bool temp=false)
	{
		if (!distance.isReal()) return false;
		if (count > MAXCOPYCOUNT) return false;

		bool rc = true;
		bool copy;
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		if (count == 0 || gripMode == true)		// only move once
		{
			count = 1;
			copy = false;
		}
		else
		{
			copy = true;
		}

		foreach (layer; activeBlock().list)
		{
			foreach (item; layer.selected[])
			{
				for (int i=1; i<=count; i++)
				{
					if (temp)
					{
						WDItem clone = item.clone();
						if (clone)
						{
							if (gripMode) clone.move_grips (distance*i);
							else
							{
								clone.move (distance*i);
							}

							addItemToTemporary (clone);
						}
						else
						{
							rc = false;
						}
					}
					else
					{
						setModified();

						if (copy == false)
						{
							layer.drawItem  (painter, _matrix, item, 0.0);

							if (gripMode)
							{
								layer.drawGrips (painter, _matrix, item, 0.0);
								item.move_grips (distance*i);
								layer.drawGrips (painter, _matrix, item, 1.0);
							}
							else
							{
								item.move (distance*i);
							}


							layer.drawItem  (painter, _matrix, item, Settings.selectDensity);
						}
						else
						{
							WDItem clone = item.clone();
							if (clone)
							{
								clone.move (distance*i);

								if (temp) addItemToTemporary (clone);
								else      addItemToLayer (clone, layer);
							}
						}
					}
				}
			}
		}
		return rc;
	}

//	bool moveSelected (DPair distance, int count, bool temp=false)
//	{
//		auto rc = true;
//		auto distance = to-from;
//		auto painter = new Painter (this);
//		scope (exit) destroy (painter);


//		foreach (layer; activeBlock().list)
//		{
//			foreach (item; layer.selected[])
//			{
//				if (temp)
//				{
//					WDItem clone = item.clone();
//					if (clone)
//					{
//						if (gripMode) clone.move_grips (distance);
//						else          clone.move (distance);

//						addItemToTemporary (clone);
//					}
//					else
//					{
//						rc = false;
//					}
//				}
//				else
//				{
//					setModified();

//					layer.drawItem  (painter, _matrix, item, 0.0);
//					if (gripMode) layer.drawGrips (painter, _matrix, item, 0.0);

//					if (gripMode) item.move_grips (distance);
//					else          item.move (distance);

//					layer.drawItem  (painter, _matrix, item, Settings.selectDensity);
//					if (gripMode) layer.drawGrips (painter, _matrix, item, 1.0);
//				}
//			}
//		}
//		return rc;
//	}

	bool mirrorSelected (DPair anchor, DPair direction, bool temp=false)
	{
		if (!anchor.isReal()) return false;
		if (!direction.isReal()) return false;
		if (direction == DPair(0, 0)) return false;

		auto rc = true;
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		foreach (layer; activeBlock().list)
		{
			foreach (item; layer.selected[])
			{
				WDItem clone = item.clone();
				if (clone)
				{
					clone.mirror (anchor, direction);

					if (temp)  addItemToTemporary (clone);
					else       addItemToLayer     (clone, layer);
				}
				else
				{
					rc = false;
				}

			}
		}
		return rc;
	}

	bool rotateSelected (DPair center, double angle, int count, bool temp=false)
	{
		if (!center.isReal()) return false;
		if (!angle.isReal()) return false;
		if (count > MAXCOPYCOUNT) return false;

		bool rc = true;
		bool copy;
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		if (count == 0 || gripMode == true)		// only rotate once
		{
			count = 1;
			copy = false;
		}
		else
		{
			copy = true;
		}

		foreach (layer; activeBlock().list)
		{
			foreach (item; layer.selected[])
			{
				for (int i=1; i<=count; i++)
				{
					if (temp)
					{
						WDItem clone = item.clone();
						if (clone)
						{
							if (gripMode) clone.rotate_grips (center, angle*i);
							else
							{
								clone.rotate (center, angle*i);
							}

							addItemToTemporary (clone);
						}
						else
						{
							rc = false;
						}
					}
					else
					{
						setModified();

						if (copy == false)
						{
							layer.drawItem  (painter, _matrix, item, 0.0);

							if (gripMode)
							{
								layer.drawGrips (painter, _matrix, item, 0.0);
								item.rotate_grips (center, angle*i);
								layer.drawGrips (painter, _matrix, item, 1.0);
							}
							else
							{
								item.rotate (center, angle*i);
							}


							layer.drawItem  (painter, _matrix, item, Settings.selectDensity);
						}
						else
						{
							WDItem clone = item.clone();
							if (clone)
							{
								clone.rotate (center, angle*i);

								if (temp) addItemToTemporary (clone);
								else      addItemToLayer (clone, layer);
							}
						}
					}
				}
			}
		}
		return rc;
	}

	bool scaleSelected (DPair base, double factor, int count, bool temp=false)
	{
whereamif ("%s %s %s", base, factor, count);
		if (!base.isReal()) return false;
		if (!factor.isReal()) return false;
		if (count > MAXCOPYCOUNT) return false;
whereamif ("%s %s %s", base, factor, count);

		bool rc = true;
		bool copy;
		auto painter = new Painter (this);
		scope (exit) destroy (painter);

		if (count == 0 || gripMode == true)		// only scale once
		{
			count = 1;
			copy = false;
		}
		else
		{
			copy = true;
		}

		foreach (layer; activeBlock().list)
		{
			foreach (item; layer.selected[])
			{
				for (int i=1; i<=count; i++)
				{
					if (temp)
					{
						WDItem clone = item.clone();
						if (clone)
						{
							if (gripMode) clone.scale_grips (base, factor*i);
							else          clone.scale (base, factor*i);

							addItemToTemporary (clone);
						}
						else
						{
							rc = false;
						}
					}
					else
					{
						setModified();

						if (copy == false)
						{
							layer.drawItem  (painter, _matrix, item, 0.0);

							if (gripMode)
							{
								layer.drawGrips (painter, _matrix, item, 0.0);
								item.scale_grips (base, factor*i);
								layer.drawGrips (painter, _matrix, item, 1.0);
							}
							else
							{
								item.scale (base, factor*i);
							}


							layer.drawItem  (painter, _matrix, item, Settings.selectDensity);
						}
						else
						{
							WDItem clone = item.clone();
							if (clone)
							{
								clone.scale (base, factor*i);

								if (temp) addItemToTemporary (clone);
								else      addItemToLayer (clone, layer);
							}
						}
					}
				}
			}
		}
		return rc;
	}

	bool selectItem (DPair pos, bool grips)
	{
		if (!pos.isReal()) return false;

		auto list = activeBlock();
		auto item = list.selectItem (pos, realWorldApertureRadius(), grips);

		if (item is null) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		list.drawItem  (p, _matrix, item, Settings.selectDensity);
		if (gripMode) list.drawGrips (p, _matrix, item, 1);
		return true;
	}

	/// toggles the grip nearest to pos
	bool toggleGrip (DPair position)
	{
		if (!position.isReal()) return false;

		auto list = activeBlock();
		auto item = list.toggleGrip (position, realWorldApertureRadius());

		if (item is null) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		list.drawItem  (p, _matrix, item, Settings.selectDensity);
		if (gripMode) list.drawGrips (p, _matrix, item, 1);
		return true;
	}

	/// toggles all grips in window grip nearest to pos
	bool toggleGrips (DRect window)
	{
		if (!window.isReal()) return false;

		auto list = activeBlock();
		auto items = list.toggleGrips (window);

		if (items is null || items.empty) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);

		foreach (item; items)
		{
			list.drawItem  (p, _matrix, item, Settings.selectDensity);
			if (gripMode) list.drawGrips (p, _matrix, item, 1);
		}
		return true;
	}


	bool deselectItem (DPair m)
	{
		WDItem o;
		if ((o = activeBlock().deselectItem (m, realWorldApertureRadius())) is null) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		activeBlock().drawGrips (p, _matrix, o, 0);	// invisible
		activeBlock().drawItem  (p, _matrix, o, 1);  // visible
		return true;
	}

	bool selectItems (DRect window, bool entirely, bool grips)
	{
		auto array = activeBlock().selectItems (window, entirely, grips);

		if (array.length == 0) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		draw (false);
		return true;
	}

	bool deselectItems (DRect window, bool entirely)
	{
		auto array = activeBlock().deselectItems (window, entirely);
		if (array.length == 0) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		draw (false);
		return true;
	}

	bool selectAllItems ()
	{
		auto rc = activeBlock().selectAllItems();
		draw();
		return rc;
	}

	bool deselectAllItems (cstring layer = "")
	{
		auto p= new Painter (this);
		scope (exit) destroy (p);
		activeBlock().drawAllGrips (p, _matrix,    0); // invisible
		activeBlock().drawAllSelected (p, _matrix, 1); // visible
		p.destroy;

		if (activeBlock().deselectAllItems(layer)) return true;
		else                                       return false;
	}

	bool fitItemToSelected (bool trim, bool extended)
	{
		auto pi = _pickedItem;
		_pickedItem = null;
		if (pi is null) return false;

		DPair[] trimp;   // trimming points
		DPair[] intrp;   // intersection points
		WDItem [] border;
		DPair from, to;

		// is object trimmable?
		trimp = pi.trimmingPoints();
		if (trimp.empty) return false;

		// determine point to trim to:
		// iterate through all selected items
		foreach (b; activeBlock().selectedItems())
		{
			// get intersections with selected item
			DPair[] isec= intersections_all(pi, b);

			foreach (i; isec)
			{
				// if mode is trim,
				if (trim == true)
				{
					// sort out points, that don't lay on object (including the endpoints)

					if (canFind!((a, b) => circa(a,b)) (trimp, i)) continue;
					if (!pi.encloses (i)) continue;
				}
				else // mode is extend
				{
					// sort out points, that lay on object
					if (pi.encloses (i)) continue;
				}

				// sort out points that don't lay on border lines
				if (extended == false)
				{
					if (!b.encloses (i)) continue;
				}
				intrp ~= i;
			}
		}

		// get intersection point nearest to <at>
		double dist = double.infinity;
		foreach (i; intrp)
		{
			double d;
			if ((d = pdist(i, _pickedThere)) < dist) {dist = d; to = i;}
		}

		// found a point to trim to?
		if (to.notReal == true) return false;

		// determine point to trim from
		dist = double.infinity;
		foreach (i; trimp)
		{
			double d;
			// get trimming point nearest to <at>
			if ((d = pdist(i, _pickedThere)) < dist) {dist = d; from = i;}
		}

		// found a point to trim from?
		if (from.notReal == true) return false;

		auto p = new Painter (this);
		scope (exit) destroy (p);
		p.setColor (Settings.canvasColor);

		// delete old item
		if (_inhibited==false) pi.draw (p, _matrix, 0);
		pi.fit (from, to);
		setModified();
		drawAllLayers (p); // complete redraw neccessary because we don't know, if an
						   // item was hidden by <c> before the move
		return true;
	}

//bool checkForCircularReferences ()
//{
//    return _blocktable.checkForCircular ();
//}

	void quit ()
	{
		if (closeDrawing()) App.quit();
	}

	static extern (C) cairo_status_t writer (void *closure, const ubyte *data, uint length)
	{
		FILE* file = cast(FILE*) closure;
		fwrite (data, 1, length, file);

		return CAIRO_STATUS_SUCCESS; // else CAIRO_STATUS_WRITE_ERROR
	}

	double mm_to_pt (double mm) {return (mm/25.4*72.0);}

	bool print (DPair from, DPair to, cstring printer)
	{
		double printer_width = mm_to_pt(210);
		double printer_height = mm_to_pt(297);

		auto file = File.tmpfile();
		scope (exit) file.close;

		// enter new scope, so that at end of scope surface gets destroyed; this is
		// neccessary, so that cairo flushes the drawing into the file. Is this a bug?
		{
			auto surface = CairoPsSurface.create_for_stream  (&writer, cast (void*) file.getFP(), printer_width, printer_height);
			scope (exit) destroy (surface);
			auto painter = new Painter (surface);
			scope (exit) destroy (painter);

			if (!(from.isReal() && to.isReal()))
			{
				auto e = extents;
				from = DPair (e.x0, e.y0);
				to = DPair (e.x1, e.y1);

			}

			double drawing_width = to.x - from.x;
			double drawing_height = to.y - from.y;

//			auto w = activeView.window;

//			double sc = min (size.width/w.width, size.height/w.height);
			double sc = min (printer_width/drawing_width, printer_height/drawing_height);

			double tx = -from.x;
			double ty = -from.y;

			auto matrix = CairoMatrix();

			// scale view.window to widget size and move to lower left corner
//			_matrix.translate (0, size.height);
			matrix.translate (0, printer_height);
			matrix.scale (sc, -sc);
			matrix.translate (tx, ty);

			// centering the window in widget
			double dx, dy;
			matrix.transform_distance (drawing_width, drawing_height, tx, ty);
			matrix.translate ((printer_width-tx)/sc/2, (printer_height+ty)/sc/2);

			activeBlock.drawAllItems (painter, matrix, 1.0);

			painter.cairoContext.stroke();
//			painter.cairoContext.show_page();	has no effect
//			surface.flush();					has no effect
		}	// context and surface get destroyed here

		// PS file is now created
		file.rewind;

		import std.string: toStringz;
		immutable (char)* printerz;
		if (printer == "") printerz = cupsGetDefault();
		else               printerz = toStringz(printer);

		int job_id = cupsCreateJob (CUPS_HTTP_DEFAULT, printerz, "WildCAD ", 0, null);

		if (cupsStartDocument (CUPS_HTTP_DEFAULT, printerz, job_id, "", CUPS_FORMAT_POSTSCRIPT, 1) != HTTP_STATUS_CONTINUE)
			return false;

		char[65536] buffer;
		for(;;)
		{
			auto b = file.rawRead (buffer);
			if (b.length == 0) break;

			if (cupsWriteRequestData(CUPS_HTTP_DEFAULT, buffer.ptr, buffer.length) != HTTP_STATUS_CONTINUE)
				break;
		}

		if (cupsFinishDocument(CUPS_HTTP_DEFAULT, printerz) != IPP_STATUS_OK) return false;
		return true;
	}

	bool selectedToUndo ()
	{
		foreach (layer; activeBlock.list)
		{
			if (layer.empty) continue;


			foreach (item; layer.selected[])
			{
				_undoBuffer ~= item.toString ~ "\n";
			}
		}

		return true;
	}

	bool undoToClipboard ()
	{
		App.clipboard.writeText (_undoBuffer, SelectionType.CLIPBOARD);
		return true;
	}

	bool copyToClipboard (DPair from, bool cut=false)
	{
		DPair anchor;
		if (from.isReal) anchor = from;
		else             anchor = _viewtable.activeView().origin;

		char[] text = "# WDF\n".dup;
		text ~= "anchor @ca " ~ pretty(anchor) ~ "\n";

		foreach (layer; activeBlock.list)
		{
			if (layer.empty) continue;


			foreach (item; layer.selected[])
			{
				text ~= item.toString ~ "\n";
			}
		}
		App.clipboard.writeText (text, SelectionType.CLIPBOARD);

		if (cut) return eraseSelected;
		else     return true;
	}

	bool pasteFromClipboard (DPair to)
	{
		DPair anchor;
		if (to.isReal) anchor = to;
		else           anchor = _viewtable.activeView().origin;


		auto clip = App.clipboard.readText (SelectionType.CLIPBOARD);
		auto rc = WDFReader.read (clip, anchor, this);
		redraw();
		return rc;
	}
}
