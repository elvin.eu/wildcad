import std.conv;
import std.math;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDCircle : WDItem
{
private:
	DPair _center, _quad0, _quad90, _quad180, _quad270;
	double _radius;

public:

	this (DPair center, double radius)
	{
		init (center, radius);
	}

	this (DPair p1, DPair p2)
	{
		this (mid (p1, p2), pdist (p1, p2)/2);
	}

	this (DPair p1, DPair p2, DPair p3)
	{
		DPair mid1 = mid (p1, p2);
		DPair mid2 = mid (p1, p3);

		DPair per1 = perpendicular (p1, p2) + mid1;
		DPair per2 = perpendicular (p1, p3) + mid2;

		auto center = intersect_xx (mid1, per1, mid2, per2);

		// when all 3 points are on a line, we use only the 1st two points
		// to describe the circle. This is a failsafe option
		if (center.notReal()) this (p1, p2);
		else                  this (center, pdist (p1, center));
	}

	this (const WDCircle c)
	{
	  _center  = c._center;
	  _quad0   = c._quad0;
	  _quad90  = c._quad90;
	  _quad180 = c._quad180;
	  _quad270 = c._quad270;
	  _radius  = c._radius;
	}


	void init (DPair center, double radius)
	{
		_center = center.isReal ? center : DPair(0.0, 0.0);
		_radius = radius.isReal ? radius : 1.0;

		_quad0   = _center + DPair (_radius, 0.0);
		_quad90  = _center + DPair (0.0, _radius);
		_quad180 = _center - DPair (_radius, 0.0);
		_quad270 = _center - DPair (0.0, _radius);
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDCircle (this);
	}

	DPair center () const {return _center;}
	double radius() const {return _radius;}

	override string toString () const
	{
		return "circle @cc " ~ pretty(_center) ~
			" @fr " ~ pretty(_radius);
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		p.drawCircle (_center, _radius, matrix);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		with (WDItemSnap)
		{
			return ((s & (NEAREST | INTERSECTION | CARDINAL | CENTER | TANGENT)) != 0);
		}
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapmode & (WDItemSnap.NEAREST | WDItemSnap.INTERSECTION))
		{
			snap = cnear (_center, _radius, pointer);
			dist = pdist (snap, pointer);
		}
		else
		{
			if (snapmode & WDItemSnap.CENTER)
			{
				snap = _center;
				dist = pdist (snap, pointer);
			}
			if (snapmode & WDItemSnap.CARDINAL)
			{
				double pd;
				if ((pd = pdist (_quad0,   pointer)) < dist) {dist = pd;  snap = _quad0;  }
				if ((pd = pdist (_quad90,  pointer)) < dist) {dist = pd;  snap = _quad90; }
				if ((pd = pdist (_quad180, pointer)) < dist) {dist = pd;  snap = _quad180;}
				if ((pd = pdist (_quad270, pointer)) < dist) {dist = pd;  snap = _quad270;}
			}
			if (snapmode & WDItemSnap.TANGENT)
			{
//				double d;
//				auto pq = csquare (_center, _radius, from);

//				if ((d = pdist (pq[0], pointer)) < dist) {dist = d;  snap = pq[0];}
//				if ((d = pdist (pq[1], pointer)) < dist) {dist = d;  snap = pq[1];}

				DPair m = (_center+from) / 2;
				auto pq = intersect_cc (_center, _radius, m, pdist (_center, m));
				double d;
				if ((d = pdist (pq[0], pointer)) < dist) {dist = d;  snap = pq[0];}
				if ((d = pdist (pq[1], pointer)) < dist) {dist = d;  snap = pq[1];}
			}
		}
		return wdTuple(snap, dist);
	}


	override bool inWindow (const DRect w, bool fi) const
	{
		auto bl = DPair (_center.x-_radius, _center.y-_radius);
		auto ur = DPair  (_center.x+_radius, _center.y+_radius);
		if (w.contains(bl) && w.contains(ur)) return true;

		if (fi == false)
		{
			auto lt = DPair (w.x0, w.y1);
			auto lb = DPair (w.x0, w.y0);
			auto rt = DPair (w.x1, w.y1);
			auto rb = DPair (w.x1, w.y0);

			auto i1 = intersect_lc (lt, rt, _center, _radius);
			auto i2 = intersect_lc (lb, rb, _center, _radius);
			auto i3 = intersect_lc (lt, lb, _center, _radius);
			auto i4 = intersect_lc (rt, rb, _center, _radius);

			if (w.contains(i1[0])) return true;
			if (w.contains(i1[1])) return true;
			if (w.contains(i2[0])) return true;
			if (w.contains(i2[1])) return true;
			if (w.contains(i3[0])) return true;
			if (w.contains(i3[1])) return true;
			if (w.contains(i4[0])) return true;
			if (w.contains(i4[1])) return true;
		}
		return false;
	}

	override DRect extents () const
	{
		return DRect (DPair(_center.x-_radius, _center.y-_radius),
		              DPair(_center.x+_radius, _center.y+_radius));
	}

	override bool encloses (DPair p) const
	{
		return circa (pdist (p, _center), _radius, 0.1);
	}

//	override DPair[] trimmingPoints() const

	override void move (DPair d)
	{
		init (_center+d, _radius);
	}


	override void mirror (DPair a, DPair d)
	{
		init (2*xnear(a,d,_center)-_center, _radius);
	}

	override void rotate (DPair c, double a)
	{
		init (c+auxiliary.rotate(vector (c, _center), rad(a)), _radius);
	}

	override void scale (DPair c, double s)
	{
		init ((_center-c)*s+c, _radius*s);
	}

	override bool offset (double d, DPair s)
	{
		if      (pdist (_center, s) > _radius) init (_center, _radius+d);
		else if (d < _radius)                  init (_center, _radius-d);
		else                                   return false;

		return true;
	}


//	override bool fit (DPair from, DPair to)
//  override bool checkForCircular (int ttl) {return false;}
}