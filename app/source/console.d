// console.d

import std.algorithm;
import std.signals;
import std.string;

import wcw;

import auxiliary;

enum prompt_string  = ">";
enum comment_string = "#";

class WDConsole : SimpleEditor
{
	// signals
	mixin Signal!(cstring) lineChanged;
	mixin Signal!(cstring) lineEntered;
	mixin Signal!(cstring) lineEscaped;
	mixin Signal!(ModifierKey, KeySymbol) shortcut;


	const(char)[] _lastcommand;

	this (Widget parent, int[] i...)
	{
		super (parent, i);

		enum NONE        = ModifierKey.NONE;
		enum CTRL        = ModifierKey.CONTROL;
		enum SHIFT_CTRL  = ModifierKey.SHIFT|ModifierKey.CONTROL;

		addAction (SimpleEditor.KeyAction(NONE,       KeySymbol.ESCAPE, &escape));
		addAction (SimpleEditor.KeyAction(CTRL,       'a',              {shortcut.emit (CTRL,       cast(KeySymbol)'a');}));
		addAction (SimpleEditor.KeyAction(SHIFT_CTRL, 'A',              {shortcut.emit (SHIFT_CTRL, cast(KeySymbol)'A');}));
		addAction (SimpleEditor.KeyAction(CTRL,       's',              {shortcut.emit (CTRL,       cast(KeySymbol)'s');}));
		
		insertString (prompt_string);
		textChanged.connect (&emitLine);
	}

	void shortcut_ctrlshift ()
	{
		whereami ();
	}

	override void typeString (const(char)[] str)
	{
		if (str == "\n") enter();
		else             super.typeString (str);
	}

	bool onLastPrompt ()
	{
		return onLastLine && currentLine.startsWith (prompt_string);
	}

	cstring currentCommandLine ()
	{
		return currentLine.chompPrefix (prompt_string);
	}

	void emitLine ()
	{
		if (!onLastPrompt) return;
		lineChanged.emit (currentCommandLine);
	}

	// externally connected with right mouseclick from canvas
	void enter ()
	{
		if (!onLastPrompt) return;

		auto line = currentCommandLine;

		if (line == "")
		{
			insertString (_lastcommand ~ " ");
		}
		else
		{
			moveFarRight();
			insertString ("\n");
			lineEntered.emit (line);

			moveFarRight();
			if (!currentLine.empty) insertString ("\n");
			insertString (prompt_string);
		}
	}

	// also externally connected with middle mouseclick from canvas
	void escape ()
	{
		auto line = currentCommandLine;
		moveEnd();
		insertString ("\n" ~ prompt_string);
		lineEscaped.emit(line);
	}

	void appendText (const(char)[] text)
	{
		// insert only at end of text
//		if (!onLastLine) moveEnd();
		moveEnd();
		
		// if it's a command, ensure, that line starts with prompt
		if (text.startsWith (":"))
		{
			if (!onLastPrompt || currentCommandLine != "") insertString ("\n" ~ prompt_string);
		}

		insertString (text.chompPrefix (":"));
		textChanged.emit();
	}

	void setLastCommand (const(char)[] cmd)
	{
		_lastcommand = cmd;
	}

}