extern (C)
{
// from http.h
	struct http_t;

	enum
	{
		CUPS_FORMAT_AUTO = "application/octet-stream",
		CUPS_FORMAT_COMMAND = "application/vnd.cups-command",
		CUPS_FORMAT_JPEG = "image/jpeg",
		CUPS_FORMAT_PDF = "application/pdf",
		CUPS_FORMAT_POSTSCRIPT = "application/postscript",
		CUPS_FORMAT_RAW	= "application/vnd.cups-raw",
		CUPS_FORMAT_TEXT = "text/plain"
	}

// from cups.h
	alias cups_ptype_t = int;
	enum
	{
	  CUPS_PRINTER_LOCAL = 0x0000,
	  CUPS_PRINTER_CLASS = 0x0001,
	  CUPS_PRINTER_REMOTE = 0x0002,
	  CUPS_PRINTER_BW = 0x0004,
	  CUPS_PRINTER_COLOR = 0x0008,
	  CUPS_PRINTER_DUPLEX = 0x0010,
	  CUPS_PRINTER_STAPLE = 0x0020,
	  CUPS_PRINTER_COPIES = 0x0040,
	  CUPS_PRINTER_COLLATE = 0x0080,
	  CUPS_PRINTER_PUNCH = 0x0100,
	  CUPS_PRINTER_COVER = 0x0200,
	  CUPS_PRINTER_BIND = 0x0400,
	  CUPS_PRINTER_SORT = 0x0800,
	  CUPS_PRINTER_SMALL = 0x1000,
	  CUPS_PRINTER_MEDIUM = 0x2000,
	  CUPS_PRINTER_LARGE = 0x4000,
	  CUPS_PRINTER_VARIABLE = 0x8000,
	  CUPS_PRINTER_IMPLICIT = 0x10000,

	  CUPS_PRINTER_DEFAULT = 0x20000,
	  CUPS_PRINTER_FAX = 0x40000,
	  CUPS_PRINTER_REJECTING = 0x80000,
	  CUPS_PRINTER_DELETE = 0x100000,
	  CUPS_PRINTER_NOT_SHARED = 0x200000,
	  CUPS_PRINTER_AUTHENTICATED = 0x400000,
	  CUPS_PRINTER_COMMANDS = 0x800000,
	  CUPS_PRINTER_DISCOVERED = 0x1000000,
	  CUPS_PRINTER_SCANNER = 0x2000000,
	  CUPS_PRINTER_MFP = 0x4000000,
	  CUPS_PRINTER_3D = 0x8000000,
	  CUPS_PRINTER_OPTIONS = 0x6fffc
	};

	alias http_status_t = int;
	enum
	{
		CUPS_HTTP_DEFAULT = null
	}

	alias ipp_status_t = int;
	enum
	{
		HTTP_STATUS_CONTINUE = 100
	}

	enum
	{
		IPP_STATUS_OK = 0x0000
	}

	struct cups_option_t
	{
	  char* name;
	  char* value;
	};


	struct cups_dest_t
	{
	  char* name;
	  char*	instance;
	  int is_default;
	  int num_options;
	  cups_option_t	*options;
	};


	int cupsGetDests2 (http_t *http, cups_dest_t **dests);
    void cupsFreeDests (int num_dests, cups_dest_t *dests);

	immutable (char)* cupsGetDefault ();
	int cupsPrintFile (const (char) *name, const (char) *filename,
		const (char) *title, int num_options,
		cups_option_t *options);

	int cupsCreateJob(http_t *http, const char *name,
		const char *title, int num_options,
		cups_option_t *options);

	http_status_t cupsStartDocument(http_t *http, const char *name,
		int job_id, const char *docname,
		const char *format,
		int last_document);

	http_status_t cupsWriteRequestData(http_t *http, const char *buffer, size_t length);

	ipp_status_t cupsFinishDocument(http_t *http, const char *name);

	const (char) *cupsLastErrorString();
}
