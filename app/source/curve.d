import std.conv;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDCurve : WDItem
{
private:
	DPair _start, _end, _ctrl1, _ctrl2;
	bool  _grips, _gripe, _gripc1, _gripc2;

public:
	this (DPair s, DPair e, DPair c1, DPair c2)
	{
		init (s, e, c1, c2);
	}

	this (const WDCurve b)
	{
		init (b.start, b.end, b.ctrl1, b.ctrl2);

		_grips = b._grips;
		_gripe = b._gripe;
		_gripc1 = b._gripc1;
		_gripc2 = b._gripc2;
	}

	void init (DPair s, DPair e, DPair c1, DPair c2)
	{
		if (isReal (s)) _start = s;  else _start = DPair (0,0);
		if (isReal (e)) _end = e;    else _end   = _start;
		if (isReal(c1)) _ctrl1 = c1; else _ctrl1 = _start;
		if (isReal(c2)) _ctrl2 = c2; else _ctrl2 = _end;
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDCurve (this);
	}

	DPair start ()  const {return _start;}
	DPair end ()    const {return _end;}
	DPair ctrl1 ()  const {return _ctrl1;}
	DPair ctrl2 ()  const {return _ctrl2;}

	override string toString () const
	{
		return "curve @c1 " ~ pretty(_start) ~
		       " @c2 " ~ pretty (_end) ~
		       " @d1 " ~ pretty(_ctrl1) ~
		       " @d2 " ~ pretty(_ctrl2);
	}

	DPair[] convertToPoints (int count)
	{
		int n = count - 1;
		if (n < 1) n = 1;

		DPair[] points = [_start];

		for (int i=1; i<n; i++)
		{
			double t = cast (double)i / cast (double) n;

			DPair A1 = _start + t*(_ctrl1 - _start);
			DPair A2 = _ctrl1 + t*(_ctrl2 - _ctrl1);
			DPair A3 = _ctrl2 + t*(_end-_ctrl2);
			DPair B1 = A1 + t*(A2-A1);
			DPair B2 = A2 + t*(A3-A2);
			DPair Q = B1 + t*(B2-B1);

			points ~= Q;
		}
		points ~= _end;

		return points;
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		enum COUNT=60;

		DPair[] points = convertToPoints (COUNT);

		for (int i=1; i<COUNT; i++)
		{
			double px, py, qx, qy;
			matrix.transform_point (points[i-1].x, points[i-1].y, px, py);
			matrix.transform_point (points[i].x, points[i].y, qx, qy);
			p.drawLine (px, py, qx, qy);
		}
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST | WDItemSnap.END | WDItemSnap.GRIP)) != 0);
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapmode & WDItemSnap.NEAREST)
		{
			double pd;
			if ((pd=pdist(_start, pointer)) < dist) {dist = pd; snap = _start;}
			if ((pd=pdist(_end,   pointer)) < dist) {dist = pd; snap = _end;  }
		}
		else
		{
			if (snapmode & WDItemSnap.END)
			{
				double pd;
				if ((pd=pdist(_start, pointer)) < dist) {dist = pd; snap = _start;}
				if ((pd=pdist(_end,   pointer)) < dist) {dist = pd; snap = _end;  }
			}
			if (snapmode & WDItemSnap.GRIP)
			{
				double pd;
				if ((pd=pdist(_ctrl1, pointer)) < dist) {dist = pd; snap = _ctrl1;}
				if ((pd=pdist(_ctrl2, pointer)) < dist) {dist = pd; snap = _ctrl2;  }
			}
		}
		return wdTuple (snap, dist);
	}

	override void drawGrips (Painter p, CairoMatrix m, double den)
	{
		drawGrip (p, m, _start, _grips, den);
		drawGrip (p, m, _end,   _gripe, den);
		drawGrip (p, m, _ctrl1, _gripc1, den);
		drawGrip (p, m, _ctrl2, _gripc2, den);
	}

	override void clearGrips ()
	{
		_grips = _gripe = _gripc1 = _gripc2 = false;
	}

	override WDTuple!(DPair, double) snapToGrip (DPair target) const
	{
		WDTuple!(DPair, double)[] grip =
		[
			wdTuple(cast(DPair)_start, pdist (_start, target)),
		    wdTuple(cast(DPair)_end,   pdist (_end,   target)),
		    wdTuple(cast(DPair)_ctrl1, pdist (_ctrl1, target)),
		    wdTuple(cast(DPair)_ctrl2, pdist (_ctrl2, target))
		];

		ulong i, j, k;

		if (grip[0].second < grip[1].second) i=0; else i=1;
		if (grip[2].second < grip[3].second) j=2; else j=3;
		if (grip[i].second < grip[j].second) k=i; else k=j;
		return grip[k];
	}

	override bool toggleGrip  (DPair target)
	{
		bool t;
		if      (target == _start) {_grips = !_grips; t = true;}
		else if (target == _end)   {_gripe = !_gripe; t = true;}
		else if (target == _ctrl1) {_gripc1 = !_gripc1; t = true;}
		else if (target == _ctrl2) {_gripc2 = !_gripc2; t = true;}
		return t;
	}

	override bool toggleGrips (DRect window)
	{
		bool t;
		if (window.contains (_start)) {_grips = !_grips; t = true;}
		if (window.contains (_end))   {_gripe = !_gripe; t = true;}
		if (window.contains (_ctrl1)) {_gripc1 = !_gripc1; t = true;}
		if (window.contains (_ctrl2)) {_gripc2 = !_gripc2; t = true;}
		return t;
	}

	// incomplete and incorrect
	override bool inWindow (DRect r, bool entirely) const
	{
		if (r.contains (_start) && r.contains (_end) && r.contains (_ctrl1) && r.contains (_ctrl2) )
		{
			return true;
		}

		if (entirely == false)
		{
			auto p1 = DPair (r.x0,r.y0);
			auto p2 = DPair (r.x1,r.y0);
			auto p3 = DPair (r.x1,r.y1);
			auto p4 = DPair (r.x0,r.y1);

			if (intersect_ll(_start, _end, p1, p2).isReal) return true;
			if (intersect_ll(_start, _end, p2, p3).isReal) return true;
			if (intersect_ll(_start, _end, p3, p4).isReal) return true;
			if (intersect_ll(_start, _end, p4, p1).isReal) return true;
		}
		return false;
	}

   	override DRect extents () const
	{
		return DRect (_start, _end);
	}
/*
	override bool encloses (DPair p) const
	{
		if (circa (lnear (_start, _end, p), p)) return true;
		else                                    return false;
	}

	override DPair[] trimmingPoints() const
	{
		return [_start, _end];
	}
*/
	override void move (DPair d)
	{
		_start += d;
		_end   += d;
		_ctrl1 += d;
		_ctrl2 += d;
	}

	override void move_grips (DPair d)
	{
		if (_grips)  _start += d;
		if (_gripe)  _end   += d;
		if (_gripc1) _ctrl1 += d;
		if (_gripc2) _ctrl2 += d;
	}


	override void mirror (DPair a, DPair d)
	{
		if (!d.isReal || d==DPair(0,0)) return;

		_start = 2*xnear (a, d, _start) - _start;
		_end   = 2*xnear (a, d, _end)   - _end;
		_ctrl1 = 2*xnear (a, d, _ctrl1) - _ctrl1;
		_ctrl2 = 2*xnear (a, d, _ctrl2) - _ctrl2;
	}

	override void rotate (DPair c, double a)
	{
		_start = c + auxiliary.rotate(vector (c, _start) , rad(a));
		_end   = c + auxiliary.rotate(vector (c, _end)   , rad(a));
		_ctrl1 = c + auxiliary.rotate(vector (c, _ctrl1) , rad(a));
		_ctrl2 = c + auxiliary.rotate(vector (c, _ctrl2) , rad(a));
	}

	override void rotate_grips (DPair c, double a)
	{
		if (_grips)  _start = c + auxiliary.rotate(vector (c, _start) , rad(a));
		if (_gripe)  _end   = c + auxiliary.rotate(vector (c, _end)   , rad(a));
		if (_gripc1) _ctrl1 = c + auxiliary.rotate(vector (c, _ctrl1) , rad(a));
		if (_gripc2) _ctrl2 = c + auxiliary.rotate(vector (c, _ctrl2) , rad(a));
	}


	override void scale  (DPair c, double s)
	{
		_start = (_start-c)*s + c;
		_end   = (_end-c)*s + c;
		_ctrl1 = (_ctrl1-c)*s + c;
		_ctrl2 = (_ctrl2-c)*s + c;
	}
/*
	override bool offset  (double d, DPair s)
	{
		DPair p = xnear  (_start, _end-_start, s);
		DPair v = vector(p, s).unit * d;

		if (p == s) s = p + DPair(1.0,1.0);

		p = xnear  (_start, _end-_start, s);
		v = vector(p, s).unit * d;

		move (v);
		return true;
	}

	override bool fit (DPair from, DPair to)
	{
		if      (from == _start) _start = to;
		else if (from == _end)   _end   = to;
		else    return false;

		_mid = (_start+_end)/2;
		return true;
	}
*/
	override bool checkForCircular (int ttl) {return false;}

}
