import std.math;
import std.conv;
import std.regex;

import cairo;
import wcw;

import auxiliary;
import painter;
import enums;
import item;
import line;
import text;
import settings;


class WDDimension : WDItem
{
private:
    WDDimensionType _type;
    DPair _start;
    DPair _end;
    DPair _line;

    double _dimlength;

    WDLine _arr1, _arr2, _arr3, _arr4;
    WDLine _del1, _del2;
    WDLine _dim1;

	cstring _usertext;
    WDText _textitem;


	bool  _grips, _gripe;

public:
	this (WDDimensionType dt, DPair st, DPair en, DPair ln, double sz, cstring tx)
	{
		_del1 = new WDLine (DPair(0,0),DPair(0,0));
		_del2 = new WDLine (DPair(0,0),DPair(0,0));
		_dim1 = new WDLine (DPair(0,0),DPair(0,0));
		_arr1 = new WDLine (DPair(0,0),DPair(0,0));
		_arr2 = new WDLine (DPair(0,0),DPair(0,0));
		_arr3 = new WDLine (DPair(0,0),DPair(0,0));
		_arr4 = new WDLine (DPair(0,0),DPair(0,0));
		_textitem = new WDText ("", DPair(0.0, 0.0), 5, 0, WDTextAlign.LEFT);

		init (dt, st, en, ln, sz, tx);
	}

	void init (WDDimensionType dt, DPair st, DPair en, DPair ln, double sz, cstring tx)
	{
		_type  = dt;
		_start = st;
		_end   = en;
		_line  = ln;

		// identify dimension type
		if (_type == WDDimensionType.AUTO)
		{
			// ORTHOGONAL is a dummy type, it's either horizontal or vertical
			if (abs(_end.x-_start.x) > abs(_end.y-_start.y)) _type = WDDimensionType.HORIZONTAL;
			else                                             _type = WDDimensionType.VERTICAL;
		}

		// locate dimension line
		if (_type == WDDimensionType.HORIZONTAL)
		{
			_line.x = (_start.x);
		}
		else if (_type == WDDimensionType.VERTICAL)
		{
			_line.y = (_start.y);
		}
		else  // (_type == WDDimensionType.ALIGNED)
		{
			_line = xnear (_start, perpendicular (_end-_start), _line);
		}

		// components
		double gap = sz*0.2;
		double ext = sz*0.6;
		double obs = sz*0.5;

		double angle = arg(_line-_start);


		DPair pob1 = DPair (vector(angle-PI_4, obs));
		DPair pob2 = DPair (vector(angle-3*PI_4, obs));
		DPair pgap = DPair (vector(angle, gap));
		DPair pext = DPair (vector(angle, ext));

		DPair eline;
		if      (_type == WDDimensionType.HORIZONTAL) eline = DPair(_end.x, _line.y);
		else if (_type == WDDimensionType.VERTICAL)   eline = DPair(_line.x, _end.y);
		else     /* ALIGNED */                        eline = DPair(_end+_line-_start);

		_del1.init (_start+pgap, _line+pext);
		_del2.init (_end+pgap,  eline+pext);
		_dim1.init (_line,      eline);
		_arr1.init (_line, _line+pob1);
		_arr2.init (_line, _line+pob2);
		_arr3.init (eline, eline-pob1);
		_arr4.init (eline, eline-pob2);

		_dimlength = pdist(_line, eline);

		// placing the text

		_usertext = tx;

		if (tx == "")
		{
			int p = Settings.dimLengthPrecision;
			// create a format string: fmt = %%.2f (when p=2)
			string fmt = format ("%%.%sf", p);
			// correctly round _dimlength
			tx = format(fmt, quantize!round(_dimlength, 10.0^^-p));
		}
//		else
//		{
//			auto r = regex(r"\%([0-9]*)\.([0-9]*)(f)");
//			auto res = matchFirst (_usertext, r);
//			int p;
//			try	p = to!int (res[2]); catch (Throwable) {p=6;}

//			try
//			{
//				tx = format (_usertext, quantize!round(_dimlength, 10.0^^-p));
//			}
//			catch (Throwable)
//			{
//				whereamif ("Error in dimension format string \"%s\"", _usertext);
//			}
//		}

		DPair pos;
		double an = 0;
		WDTextAlign al = WDTextAlign.HCENTER|WDTextAlign.BOTTOM;

		if (_type == WDDimensionType.HORIZONTAL)
		{
			pos = DPair(_line.x + 0.5*(_end.x-_start.x), _line.y);
		}
		else if (_type == WDDimensionType.VERTICAL)
		{
			an = 90;
			pos = DPair (_line.x, _line.y + 0.5*(_end.y-_start.y));
		}
		else  // ALIGNED
		{
			pos = _line + 0.5*(_end-_start);
			an = deg (arg(_end-_start));
			if      (an > +90.0) an -= 180.0;
			else if (an < -90.0) an += 180.0;
		}

		_textitem.init (tx, pos, sz, an, al);
	}


	this (const WDDimension dim)
	{
		_type  = dim._type;
		_start = dim._start;
		_end   = dim._end;
		_line  = dim._line;
		_arr1  = new WDLine (dim._arr1);
		_arr2  = new WDLine (dim._arr2);
		_arr3  = new WDLine (dim._arr3);
		_arr4  = new WDLine (dim._arr4);
		_del1  = new WDLine (dim._del1);
		_del2  = new WDLine (dim._del2);
		_dim1  = new WDLine (dim._dim1);
		_textitem = new WDText (dim._textitem);
		_usertext = dim._usertext;

		_grips = dim._grips;
		_gripe = dim._gripe;

	}


	override WDItem clone()
	{
		return new WDDimension (this);
	}

	override string toString () const
	{
		return "dimension @et " ~ pretty (_type) ~
			" @c1 " ~ pretty (_start) ~
			" @c2 " ~ pretty (_end) ~
			" @cl " ~ pretty (_line) ~
			" @tc1 " ~ pretty (_textitem.base) ~
			" @tfs " ~ pretty (_textitem.size) ~
			" @tea " ~ pretty (_textitem.alignment) ~
			" @tst " ~ pretty (_usertext);
	}

	override void draw (Painter p, CairoMatrix m, double d)
	{
	    _del1.draw (p, m, d);
		_del2.draw (p, m, d);
		_dim1.draw (p, m, d);
		_arr1.draw (p, m, d);
		_arr2.draw (p, m, d);
		_arr3.draw (p, m, d);
		_arr4.draw (p, m, d);
		_textitem.draw (p, m, d);
	}

	override void drawGrips (Painter p, CairoMatrix m, double den)
	{
		drawGrip (p, m, _start, _grips, den);
		drawGrip (p, m, _end,   _gripe, den);
	}
/*
	override bool toggleGrip  (DPair target)
	{
		bool ge = _gripe;
		bool gs = _grips;
		double ds = pdist (_start, target);
		double de = pdist (_end, target);

		clearGrips();

		if (ds < de) _grips = !gs;
		else         _gripe = !ge;

		return true;
	}

	override bool pickGrip  (DRect window)
	{
		clearGrips();

		auto s = window.contains (_start);
		auto e = window.contains (_end);

		_gripe = s && !e;
		_grips = e && !s;

		return _grips || _gripe;
	}

	override void clearGrips ()
	{
		_grips = _gripe = false;
	}
*/
	override bool inWindow (DRect r, bool fi) const
	{
		if (fi == true) // all must be fully included
		{
			if (!_del1.inWindow(r, fi)) return false;
			if (!_del2.inWindow(r, fi)) return false;
			if (!_dim1.inWindow(r, fi)) return false;
			if (!_textitem.inWindow(r, fi)) return false;
			if (!_arr1.inWindow(r, fi)) return false;
			if (!_arr2.inWindow(r, fi)) return false;
			if (!_arr3.inWindow(r, fi)) return false;
			if (!_arr4.inWindow(r, fi)) return false;
			return true;
		}
		else // alt least one must touch the rectangle
		{
			if (_del1.inWindow(r, fi)) return true;
			if (_del2.inWindow(r, fi)) return true;
			if (_dim1.inWindow(r, fi)) return true;
			if (_textitem.inWindow(r, fi)) return true;
			if (_arr1.inWindow(r, fi)) return true;
			if (_arr2.inWindow(r, fi)) return true;
			if (_arr3.inWindow(r, fi)) return true;
			if (_arr4.inWindow(r, fi)) return true;
			return false;
		}
	}

	override bool encloses (DPair p) const
	{
		if (_del1.encloses(p)) return true;
		if (_del2.encloses(p)) return true;
		if (_arr1.encloses(p)) return true;
		if (_arr2.encloses(p)) return true;
		if (_arr3.encloses(p)) return true;
		if (_arr4.encloses(p)) return true;
		if (_dim1.encloses(p)) return true;
		if (_textitem.encloses(p)) return true;
		return false;
	}

	override DRect extents () const
	{
		DRect e;

		e = e.united (_del1.extents());
		e = e.united (_del2.extents());
		e = e.united (_dim1.extents());
		e = e.united (_textitem.extents());
		e = e.united (_arr1.extents());
		e = e.united (_arr2.extents());
		e = e.united (_arr3.extents());
		e = e.united (_arr4.extents());

		return e;
	}

	override void move (DPair d)
	{
		_start += d;
		_end   += d;
		_line  += d;

		_del1.move (d);
		_del2.move (d);
		_dim1.move (d);
		_arr1.move (d);
		_arr2.move (d);
		_arr3.move (d);
		_arr4.move (d);
		_textitem.move (d);
	}

/*
	override void drag (DPair d)
	{
		DPair s = _start;
		DPair e = _end;
		DPair l = _line;

		if (_type == WDDimensionType.HORIZONTAL)
		{
			if (_grips == false)
			{
				s += d;
			}

			if (_gripe == false)
			{
				e += d;
			}

			l += d;

		}
		else if (_type == WDDimensionType.VERTICAL)
		{
			if (_grips == false)
			{
				s += d;
			}

			if (_gripe == false)
			{
				e += d;
			}

			l += d;
		}

		init (_type, s, e, l, _textitem.size, _usertext);
	}
*/
}