import std.stdio;
import std.string;
import std.conv;
import std.encoding;
import std.math.traits;

import wcw.auxiliary;
import wcw.color;

import auxiliary;
import enums;
import settings;
import canvas;
import layer;
import layertable;
import block;
import line;
import xline;
import polyline;
import marker;
import ray;
import arc;
import circle;
import text;
import dimension;


alias Group = WDTuple!(int, string); // first is code, second is value

Color dxf_colortable (int index)
{
	static bool initialized;
	static Color[256] _dxf_colortable;

	if (!initialized)
	{
		initialized = true;

		void init_column (int c, int r, int g, int b)
		{
			for (int i=0; i<10; i+=2)
			{
				int f = 100-(i*10);

				_dxf_colortable[c+i]   = Color (      r*f/100,       g*f/100,       b*f/100);
				_dxf_colortable[c+i+1] = Color ((255+r)*f/200, (255+g)*f/200, (255+b)*f/200);
			}
		}

		init_column ( 10, 255,   0,   0);
		init_column ( 20, 255,  64,   0);
		init_column ( 30, 255, 128,   0);
		init_column ( 40, 255, 196,   0);
		init_column ( 50, 255, 255,   0);
		init_column ( 60, 196, 255,   0);
		init_column ( 70, 128, 255,   0);
		init_column ( 80,  64, 255,   0);
		init_column ( 90,   0, 255,   0);
		init_column (100,   0, 255,  64);
		init_column (110,   0, 255, 128);
		init_column (120,   0, 255, 196);
		init_column (130,   0, 255, 255);
		init_column (140,   0, 196, 255);
		init_column (150,   0, 128, 255);
		init_column (160,   0,  64, 255);
		init_column (170,   0,   0, 255);
		init_column (180,  64,   0, 255);
		init_column (190, 128,   0, 255);
		init_column (200, 196,   0, 255);
		init_column (210, 255,   0, 255);
		init_column (220, 255,   0, 196);
		init_column (230, 255,   0, 128);
		init_column (240, 255,   0,  64);

		_dxf_colortable[  0] = Color(  0,   0,   0);   // ungültig
		_dxf_colortable[  1] = Color(255,   0,   0);   // rot
		_dxf_colortable[  2] = Color(220, 220,   0);   // gelb
		_dxf_colortable[  3] = Color(  0, 200,   0);   // grün
		_dxf_colortable[  4] = Color(  0, 220, 220);   // cyan
		_dxf_colortable[  5] = Color(  0,   0, 255);   // blau
		_dxf_colortable[  6] = Color(220,   0, 220);   // magenta
		_dxf_colortable[  7] = Color(  0,   0,   0);   // schwarz
		_dxf_colortable[  8] = Color(100, 100, 100);   // grau
		_dxf_colortable[  9] = Color(180, 180, 180);   // hellgrau

		_dxf_colortable[250] = Color(  0,   0,   0);
		_dxf_colortable[251] = Color( 40,  40,  40);
		_dxf_colortable[252] = Color( 80,  80,  80);
		_dxf_colortable[253] = Color(120, 120, 120);
		_dxf_colortable[254] = Color(160, 160, 160);
		_dxf_colortable[255] = Color(200, 200, 200);
	}
	return _dxf_colortable[index];
}


class DXFReader
{
public:
	static bool read  (File f, WDCanvas canvas)
	{
		bool rc = false;

		try
		{
			auto r = new DXFReader (f, canvas);
			canvas.activateCanvas();
			rc = r.readDXF ();
			canvas.activateCanvas();
		}
		catch (Throwable)
		{}
		return rc;
	}

private:
	WDCanvas _canvas;
	File _file;
	int    _line_num;
	string _line_txt;

	this (File f, WDCanvas canvas)
	{
		_canvas = canvas;
		_file =f;
	}

	bool isEOF (Group grp)
	{
		if (grp.second == "EOF") return true;
		else                    return false;

	}

	bool isEOS (Group grp)
	{
		if      (grp.second == "ENDSEC")  return true;
		else if (grp.second == "SECTION") return true;
		else if (grp.second == "EOF")     return true;
		else                           return false;
	}

	bool isEOB (Group grp)
	{
		if      (grp.second == "ENDBLK")  return true;
		else if (grp.second == "BLOCK")   return true;
		else if (grp.second == "EOF")     return true;
		else                           return false;
	}

	bool readDXF ()
	{
		Group grp; // code and value

		_file.seek(0);	// guarantee, that we start from the beginnig
		while (!isEOF(grp))
		{
			readHeader(grp) ||
			readClasses(grp) ||
			readTables(grp) ||
			readBlocks(grp) ||
			readEntities(grp) ||
			readObjects(grp) ||
			nextGroup(grp);

		}
		return true;
	}

	int lineno;

	void nextGroup (ref Group grp)
	{
		// read in a code/value pair from currrent position in _stream
		// 1st line : read code as integer
		// 2nd line: read value as string

		int code = -1;
		string value = "Error";

		do
		{
			if (_file.eof)
			{
				grp = wdTuple (0, "EOF");
				return;
			}

			// read 1st of 2 lines
			try
			{
				_line_num++;
				_line_txt = _file.readln.strip;
				code = _line_txt.to!int ();
			}
			catch (Throwable)
			{
				whereamif ("dxf error in line %s: can't convert '%s' to group code", _line_num, _line_txt);
				continue;
			}

			try // should never throw
			{
				_line_num++;
				_line_txt = _file.readln.strip;
				value = _line_txt;
			}
			catch (Throwable)
			{
				whereamif ("dxf error in line %s: '%s'", _line_num, _line_txt);
				continue;
			}

		}
		while (code == -1);

		grp = wdTuple (code, value);
	}

	bool parseGroup (Group g, int c, ref int value)
	{
		if (g.first != c) return false;

		try
		{
			value = g.second.to!int();
			return true;
		}
		catch (Throwable)
		{
			whereamif ("dxf error in line %s: can't convert '%s' to integer", _line_num, g.second);
			return false;
		}
	}

	bool parseGroup (Group g, int c, ref double value)
	{
		if (g.first != c) return false;

		try
		{
			value = g.second.to!double();
			return true;
		}
		catch (Throwable)
		{
			whereamif ("dxf error in line %s: can't convert '%s' to double", _line_num, g.second);
			return false;
		}
	}

	bool parseGroup (Group g, int c, ref string value)
	{
		if (g.first != c) return false;

		auto str = cast(Windows1252String)g.second;

		try
		{
			transcode (str, value);
			return true;
		}
		catch (Throwable)
		{
			// this shouldn't happen since all ascii-strings should be valid
			whereamif ("dxf error in line %s: '%s' is not a valid string", _line_num, g.second);
			value = "not a WINDOWS-1252 coded string";
			return false;
		}
	}

	bool readHeader (ref Group grp)
	{
		if (grp != Group(2, "HEADER")) return false;

		do
		{
			nextGroup (grp);
		}
		while (!isEOS(grp));

		return true;
	}

	bool readClasses (ref Group grp)
	{
		if (grp != Group(2, "CLASSES")) return false;

		do
		{
			nextGroup (grp);
		}
		while (!isEOS(grp));

		return true;
	}

	bool readTables (ref Group grp)
	{
		if (grp != Group(2, "TABLES")) return false;

		nextGroup (grp);

		while (!isEOS(grp))
		{
	        readLayer(grp) ||
			nextGroup(grp);
		}

		return true;
	}

	bool readLayer (ref Group grp)
	{
		if (grp != Group (0, "LAYER")) return false;

		string name;	// name
		int index;		// color index

		nextGroup(grp);

		while (grp.first != 0)
		{
			parseGroup (grp, 2, name);
			parseGroup (grp, 62, index);
			nextGroup (grp);
		}

		if (index < 0)   index = 0;
		if (index > 255) index = 255;

		if (_canvas.layerTable().find(name) is null)
		{
			WDLayer l = _canvas.layerTable().create (name);
			l.setColor (dxf_colortable(index));
		}

		return true;
	}

// BLOCKS Section

	bool readBlocks (ref Group grp)
	{
		if (grp != Group(2, "BLOCKS")) return false;

		while (!isEOS(grp))
		{
	        readBlock(grp) ||
			nextGroup(grp);
		}

		return true;
	}

	bool readBlock (ref Group grp)
	{
		if (grp != Group (0, "BLOCK")) return false;

		double x;
		double y;
		string name;	// name
		string layer;

		nextGroup(grp);

		while (grp.first != 0)
		{
			parseGroup (grp, 10, x);
			parseGroup (grp, 20, y);
			parseGroup (grp, 8, layer);
			parseGroup (grp, 3, name);
			nextGroup (grp);
		}

		WDBlock block = null;

		if (_canvas.findBlock(name) is null) block =  _canvas.createBlock (name, DPair(x, y));

		if (block is null)
		{
			whereami("Could not create block \"%s\"", name);
			while (!isEOB(grp))
			{
				nextGroup(grp);
			}
		}
		else
		{
			while (!isEOB(grp))
			{
				readEntity (grp, block);
			}
		}
		return true;
	}

	bool readEntities (ref Group grp)
	{
		if (grp != Group(2, "ENTITIES")) return false;

		auto table = _canvas.layerTable();
		while (!isEOS(grp))
		{
			readEntity (grp, table);
		}
		return true;
	}

	void readEntity (ref Group grp, WDLayerTable table)
	{
		readLine       (grp, table) ||
		readLwPolyLine (grp, table) ||
		readPoint      (grp, table) ||
		readCircle     (grp, table) ||
		readArc        (grp, table) ||
		readText       (grp, table) ||
		readMText      (grp, table) ||
		readInsert     (grp, table) ||
		readDimension  (grp, table) ||
		readXLine      (grp, table) ||
		readRay        (grp, table) ||
		readEllipse    (grp, table) ||
		ignoreEntity   (grp, table);
	}

	bool[string] unknown_entities;

	void ignoreEntity (ref Group grp, WDLayerTable table)
	{
		if (grp.second !in unknown_entities)
		{
			unknown_entities[grp.second] = true;
			stderr.writeln ("Unknown entity \"" ~ grp.second ~ "\"");
		}

		do // skip entity parameters
		{
			nextGroup (grp);
		}
		while (grp.first != 0);
	}

	bool readLine (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "LINE")) return false;

		double x0, y0, x1, y1;
		string layer;  // layer name

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||
			parseGroup (grp, 20, y0) ||
			parseGroup (grp, 11, x1) ||
			parseGroup (grp, 21, y1) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (x0.isReal && y0.isReal && x1.isReal && y1.isReal && layer != "")
		{
			table.addItemToLayer (new WDLine (DPair (x0, y0), DPair (x1, y1)), layer);
		}
		return true;
	}

	bool readLwPolyLine (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "LWPOLYLINE")) return false;

		WDTuple!(DPair, double)[] points;

		int flag = 0;	// Polyline flag 0 (open), 1 (open) or 128
		string layer;	// layer name
		double x, y, b;	// b is bulge

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x) ||
			parseGroup (grp, 20, y) ||
			parseGroup (grp, 42, b) ||
			parseGroup (grp, 70, flag) ||
			parseGroup (grp, 8, layer);

			if (areReal (x, y))
			{
				points ~= wdTuple(DPair (x, y), 0.0);
				x = y = double.nan;
			}

			if (isReal (b) && points.length > 0)
			{
				points[$-1].second = b;
				b = double.nan;
			}
		}
		while (grp.first != 0);

		WDPolylineType pt = (flag==0 ? WDPolylineType.OPEN : WDPolylineType.CLOSED);

		// it's a polyline, so it should have at least a start and an end
		if (points.length >= 2 && layer != "")
		{
			auto item = new WDPolyline (points, pt);
			table.addItemToLayer (item, layer);
		}
		return true;
	}

	bool readPoint (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "POINT")) return false;

		double x0, y0;
		string layer;  // layer name

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||
			parseGroup (grp, 20, y0) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (areReal (x0, y0) && layer != "")
		{
			table.addItemToLayer (new WDMarker (DPair (x0, y0)), layer);
		}
		return true;
	}

	bool readCircle (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "CIRCLE")) return false;

		double x, y, r, a0, a1;
		string layer;

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x) ||
			parseGroup (grp, 20, y) ||
			parseGroup (grp, 40, r) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (areReal (x, y, r) && layer != "")
		{
			table.addItemToLayer (new WDCircle (DPair (x, y), r), layer);
		}
		return true;
	}

	bool readArc (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "ARC")) return false;

		double x, y, r, a0, a1;
		string layer;

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x) ||
			parseGroup (grp, 20, y) ||
			parseGroup (grp, 40, r) ||
			parseGroup (grp, 50, a0) ||
			parseGroup (grp, 51, a1) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (areReal (x, y, r, a0, a1) && layer != "")
		{
			table.addItemToLayer (new WDArc (DPair (x, y), r, a0, a1), layer);
		}
		return true;
	}

	bool readText (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "TEXT")) return false;

		double x0, y0, x1, y1;
		double he=Settings.textSize;
		double an=0.0;
		int ha=0, va=0;
		string lay, tex;

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||	// only used for (ha=0 and va=0), align and fit
			parseGroup (grp, 20, y0) ||	// ditto
			parseGroup (grp, 11, x1) ||	// main anchor point
			parseGroup (grp, 21, y1) ||	// ditto
			parseGroup (grp, 40, he) ||	// height
			parseGroup (grp, 50, an) ||	// angle
			parseGroup (grp, 1, tex) ||	// the text
			parseGroup (grp, 72, ha) ||	// horizontal alignment
			parseGroup (grp, 73, va) ||	// vertical alignment
			parseGroup (grp, 8, lay);
		}
		while (grp.first != 0);

//	ha  acad		wildcad
//	0	left		left
//	1	center		hcenter
//	2	right		right
//	3	align		left/base width=pdist((x0,y0)(x1,y1)), height=0
//	4	middle		hcenter/vcenter
//	5	fit			left/base + width=pdist((x0,y0)(x1,y1))
//  va
//	0	base		base
//	1	bottom		bottom
//	2	middle		vcenter
//	3	top			top

		auto al = WDTextAlign.LEFT | WDTextAlign.BASE;	// acad default
		auto x = x1;
		auto y = y1;
		auto wi = 0.0;

		if (ha == 0 && va == 0)
		{
			al = WDTextAlign.LEFT | WDTextAlign.BASE;
			x = x0;
			y = y0;
		}
		else
		{
			switch (ha)
			{
			case 3:	// align
				wi = pdist(DPair(x0,y0), DPair(x1,y1));
				he = 0.0;
				break;
			case 5:	// fit
				wi = pdist(DPair(x0,y0), DPair(x1,y1));
				break;
			default:
				final switch (ha)
				{
				case 1, 4:
					al = (al&WDTextAlign.VMASK) | WDTextAlign.HCENTER;
					break;
				case 2:
					al = (al&WDTextAlign.VMASK) | WDTextAlign.RIGHT;
					break;
				}

				final switch (va)
				{
				case 1:
					al = (al&WDTextAlign.HMASK) | WDTextAlign.BOTTOM;
					break;
				case 2:
					al = (al&WDTextAlign.HMASK) | WDTextAlign.VCENTER;
					break;
				case 3:
					al = (al&WDTextAlign.HMASK) | WDTextAlign.TOP;
					break;
				}
			}
		}

		if (areReal (x, y, he, an) && lay != "" && tex != "")
		{
			table.addItemToLayer (new WDText (tex, DPair(x,y), he, an, al), lay);
		}

		return true;
	}

	bool readMText (ref Group grp, WDLayerTable table)
	{
		return false;
	}

	bool readInsert (ref Group grp, WDLayerTable table)
	{
		return false;
	}

	bool readDimension (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "DIMENSION")) return false;

		double x0, y0, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, an;
		int tp = 0;
		string lay, tex;

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||		// definition point
			parseGroup (grp, 20, y0) ||
			parseGroup (grp, 11, x1) ||		// middle point of text
			parseGroup (grp, 21, y1) ||
			parseGroup (grp, 12, x2) ||		// baseline and continue
			parseGroup (grp, 22, y2) ||
			parseGroup (grp, 13, x3) ||		// def point for linear and angular dim
			parseGroup (grp, 23, y3) ||
			parseGroup (grp, 14, x4) ||		// def point for linear and angular dim
			parseGroup (grp, 24, y4) ||
			parseGroup (grp, 15, x5) ||		//
			parseGroup (grp, 25, y5) ||
			parseGroup (grp, 50, an) ||		// angle
			parseGroup (grp, 70, tp) ||		// dimension type
//			parseGroup (grp, 2, blk) ||		// name of block that contains dimension drawing
			parseGroup (grp, 1, tex) ||		// text explicitly entered by user
			parseGroup (grp, 8, lay);
		}
		while (grp.first != 0);


		tp &= 31; // filter out flags, that I don't understand...

		if (tp == 0) // Rotated, Horizontal, or Vertical
		{
			auto type = WDDimensionType.ALIGNED;

			// actually, only NaN and 90 are used for an
			if      ((an==0) || (an==180) || an.isNaN) type = WDDimensionType.HORIZONTAL;
			else if ((an==90) || (an==270)) type = WDDimensionType.VERTICAL;
			else
			{
				whereamif ("%s", an);
//				todo: dimension is rotated, not aligned
			}

			auto item = new WDDimension (type,
				DPair (x4, y4), DPair (x3, y3), DPair (x0, y0),
				Settings.dimTextSize, tex);

			table.addItemToLayer (item, lay);
		}
		else if (tp == 1) // Aligned
		{
			auto item = new WDDimension (WDDimensionType.ALIGNED,
				DPair (x4, y4), DPair (x3, y3), DPair (x0, y0),
				Settings.dimTextSize, tex);

			table.addItemToLayer (item, lay);
		}
/+
		else if (tp == 2) // Angular
		{
		}
		else if (tp == 3) // Diameter
		{
		}
		else if (tp == 4) // Radius
		{
		}
		else if (tp == 5) //  Angular 3 point
		{
		}
		else if (tp == 6) // Ordinate
		{
		}
+/
		return true;
	}

	bool readXLine (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "XLINE")) return false;

		double x0, y0, x1, y1;
		string layer;  // layer name

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||
			parseGroup (grp, 20, y0) ||
			parseGroup (grp, 11, x1) ||
			parseGroup (grp, 21, y1) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (areReal (x0, y0, x1, y1) && layer != "")
		{
			table.addItemToLayer (new WDXLine (DPair (x0, y0), DPair (x1, y1)), layer);
		}
		return true;
	}

	bool readRay (ref Group grp, WDLayerTable table)
	{
		if (grp != Group (0, "RAY")) return false;

		double x0, y0, x1, y1;
		string layer;  // layer name

		do
		{
			nextGroup (grp);

			parseGroup (grp, 10, x0) ||
			parseGroup (grp, 20, y0) ||
			parseGroup (grp, 11, x1) ||
			parseGroup (grp, 21, y1) ||
			parseGroup (grp, 8, layer);
		}
		while (grp.first != 0);

		if (areReal (x0, y0, x1, y1) && layer != "")
		{
			table.addItemToLayer (new WDRay (DPair (x0, y0), DPair (x1, y1)), layer);
		}
		return true;
	}

	bool readEllipse (ref Group grp, WDLayerTable table)
	{
		return false;
	}

	bool readObjects (ref Group grp)
	{
		if (grp != Group(2, "OBJECTS")) return false;

		do
		{
			nextGroup (grp);
		}
		while (!isEOS(grp));

		return true;
	}
}