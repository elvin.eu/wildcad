import std.stdio;
import std.range;
import std.math: abs;

import wcw.auxiliary;
import wcw.color;

import canvas;
import layertable;
import settings;
import auxiliary;
import layer;
import view;
import dxfreader;
import item;
import line;
import arc;
import circle;
import curve;

int dxf_color_index (Color color)
{
	int find_index (int start, int end)
	{
		int index;
		int deviation = int.max;

		for (int i=start; i<end; i++)
		{
			auto c = dxf_colortable(i);
			int dr = abs(c.red - color.red);
			int dg = abs(c.green - color.green);
			int db = abs(c.blue - color.blue);
			int d = dr + dg + db;

			if (d < deviation)
			{
				if (deviation == 0)return i;

				deviation = d;
				index = i;
			}
		}
		return index;
	}

	int avg = (color.red+color.green+color.blue) / 3;

	if (abs(color.red-avg) < 20 && abs(color.green-avg) < 20 && abs(color.blue-avg) < 20 )
	{
		return find_index (250, 256); 	// indexes > 250 are gray
	}
	else
	{
		return find_index (1, 250);
	}
}

class DXFWriter
{
public:
	static bool write  (File f, WDCanvas cv)
	{
		auto w = new DXFWriter (f, cv);

		w.writeHeader ();
//		w.writeClasses ();
		w.writeTables ();
		w.writeBlocks ();
		w.writeEntities ();
		w.writeObjects ();
		w.write_pair (0, "EOF");
		return true;
	}

private:
	WDCanvas _canvas;
	File    _file;

	this (File f, WDCanvas cv)
	{
		_file = f;
		_canvas = cv;
	}


	void write_pair(T)(int n, T v)
	{
		_file.writef ("%3d\r\n", n);
		_file.writef ("%s\r\n", v);
	}

	void section () {write_pair (0, "SECTION");}
	void endsec ()  {write_pair (0, "ENDSEC");}

	void writeHeader ()
	{
		section();

		write_pair (2, "HEADER");
		write_pair (9, "$ACADVER");
		write_pair (1, "AC1006");		// R10

		auto ex = _canvas.extents;
		write_pair (9, "$EXTMIN");
		write_pair (10, ex.x0);
		write_pair (20, ex.y0);
		write_pair (30, 0.0);
		write_pair (9, "$EXTMAX");
		write_pair (10, ex.x1);
		write_pair (20, ex.y1);
		write_pair (30, 0.0);

		endsec();
	}

	void writeTables   ()
	{
		section();
		write_pair (2, "TABLES");
		writeTable_Layers ();
		endsec();
	}

	void writeTable_Layers ()
	{
		write_pair (0, "TABLE");
		write_pair (2, "LAYER");
//		5 handle
//		100 subclass
		write_pair (70, _canvas.layerTable.list.length);

		foreach (layer; _canvas.layerTable.list)
		{
			write_pair (0, "LAYER");
			write_pair (2, layer.name);
			write_pair (70, ((layer.isHidden?1:0)|(layer.isLocked?4:0)));
			write_pair (62, dxf_color_index (layer.color));
			write_pair (6, "CONTINUOUS");

		}
		write_pair (0, "ENDTAB");
	}

	void writeBlocks   ()
	{
		section();
		_file.writeln ("  2\nBLOCKS");
		endsec();
	}

	void writeEntities ()
	{
		section();
		_file.writeln ("  2\nENTITIES");
		writeLayerItems__ (_canvas.layerTable);
		endsec();
	}

	void writeLayerItems__ (WDLayerTable layertable)
	{
		foreach (layer; layertable.list)
		{
			if (layer.empty) continue;

			// first for not selected items
			foreach (item; layer.items[]) writeItem (item, layer);
			// then for selected items
			foreach (item; layer.selected[]) writeItem (item, layer);
		}
	}

	void writeItem (WDItem item, WDLayer layer)
	{
		if (typeid(item) == typeid(WDLine))
		{
			auto line = cast(WDLine)item;
			auto s = line.start;
			auto e = line.end;
			write_pair (0, "LINE");
			write_pair (8, layer.name);
			write_pair (10, s.x);
			write_pair (20, s.y);
			write_pair (30, 0.0);
			write_pair (11, e.x);
			write_pair (21, e.y);
			write_pair (31, 0.0);
		}
		else if (typeid(item) == typeid(WDArc))
		{
			auto arc = cast(WDArc)item;
			auto c = arc.center;
			write_pair (0, "ARC");
			write_pair (8, layer.name);
			write_pair (10, c.x);
			write_pair (20, c.y);
			write_pair (30, 0.0);
			write_pair (40, arc.radius);
			write_pair (50, arc.startAngle);
			write_pair (51, arc.endAngle);

		}
		else if (typeid(item) == typeid(WDCircle))
		{
			auto circle = cast(WDCircle)item;
			auto c = circle.center;
			write_pair (0, "CIRCLE");
			write_pair (8, layer.name);
			write_pair (10, c.x);
			write_pair (20, c.y);
			write_pair (30, 0.0);
			write_pair (40, circle.radius);
		}
		else if (typeid(item) == typeid(WDCurve))
		{
			auto curve = cast(WDCurve)item;
			DPair[] points = curve.convertToPoints(50);	// currently a function that is unique to WDCurve
			write_pair (0, "LWPOLYLINE");
			write_pair (90, points.length);
			write_pair (70, 0);
			foreach (p; points)
			{
				write_pair (10, p.x);
				write_pair (20, p.y);
			}
		}
	}

	void writeObjects  ()
	{
		section();
		_file.writeln ("  2\nOBJECTS");
		endsec();
	}
}
