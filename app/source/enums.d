enum WDFlagOp {SET, ADD, DEL}

enum WDSnapType {NOSNAP, GRID, ITEM, FREEHAND}

enum WDTextAlign
{
	DEFAULT = 0x11,
	CENTER  = 0x22,
	LEFT = 0x01, HCENTER = 0x02, RIGHT  = 0x03,              HMASK = 0x0f,
	TOP  = 0x10, VCENTER = 0x20, BOTTOM = 0x30, BASE = 0x40, VMASK = 0xf0,
}


enum WDExpect {NOTHING, COMMAND, COORDINATE}

enum WDItemSnap
{
	NONE          = 0x0000,
	ALL           = 0x00ff,
	NEAREST       = 0x0001,
	END           = 0x0002,
	MIDDLE        = 0x0004,
	CENTER        = 0x0008,
	CARDINAL      = 0x0010,
	INTERSECTION  = 0x0020,
	PERPENDICULAR = 0x0040,
	TANGENT       = 0x0080,

	GRIP          = 0x8000
}

enum WDInfo {LAYERS, VIEWS, ITEMS, BLOCKS, CANVAS, SETTINGS, DIRECTORY}

enum WDDirection {UP, DOWN, FRONT, BACK}

enum WDDimensionType
{
	AUTO = 0, HORIZONTAL = 1, VERTICAL = 2, ALIGNED = 3
}

enum WDPolylineType {OPEN, CLOSED}

enum WDDimensionStyle {OBLIQUE, ARROW}

enum WDPageUnit {MM, INCH}

enum WDMouseMode {NONE, COORD, SELECT, PICK, PAN}

enum WDFileType {NOTYPE, WDFTYPE, DXFTYPE}

enum WDLayerAttribute
{
	NONE   = 0x00,
	ACTIVE = 0x80,    // there can be only one active layer
	MASK   = ~ACTIVE, // ACTIVE is not controlled by layers
	LOCKED = 0x01,    // visible and snappable, not moveable
	FROZEN = 0x02,    // visible, not snappable nor moveable
	HIDDEN = 0x04,    // invisible, not snappable nor moveable
}

// the coordinate modes, can be a combination of any of them
// BASE:  this coordinate is used as a base point for ther next relative c.
// ORTHO: in orthogonal mode, the mouse gets only c's with dx=0 or dy=0
// FREE:  no snapping allowed for this (e.g. for zoomimg, panning)
enum WDCoordMode {NONE=0, BASE=1, ORTHO=2, FREE=4}

enum Pending_Action {CREATE, REMOVE, SWITCH, RENAME, COLOR, ARRANGE, ATTRIBUTE, INSERT, UPDATE}
