import std.stdio;
import std.string;
import std.math;
import std.signals;
import std.conv;

import wcw;

import canvas;
import pending;
import auxiliary;
import enums;
import token;

import arc;
import curve;
import dimension;
import circle;
import line;
import marker;
import ray;
import text;
import xline;

import settings;
import reference;
import page;

bool match (cstring word, cstring symbol, ulong minlen)
{
	if (minlen==0) minlen = symbol.length;

	if ((word.length >= minlen) && symbol.startsWith(word))
		return true;
	else
		return false;
}

class WDInterpreter
{
private:
	WDCanvas _canvas;

	Directory _directory;
	Pending[] _pending;

	cstring _hint;           // message to the outside
	cstring _input;          // string to parse
	cstring _word;           // currently parsed word
	size_t _inpos;			 // current position int _input

	cstring _lastcommand;    // last recognised instruction

	bool _temporary;        // temporary interpreter mode - different from final mode
	bool _mousecoord;       // tells coordinate(), if mouse coordinates can be taken from
							// current mouse position. Only once allowed per parse()
	bool    _executed;      // if a Coord has already executed the command,
							// this flag hinders End executing again
	DPair  _basepoint = DPair(0,0);       // base coordinate for relative and orthogonal modes

//	debug (inpr) uint _indent;

public:
	mixin Signal!(cstring) notice;
	mixin Signal!(DPair) basepointChanged;

	this (WDCanvas c)
	{
		assert (c);
		_canvas = c;

		Token t1, t2, t3, t4;

		_directory = new Directory;
		alias dir=_directory;

		alias NONE  = WDCoordMode.NONE;
		alias BASE  = WDCoordMode.BASE;
		alias ORTHO = WDCoordMode.ORTHO;
		alias FREE  = WDCoordMode.FREE;

		dir.entry("arc", 2, &cmd_arc).fork(t1);
			t1.coordinate("center", BASE)
				.coordinate("start", BASE|ORTHO)
				.coordinate("end", BASE|ORTHO)
				.end;
			t1.symbol("3points", 2)
				.coordinate("start", BASE)
				.coordinate("mid", BASE|ORTHO)
				.coordinate("end", BASE|ORTHO)
				.end;

//		dir.symbol("block", 2).fork(t1);
//			t1.symbol("create", 1)
//				.name("name")
//				.coordinate ("base")
//				.end (&cmd_block_create);
//			t1.symbol("remove")
//				.name("name")
//				.end (&cmd_block_remove);
//			t1.symbol("rename")
//				.name("name")
//				.end (&cmd_block_rename);
//			t1.symbol("edit")
//				.name("name")
//				.end (&cmd_block_edit);
//			t1.symbol ("insert", 1)
//				.name("name")
//				.coordinate ("base", BASE|ORTHO, &cmd_block_insert).fork(t1);
//					t1.coordinate ("base", BASE|ORTHO, &cmd_block_insert).add(t1);
//					t1.end (&cmd_block_insert);

		dir.entry ("canvas", 2, &cmd_canvas).end;

		dir.entry("circle", 2, &cmd_circle).fork(t1);
			t1.coordinate("center", BASE).fork(t2);
				t2.coordinate("point", BASE|ORTHO)
					.end;
				t2.decimal("diameter")
					.end;
				t2.symbol("radius", 1)
					.decimal("radius")
					.end;
			t1.symbol("2points", 2)
				.coordinate("first", BASE)
				.coordinate("second", BASE|ORTHO)
				.end;
			t1.symbol("3points", 2)
				.coordinate("first", BASE)
				.coordinate("second", BASE|ORTHO)
				.coordinate("third", BASE|ORTHO)
				.end;

		dir.entry("close", 0, &cmd_close).end;

		dir.entry("copy", 2, &cmd_copy).fork(t1);
			t1.coordinate ("anchor", NONE).end;
			t1.symbol ("undo", 1).end;
			t1.end;

		dir.entry("curve", 3, &cmd_curve)
			.coordinate("start", BASE)
			.coordinate("end", BASE|ORTHO)
			.coordinate("control1", BASE)
			.fork(t1);

			t1.coordinate("control2", BASE)
				.end;
			t1.end;

		dir.entry("cut", 3, &cmd_cut).fork(t1);
			t1.coordinate ("anchor", NONE).end;
			t1.end;

		dir.entry ("dimension", 2, &cmd_dimension).fork(t1);

				t2 = new Coordinate("from");
				t2.coordinate("to")
					.coordinate("line", NONE)
					.fork(t3);
					t3.name("text").end;
					t3.end;

			t1.symbol ("auto", 2).add(t2);
			t1.symbol ("horizontal", 1).add(t2);
			t1.symbol ("vertical", 1).add(t2);
			t1.symbol ("aligned", 2).add(t2);
			t1.add(t2);

		dir.entry("erase", 0, &cmd_erase).end;


		dir.entry ("extend", 2, &cmd_extend).fork(t2);
				t1 = new Fork;
				t1.pick("item").add(t1);
				t1.end;
			t2.symbol("extended", 1).add(t1);
			t2.pick("item").add(t1);

//		dir.symbol("grid", 2).fork(t1);
//			t1.decimal("x").end(&cmd_grid_x);
//			t1.couple("x,y").end(&cmd_grid_xy);
//			t1.symbol("*").decimal("factor").end(&cmd_grid_mul);
//			t1.symbol("/").decimal("factor").end(&cmd_grid_div);


		dir.entry("grid", 2, &cmd_grid).fork(t1);
			t1.decimal("x").end;
			t1.couple("x,y").end;
			t1.symbol("*").decimal("factor").end;
			t1.symbol("/").decimal("factor").end;

		dir.entry("layer", 2, &cmd_layer).fork(t1);
			t1.symbol("switch",   1).name("name").end;
			t1.symbol("create",   1).name("name").end;
			t1.symbol("remove"     ).name("name").end;
			t1.symbol("rename",   3).name("new name").end;
			t1.symbol("move",     1).fork(t2);
				t2.symbol("up",     1).fork(t3);
					t3.integer("count").end;
					t3.end;
				t2.symbol("down",   1).fork(t3);
					t3.integer("count").end;
					t3.end;
				t2.symbol("top",    1).fork(t3);
					t3.integer("count").end;
					t3.end;
				t2.symbol("bottom", 1).fork(t3);
					t3.integer("count").end;
					t3.end;
			t1.symbol("color", 2).fork(t2);
				t2.rgb("rrggbb").end;
				t2.name ("string").end;
			t1.symbol("hide",     1).end;
			t1.symbol("freeze",   1).end;
			t1.symbol("lock",     1).end;
			t1.symbol("enable",   1).end;

		dir.entry("line", 2, &cmd_line)
			.coordinate("start", BASE)
			.coordinate("next", BASE|ORTHO)
			.fork(t1);
				t1.coordinate("next", BASE|ORTHO).add(t1);
				t1.end;

		dir.entry("list", 3, &cmd_list).fork(t1);
			t1.symbol("layers", 1).end;
//			t1.symbol("views",  1).end;
			t1.symbol("items",  1).end;
			t1.symbol("blocks", 1).end;
			t1.symbol("settings", 1).end;
			t1.symbol("canvas",1).end;
			t1.symbol("directory",1).end;

		dir.entry ("marker", 2, &cmd_marker)
			.coordinate ("point", BASE).fork(t1);
				t1.coordinate ("point", BASE).add(t1);
				t1.end;

		dir.entry ("measure", 2, &cmd_measure)
			.coordinate("start", BASE).fork(t1);
				t1.coordinate("next", BASE|ORTHO).add(t1);
				t1.end;

		dir.entry("mirror", 2, &cmd_mirror)
			.coordinate("first", BASE)
			.coordinate("second", ORTHO)
			.end;

		dir.entry("move", 2, &cmd_move).fork(t1);
			t1.symbol ("+").integer ("count")
				.coordinate ("from", BASE)
				.coordinate("to", BASE|ORTHO).end;
			t1.coordinate ("from", BASE)
				.coordinate("to", BASE|ORTHO).end;

		dir.entry("offset", 2, &cmd_offset).fork(t1);
			t1.symbol("*")
				.integer("count")
				.decimal("distance")
				.coordinate("side", NONE).end;
			t1.decimal("distance")
				.coordinate("side", NONE).end;

		dir.entry("open", 2, &cmd_open).fork(t1);
			t1.name("path").end;
			t1.end;

		dir.entry("orthogonal", 3, &cmd_orthogonal).fork(t1);
			t1.symbol("on").end;
			t1.symbol("off").end;

		dir.entry("origin", 3, &cmd_origin)
			.coordinate("new origin", WDCoordMode.NONE)
			.end;

		dir.entry("page", 3, &cmd_page)
			.couple ("size")
			.decimal ("scale")
			.coordinate ("origin", WDCoordMode.NONE)
			.end;


		dir.entry("pan", 3, &cmd_pan).fork(t2);

			t1 = new Fork();
				t1.symbol("up", 1).add(t1);
				t1.symbol("down", 1).add(t1);
				t1.symbol("left", 1).add(t1);
				t1.symbol("right", 1).add(t1);
				t1.end;

			t2.coordinate("from", FREE)
				.coordinate("to", FREE)
				.end;
			t2.add(t1);

		dir.entry("paste", 3, &cmd_paste).fork(t1);
			t1.coordinate ("anchor", NONE).end;
			t1.end;


		dir.entry("print", 2, &cmd_print).fork(t2);

			t1 = new Fork();
				t1.symbol("all", 1).end;
				t1.coordinate("from", FREE)
					.coordinate("to", FREE)
					.end;
				t1.end;

			t2.symbol("device", 1).name("device name").add(t1);
			t2.symbol("default").add (t1);

		dir.entry("quit", 0, &cmd_quit).end;

		dir.entry ("ray", 2, &cmd_ray).coordinate("start", BASE).fork(t1);
			t1.coordinate("direction", ORTHO).end;
			t1.decimal ("angle").end;

		dir.entry ("rotate", 2, &cmd_rotate).fork(t1);
			t1.symbol ("+").integer ("count").coordinate ("center", BASE).fork(t2);
				t2.decimal("angle").end;
				t2.coordinate ("from").coordinate ("to", NONE).end;
			t1.coordinate ("center", BASE).fork(t2);
				t2.decimal("angle").end;
				t2.coordinate ("from").coordinate ("to", NONE).end;


		dir.entry ("save", 2, &cmd_save).fork(t1);
			t1.symbol("as").fork(t2);
				t2.name("filename").end;
				t2.end;
			t1.end;

		dir.entry ("scale", 2, &cmd_scale).fork(t1);
			t1.symbol ("+").integer ("count").coordinate ("base", BASE).fork(t2);
				t2.decimal("factor").end;
				t2.coordinate ("from").coordinate ("to", NONE).end;
			t1.coordinate ("base", BASE).fork(t2);
				t2.decimal("factor").end;
				t2.coordinate ("from").coordinate ("to", NONE).end;

		dir.entry("snap", 2, &cmd_snap).fork(t1);
			t1.symbol ("-", 1).add(t1);
			t1.symbol ("+", 1).add(t1);
			t1.symbol ("toggle", 1).add(t1);
			t1.symbol ("near",   1).add (t1);
			t1.symbol ("end",    1).add(t1);
			t1.symbol ("mid",    1).add(t1);
			t1.symbol ("center", 1).add(t1);
			t1.symbol ("quad",   1).add(t1);
			t1.symbol ("int",    1).add(t1);
			t1.symbol ("square", 1).add(t1);
			t1.end;

		dir.entry ("text", 2, &cmd_text).fork(t1);
			t1.symbol ("height", 1).decimal("height").add(t1);
			t1.symbol ("angle", 1).decimal("angle").add(t1);
			t1.symbol ("left", 1).add(t1);
			t1.symbol ("right", 1).add(t1);
			t1.symbol ("top", 1).add(t1);
			t1.symbol ("baseline", 2).add(t1);
			t1.symbol ("bottom", 2).add(t1);
			t1.symbol ("center", 1).add(t1);
			t1.coordinate("base", NONE).name("text").end;


		dir.entry ("trim", 2, &cmd_trim).fork(t2);
				t1 = new Fork;
				t1.pick("item").add(t1);
				t1.end;
			t2.symbol("extended", 1).add(t1);
			t2.pick("item").add(t1);

//		dir.entry ("view", 2, &cmd_trim).fork(t1);
//			t1.symbol("switch",   1).name("name").end;
//			t1.symbol("create",   1).name("name").end;
//			t1.symbol("rename",   3).name("new name").end;
//			t1.symbol("remove"     ).name("name").end;
//			t1.symbol("update",   1).name("name").end;

		dir.entry ("xline", 2, &cmd_ray).coordinate("start", BASE).fork(t1);
			t1.coordinate("direction", ORTHO).end;
			t1.decimal ("angle").end;

		dir.entry("zoom", 2, &cmd_zoom).fork(t1);
			t1.decimal("factor").end;
			t1.coordinate("window", FREE).coordinate("other corner", FREE).end;
			t1.end;
	}

	WDCanvas canvas () {return _canvas;}

	bool executed () const {return _executed;}
	void setExecuted (bool e) {_executed = e;}

	cstring read_next ()
	{
		_word = nextWord (_input, _inpos);
		return _word;
	}

	cstring word () const {return _word;}

	const(char)[] lastCommand () const {return _lastcommand;}
	cstring hint () const {return _hint;}
	void setHint (cstring h) {_hint = h;}

	cstring word() {return _word;}

	bool temporary ()  const {return _temporary;}
	bool mousecoord () const {return _mousecoord;}
	void setMousecoord (bool m) {_mousecoord = m;}

	DPair basepoint () const {return _basepoint;}
	void setBasepoint () (DPair b)
	{
		_basepoint = b;
		basepointChanged.emit (b);
	}

	void push_pending (Pending pi)
	{
		_pending ~= pi;
	}

	bool parse (cstring input, bool temp=false)
	{
		_mousecoord = true;
		_executed = false;
		_temporary = temp;
		_input = input;
		_inpos = 0;


		// in _temporary mode, these values have to be restored at the end of this function
		DPair temp_base = _basepoint;		// mouse coordinate mode
		cstring temp_last = _lastcommand;	// last command that was parsed

		canvas.eraseTemporary (); // make sure there are no temporary items

		// delete temporary snapmode if active (if we're still on the same line it will
		// become activated while parsing the line but if we're on another line,
		// we will be glad to have the snapmode cleared)

		canvas.snapOn (false);
		canvas.setMouseHint (WDMouseMode.NONE);

		if (read_next.startsWith ("#")) return true;	// ignore comments

		auto prc = _directory.parse (this);
		_lastcommand = Directory.lastCommand;

		if (_temporary == false)
		{
			if (prc == ParseRC.FALSE) _hint = "Error";
			else                      _hint = "OK";
		}
		else
		{
			_lastcommand = temp_last;
			_basepoint = temp_base;
		}


		if (prc == ParseRC.FALSE) return false;
		else                      return true;
	}

	bool execute ()
	{
		bool rc = true;
		while (_pending.length > 0)
		{
			Pending p = _pending[$-1];
			_pending = _pending[0..$-1];

			// if execute() returns false, just delete the rest
			if (rc)
			{
				rc = p.execute();
				_hint = (p.message());
			}
		}
		return rc;
	}

	bool exetemp ()
	{
		bool rc = true;
		while (_pending.length > 0)
		{
			Pending p = _pending[$-1];
			_pending = _pending[0..$-1];

			if (rc)
			{
				rc = p.exetemp();
				_hint = (p.message());
			}
		}
		return rc;
	}

	double _arc_prev = 0.0;
	bool _arc_left;

	void cmd_arc ()
	{
		DPair center, start, end, mid;

		auto c = Coordinate.getValue ("center", center);
		auto s = Coordinate.getValue ("start", start);
		auto e = Coordinate.getValue ("end", end);
		auto m = Coordinate.getValue ("mid", mid);

		if (c && s && e)
		{
			auto rd = pdist(start, center);
			auto a0 = deg(arg (start-center));
			auto a1 = deg(arg (end-center));
			auto diff = a1 - a0;

			if (diff>0 && diff<90 && _arc_prev<=0 && _arc_prev>-90)
				_arc_left = true;
			if (diff<0 && diff>-90 && _arc_prev>=0 && _arc_prev<90)
				_arc_left = false;

			_arc_prev = diff;

			if (_arc_left)
				push_pending (new Pending_Item (canvas, new WDArc (center, rd, a0, a1)));
			else
				push_pending (new Pending_Item (canvas, new WDArc (center, rd, a1, a0)));
		}
		else if (s && m && e)
		{
			push_pending (new Pending_Item (canvas, new WDArc (start, mid, end)));
		}
	}

	void cmd_curve ()
	{
		bool s, e, c1, c2;
		DPair start, end, ctrl1, ctrl2;

		s = Coordinate.getValue ("start", start);
		e = Coordinate.getValue ("end", end);
		c1 = Coordinate.getValue ("control1", ctrl1);
		c2 = Coordinate.getValue ("control2", ctrl2);

		push_pending (new Pending_Item (canvas, new WDCurve (start, end, ctrl1, ctrl2)));
	}

	void cmd_block_create ()
	{
		auto base = Coordinate.getValue ("base");
		auto name = Name.getValue ("name");

		push_pending (new Pending_Block (Pending_Action.CREATE, canvas, name, base));
	}

	void cmd_block_remove ()
	{
		auto name = Name.getValue ("name");

		push_pending (new Pending_Block (Pending_Action.REMOVE, canvas, name));
	}

	void cmd_block_rename ()
	{
		auto name = Name.getValue ("name");

		push_pending (new Pending_Block (Pending_Action.RENAME, canvas, name));
	}

	void cmd_block_edit ()
	{
		auto name = Name.getValue ("name");

		push_pending (new Pending_Block (Pending_Action.SWITCH, canvas, name));
	}

	void cmd_block_insert ()
	{
		DPair base;
		auto name = Name.getValue ("name");

		while (Coordinate.getValue ("base", base))
		{
			push_pending (new Pending_Block (Pending_Action.INSERT, canvas, name, base));
		}
	}

	void cmd_canvas ()
	{
		push_pending (new Pending_Canvas (canvas));
	}

	void cmd_circle ()
	{
		bool c, p, r, d, f, s, t;
		DPair center, point, first, second, third;
		double diameter, radius;

		c = Coordinate.getValue ("center", center);
		p = Coordinate.getValue ("point", point);
		r = Decimal.getValue ("radius", radius);
		d = Decimal.getValue ("diameter", diameter);
		f = Coordinate.getValue ("first", first);
		s = Coordinate.getValue ("second", second);
		t = Coordinate.getValue ("third", third);

		if (p)      push_pending (new Pending_Item (canvas, new WDCircle (center, pdist(center, point))));
		else if (r) push_pending (new Pending_Item (canvas, new WDCircle (center, radius)));
		else if (d) push_pending (new Pending_Item (canvas, new WDCircle (center, diameter/2)));
		else if (f && s)
		{
			if (t)  push_pending (new Pending_Item (canvas, new WDCircle (first, second, third)));
			else    push_pending (new Pending_Item (canvas, new WDCircle (first, second)));
		}
	}

	void cmd_close ()
	{
		push_pending (new Pending_Close (canvas));
	}

	void cmd_copy ()
	{
		DPair anchor;
		bool undo;

		Coordinate.getValue ("anchor", anchor);
		Symbol.getValue ("undo", undo);

		if (undo) push_pending (new Pending_CopyUndo (canvas));
		else      push_pending (new Pending_Copy (canvas, anchor));
	}

	void cmd_cut ()
	{
		DPair anchor;
		Coordinate.getValue ("anchor", anchor);
		push_pending (new Pending_Copy (canvas, anchor, true));
	}

	void cmd_dimension ()
	{
		static WDDimensionType dt = WDDimensionType.AUTO;
		DPair st, en, ln;
		double sz = 5;
		cstring tx;

		if      (Symbol.getValue ("auto"))       dt = WDDimensionType.AUTO;
		else if (Symbol.getValue ("horizontal")) dt = WDDimensionType.HORIZONTAL;
		else if (Symbol.getValue ("vertical"))   dt = WDDimensionType.VERTICAL;
		else if (Symbol.getValue ("aligned"))    dt = WDDimensionType.ALIGNED;

		if (!Coordinate.getValue ("from", st)) return;
		if (!Coordinate.getValue ("to", en)) return;
		if (!Coordinate.getValue ("line", ln)) return;

		Name.getValue ("text", tx);

		push_pending (new Pending_Item (canvas, new WDDimension (dt, st, en, ln, sz, tx)));
	}

	void cmd_erase()
	{
		push_pending (new Pending_Erase (canvas));
	}

	void cmd_extend
	{
		push_pending (new Pending_Fit (canvas, false, Symbol.getValue("extended")));
	}

	void cmd_grid ()
	{
		bool g, x, f, m, d;
		DPair grid;
		double xgrid, factor;

		g = Couple.getValue ("x,y", grid);
		x = Decimal.getValue ("x", xgrid);
		m = Symbol.getValue ("*");
		d = Symbol.getValue ("/");
		f = Decimal.getValue ("factor", factor);

		if      (g) push_pending (new Pending_Grid (canvas, grid));
		else if (x) push_pending (new Pending_Grid (canvas, DPair (xgrid, xgrid)));
		else if (m && f) push_pending (new Pending_Grid (canvas, DPair (canvas.grid()*factor)));
		else if (d && f) push_pending (new Pending_Grid (canvas, DPair (canvas.grid()/factor)));
	}

	void cmd_layer ()
	{
		if (Symbol.getValue ("create"))
		{
			auto n = Name.getValue ("name");
			push_pending (new Pending_Layer (Pending_Action.CREATE, canvas, n));
		}
		else if (Symbol.getValue ("remove"))
		{
			auto n = Name.getValue ("name");
			push_pending (new Pending_Layer (Pending_Action.REMOVE, canvas, n));
		}
		else if (Symbol.getValue ("switch"))
		{
			auto n = Name.getValue ("name");
			push_pending (new Pending_Layer (Pending_Action.SWITCH, canvas, n));
		}
		else if (Symbol.getValue ("rename"))
		{
			auto n = Name.getValue ("new name");
			push_pending (new Pending_Layer (Pending_Action.RENAME, canvas, "", n));
		}
		else if (Symbol.getValue ("color"))
		{
			cstring name;
			uint rgb;

			RGB.getValue ("rrggbb", rgb) ||
			Name.getValue ("string", name);

			if (name != "")
			{
				if      (name == "red")    rgb = 0xff0000;
				else if (name == "green")  rgb = 0x00e000;
				else if (name == "blue")   rgb = 0x0000ff;
				else if (name == "brown")  rgb = 0xb06000;
				else if (name == "teal")   rgb = 0x008080;
				else if (name == "purple") rgb = 0x800080;
				else if (name == "gray")   rgb = 0x808080;
				else if (name == "grey")   rgb = 0x808080;
				else                       rgb = 0x000000;
			}

			push_pending (new Pending_Layer (canvas, Color(rgb)));
		}
		else if (Symbol.getValue ("move"))
		{
			WDDirection dir;

			if      (Symbol.getValue ("up"))     dir = WDDirection.UP;
			else if (Symbol.getValue ("down"))   dir = WDDirection.DOWN;
			else if (Symbol.getValue ("top"))    dir = WDDirection.FRONT;
			else if (Symbol.getValue ("bottom")) dir = WDDirection.BACK;

			int count = 1;
			Integer.getValue ("count", count);

			push_pending (new Pending_Layer (canvas, dir, count));
		}
		else if (Symbol.getValue ("hide"))
		{
			push_pending (new Pending_Layer (canvas, WDLayerAttribute.HIDDEN));
		}
		else if (Symbol.getValue ("freeze"))
		{
			push_pending (new Pending_Layer (canvas, WDLayerAttribute.FROZEN));
		}
		else if (Symbol.getValue ("lock"))
		{
			push_pending (new Pending_Layer (canvas, WDLayerAttribute.LOCKED));
		}
		else if (Symbol.getValue ("enable"))
		{
			push_pending (new Pending_Layer (canvas, WDLayerAttribute.NONE));
		}
	}

	void cmd_line ()
	{
		DPair s = Coordinate.getValue ("start");
		DPair n;
		while (Coordinate.getValue ("next", n))
		{
			push_pending (new Pending_Item (canvas, new WDLine (s, n)));
			s = n;
		}
	}

	void cmd_list ()
	{
		if      (Symbol.getValue ("layers"))    push_pending (new Pending_List (this, WDInfo.LAYERS));
		else if (Symbol.getValue ("views"))     push_pending (new Pending_List (this, WDInfo.VIEWS));
		else if (Symbol.getValue ("items"))     push_pending (new Pending_List (this, WDInfo.ITEMS));
		else if (Symbol.getValue ("blocks"))    push_pending (new Pending_List (this, WDInfo.BLOCKS));
		else if (Symbol.getValue ("settings"))  push_pending (new Pending_List (this, WDInfo.SETTINGS));
		else if (Symbol.getValue ("canvas"))    push_pending (new Pending_List (this, WDInfo.CANVAS));
		else if (Symbol.getValue ("directory")) push_pending (new Pending_List (this, WDInfo.DIRECTORY));
	}


	void list (int i)
	{
		cstring s;
		if      (i == WDInfo.LAYERS)    s.appendln (_canvas.activeBlock.info);
		else if (i == WDInfo.VIEWS)     s.appendln (_canvas.viewTable.info);
		else if (i == WDInfo.BLOCKS)    s.appendln (_canvas.blockTable.info);
		else if (i == WDInfo.ITEMS)     s.appendln (_canvas.listItems());
		else if (i == WDInfo.CANVAS)    s.appendln (_canvas.info);
		else if (i == WDInfo.SETTINGS)  s.appendln (Settings.info);
		else if (i == WDInfo.DIRECTORY)	s.appendln (_directory.hint);

		notice.emit (s);
	}

	void cmd_marker ()
	{
		DPair point;
		while (Coordinate.getValue ("point", point))
		{
			push_pending (new Pending_Item (canvas, new WDMarker (point)));
		}
	}

	void cmd_measure ()
	{
		DPair [] points;
		DPair n;

		points ~= Coordinate.getValue ("start");
		while (Coordinate.getValue ("next", n)) points ~= n;

		push_pending (new Pending_Measure (canvas, this, points));
	}

	void measure (DPair[] points)
	{
		string s;

		if (points.length < 2)
		{
			s = "need at least 2 points!\n";
		}
		else if (points.length < 3)
		{
			s = "distance: " ~ (pdist(points[1], points[0])).to!string ~
			    ", horizontal: " ~ (points[1].x-points[0].x).to!string ~
			    ", vertical: " ~ (points[1].y-points[0].y).to!string ~
			    ", angle: " ~ deg(arg(points[1]-points[0])).to!string ~ "°\n";
		}
		else
		{
			DPair p = points[0];
			DPair q = points[1];
			double A = 0.0;
			foreach (r; points[2..$])
			{
				A += (norm(q-p)*norm(r-p)*sin(arg(p-r)-arg(p-q))/2);
				q=r;
			}

			s ~= "A: " ~ A.to!string ~ "\n";
		}
		notice.emit (s);
	}

	void cmd_mirror ()
	{
		bool f, s;
		DPair first, second;

		f = Coordinate.getValue ("first", first);
		s = Coordinate.getValue ("second", second);

		if (f && s) push_pending (new Pending_Mirror (canvas, first, second-first));
	}

	void cmd_move ()
	{
		bool f, t;
		DPair from, to;
		int count = 0;

		Integer.getValue ("count", count);
		f = Coordinate.getValue ("from", from);
		t = Coordinate.getValue ("to", to);

		if (f && t) push_pending (new Pending_Move (canvas, to-from, count));
	}

	void cmd_offset ()
	{
		bool d, s;
		int count = 1;
		double dist;
		DPair side;

		Integer.getValue("count", count);
		d = Decimal.getValue("distance", dist);
		s = Coordinate.getValue ("side", side);

		if (d && s) push_pending (new Pending_Offset (canvas, dist, side, 1));
	}

	void cmd_open ()
	{
		cstring path = "";
		Name.getValue ("path", path);
		push_pending (new Pending_Open (canvas, path));
	}


	void cmd_orthogonal ()
	{
		bool on = Symbol.getValue ("on");
		push_pending (new Pending_Ortho (canvas, on));
	}

	void cmd_origin ()
	{
		DPair origin;
		if (!Coordinate.getValue ("new origin", origin)) return;

		push_pending (new Pending_Origin (canvas, origin));
	}

	void cmd_page ()
	{
		auto sz = Couple.getValue ("size");
		auto og = Coordinate.getValue ("origin");
		auto sc = Decimal.getValue ("scale");

		push_pending (new Pending_Item (canvas, new WDPage (sz, sc, og)));
	}

	void cmd_pan ()
	{
		DPair f, t;
		double x=0.0, y=0.0;

		if (Coordinate.getValue ("from", f) && Coordinate.getValue ("to", t))
		{
			push_pending (new Pending_Pan (canvas, f, t));
			return;
		}

		foreach (v; Symbol.values)
		{
			if      (v.first == "up")    y += 0.33;
			else if (v.first == "down")  y -= 0.33;
			else if (v.first == "left")  x -= 0.33;
			else if (v.first == "right") x += 0.33;
		}

		push_pending (new Pending_Pan (canvas, DPair (x, y)));
	}

	void cmd_paste ()
	{
		DPair anchor;
		Coordinate.getValue ("anchor", anchor);

		push_pending (new Pending_Paste (canvas, anchor));
	}

	void cmd_print ()
	{
		DPair f, t;
		cstring d;

		Coordinate.getValue("from", f);
		Coordinate.getValue("to", t);
		Name.getValue("device name", d);

		push_pending (new Pending_Print (canvas, f, t, d));
	}

	void cmd_quit ()
	{
		push_pending (new Pending_Quit (canvas));
	}

	void cmd_ray ()
	{
		bool s, d, a;
		DPair start, dir;
		double angle;

		s = Coordinate.getValue ("start", start);
		d = Coordinate.getValue ("direction", dir);
		a = Decimal.getValue ("angle", angle);



		if (s && d) push_pending (new Pending_Item (canvas, new WDRay (start, dir-start)));
		else if (a) push_pending (new Pending_Item (canvas, new WDRay (start, angle)));
	}

	void cmd_rotate ()
	{
		DPair center, from, to;
		double angle;
		int count = 0;

		Coordinate.getValue ("center", center);
		Integer.getValue ("count", count);
		Decimal.getValue ("angle", angle);
		Coordinate.getValue ("from", from);
		Coordinate.getValue ("to", to);

		if (from.isReal() && to.isReal()) angle = deg(arg(to-center)-arg(from-center));

		push_pending (new Pending_Rotate (canvas, center, angle, count));
	}

	void cmd_save ()
	{
		bool p, a;
		cstring path = "";

		p = Name.getValue ("filename", path);
		a = Symbol.getValue ("as");

		push_pending (new Pending_Save (canvas, path, a));
	}

//		dir.entry ("scale", 2, &cmd_scale).fork(t1);
//			t1.symbol("*").integer("count").coordinate("base", NONE).fork(t2);
//				t2.coordinate("from", BASE)
//					.coordinate("to", NONE)
//					.end;
//				t2.decimal("factor").end;
//			t1.coordinate("base", NONE).fork(t2);
//				t2.coordinate("from", BASE)
//					.coordinate("to", NONE)
//					.end;
//				t2.decimal("factor").end;

	void cmd_scale ()
	{
		bool b, f, t;
		DPair base, from, to;
		double factor;
		int count = 0;

		b = Coordinate.getValue ("base", base);
		f = Coordinate.getValue ("from", from);
		t = Coordinate.getValue ("to", to);
		Decimal.getValue ("factor", factor);
		Integer.getValue ("count", count);

		if (!b) return;
		if (f && t) factor = pdist(base, to) / pdist(base, from);
		if (!factor.isReal()) return;

		push_pending (new Pending_Scale (canvas, base, factor, count));
	}

	void cmd_snap ()
	{
		WDItemSnap mode;
		size_t i;
		char op = 't';

		foreach (v; Symbol.values)
		{
			if      (v.first == "near")   mode |= WDItemSnap.NEAREST;
			else if (v.first == "end")    mode |= WDItemSnap.END;
			else if (v.first == "mid")    mode |= WDItemSnap.MIDDLE;
			else if (v.first == "cent")   mode |= WDItemSnap.CENTER;
			else if (v.first == "card")   mode |= WDItemSnap.CARDINAL;
			else if (v.first == "int")    mode |= WDItemSnap.INTERSECTION;
			else if (v.first == "perp")   mode |= WDItemSnap.PERPENDICULAR;
			else if (v.first == "tang")   mode |= WDItemSnap.TANGENT;
			else if (v.first == "all")    mode =  WDItemSnap.ALL;
			else if (v.first == "none")   mode =  WDItemSnap.NONE;
		}

		push_pending (new Pending_Snap (canvas, mode));
	}

	void cmd_text ()
	{
		double an = 0.0;
		double he = canvas.textSize;
		DPair ba;
		cstring tx;
		WDTextAlign av = WDTextAlign.BOTTOM;
		WDTextAlign ah = WDTextAlign.LEFT;

		Decimal.getValue ("angle", an);
		Decimal.getValue ("height", he);
		Coordinate.getValue ("base", ba);
		Name.getValue ("text", tx);

		foreach (v; Symbol.values)
		{
			if      (v.first == "left")     ah = WDTextAlign.LEFT;
			else if (v.first == "right")    ah = WDTextAlign.RIGHT;
			else if (v.first == "top")      av = WDTextAlign.TOP;
			else if (v.first == "baseline") av = WDTextAlign.BASE;
			else if (v.first == "bottom")   av = WDTextAlign.BOTTOM;
			else if (v.first == "center")
			{
				ah = WDTextAlign.HCENTER;
				av = WDTextAlign.VCENTER;
			}
		}

		push_pending (new Pending_Item (canvas, new WDText (tx, ba, he, an, ah|av)));
		canvas.setTextSize (he);
	}

	void cmd_trim ()
	{
		push_pending (new Pending_Fit (canvas, true, Symbol.getValue("extended")));
	}

//	void cmd_view ()
//	{
//		cstring name = "";
//		Name.getValue ("name", name);
//		Name.getValue ("new name", name);

//		if      (Symbol.getValue ("switch") push_pending (new Pending_View (Pending_Action.SWITCH, canvas, name));
//		else if (Symbol.getValue ("create") push_pending (new Pending_View (Pending_Action.CREATE, canvas, name));
//		else if (Symbol.getValue ("rename") push_pending (new Pending_View (Pending_Action.RENAME, canvas, name));
//		else if (Symbol.getValue ("remove") push_pending (new Pending_View (Pending_Action.REMOVE, canvas));
//		else if (Symbol.getValue ("update") push_pending (new Pending_View (Pending_Action.UPDATE, canvas));
//	}

	void cmd_xline ()
	{
		bool s, d, a;
		DPair start, dir;
		double angle;

		s = Coordinate.getValue ("start", start);
		d = Coordinate.getValue ("direction", dir);
		a = Decimal.getValue ("angle", angle);

		if (s && d) push_pending (new Pending_Item (canvas, new WDXLine (start, dir-start)));
		else if (a) push_pending (new Pending_Item (canvas, new WDXLine (start, angle)));
	}

	void cmd_zoom ()
	{
		bool f, w, c;
		double factor;
		DPair window, corner;

		w = Coordinate.getValue ("window", window);
		c = Coordinate.getValue ("other corner", corner);
		f = Decimal.getValue ("factor", factor);

		if (w && c) push_pending (new Pending_Zoom (canvas, DRect (window, corner)));
		else if (f) push_pending (new Pending_Zoom (canvas, factor));
		else        push_pending (new Pending_Zoom (canvas));
	}


}
