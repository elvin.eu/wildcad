import auxiliary;
import item;
import line;
import xline;
import circle;
import arc;

import wcw.auxiliary;

DPair[] LineLine (WDLine l1, WDLine l2)
{
	auto p = intersect_xx (l1.start, l1.end, l2.start, l2.end);

	if (p.isReal) return [p];
	else          return [];
}

DPair[] LineXLine (WDLine l1, WDXLine x2)
{
	DPair[] i;
	auto p = intersect_xx (l1.start, l1.end, x2.start, x2.start+ x2.direction);
	if (p.isReal) i~= p;
	return i;
}

//DPair[] LineRay   (WDItem co1, WDItem co2)
//{
//	DPair[] i;
//	WDLine l1 = dynamic_cast<WDLine>(co1);
//	WDRay  r2 = dynamic_cast<WDRay>(co2);
//	DPair isec = Intersect::ll (l1.start, l1.end, r2.start, r2.start+r2.direction);
//	if (! isec.isNan) i.push_back (isec);
//	return i;
//}

DPair[] LineCircle   (WDLine l1, WDCircle c2)
{
	DPair[] i;
	auto p = intersect_lc (l1.start, l1.end, c2.center, c2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[1].isReal && p[0] != p[1]) i ~= p[1];
	return i;
}

DPair[] LineArc   (WDLine l1, WDArc a2)
{
	DPair[] i;
	auto p = intersect_lc (l1.start, l1.end, a2.center, a2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[1].isReal && p[0] != p[1]) i ~= p[1];
	return i;
}

DPair[] XLineXLine (WDXLine x1, WDXLine x2)
{
	DPair[] i;
	auto p = intersect_xx (x1.start, x1.start+x1.direction, x2.start, x2.start+x2.direction);
	if (p.isReal) i ~= p;
	return i;
}

//DPair[] XLineRay   (WDItem co1, WDItem co2)
//{
//	DPair[] i;
//	WDXLine x1 = dynamic_cast<WDXLine>(co1);
//	WDRay   r2 = dynamic_cast<WDRay>(co2);
//	DPair isec = Intersect::ll (x1.start, x1.start+x1.direction,
//											 r2.start, r2.start+r2.direction);
//	if (! isec.isNan) i.push_back (isec);
//	return i;
//}

DPair[] XLineCircle   (WDXLine x1, WDCircle c2)
{
	DPair[] i;
	auto p = intersect_lc (x1.start, x1.start+x1.direction, c2.center, c2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[0] != p[1] && p[1].isReal) i ~= p[1];
	return i;
}

DPair[] XLineArc   (WDXLine x1, WDArc a2)
{
	DPair[] i;

	auto p = intersect_lc (x1.start, x1.start+x1.direction, a2.center, a2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[0] != p[1] && p[1].isReal) i ~= p[1];

	return i;
}

//DPair[] RayRay (WDItem co1, WDItem co2)
//{
//	DPair[] i;
//	WDRay r1 = dynamic_cast<WDRay>(co1);
//	WDRay r2 = dynamic_cast<WDRay>(co2);
//	DPair isec = Intersect::ll (r1.start, r1.start+r1.direction,
//											 r2.start, r2.start+r2.direction);
//	if (! isec.isNan) i.push_back (isec);
//	return i;
//}

//DPair[] RayCircle   (WDItem co1, WDItem co2)
//{
//	DPair[] i;
//	std::pair <DPair, DPair> p;

//	WDRay    r1 = dynamic_cast<WDRay>(co1);
//	WDCircle c2 = dynamic_cast<WDCircle>(co2);

//	p = Intersect::lc (r1.start, r1.start+r1.direction,
//									c2.center, c2.radius);
//	if (!p.first.isNan) i.push_back (p.first);
//	if (!(p.first == p.second || p.second.isNan)) i.push_back (p.second);

//	return i;
//}

//DPair[] RayArc   (WDItem co1, WDItem co2)
//{
//	DPair[] i;
//	std::pair <DPair, DPair> p;

//	WDRay r1 = dynamic_cast<WDRay>(co1);
//	WDArc a2 = dynamic_cast<WDArc>(co2);

//	p = Intersect::lc (r1.start, r1.start+r1.direction,
//									a2.center, a2.radius);
//	if (!p.first.isNan) i.push_back (p.first);
//	if (!(p.first == p.second || p.second.isNan)) i.push_back (p.second);

//	return i;
//}

DPair[] CircleCircle (WDCircle c1, WDCircle c2)
{
	DPair[] i;
	auto p = intersect_cc (c1.center, c1.radius, c2.center, c2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[1].isReal && p[0] != p[1]) i ~= p[1];
	return i;
}

DPair[] CircleArc (WDCircle c1, WDArc a2)
{
	DPair[] i;
	auto p = intersect_cc (c1.center, c1.radius, a2.center, a2.radius);
	if (p[0].isReal) i ~= p[0];
	if (p[1].isReal && p[0] != p[1]) i ~= p[1];
	return i;
}

DPair[] ArcArc (WDArc a1, WDArc a2)
{
	DPair[] i;
	auto p = intersect_cc (a1.center, a1.radius, a2.center, a2.radius);
	if (p[0].isReal) i~= p[0];
	if (p[1].isReal && p[0] != p[1]) i ~= p[1];
	return i;
}


DPair[] intersections_all (WDItem i1, WDItem i2)
{
	assert (i1);
	assert (i2);

	auto t1 = typeid(i1);
	auto t2 = typeid(i2);

	if (t1 == typeid(WDLine))
	{
		if      (t2 == typeid(WDLine))   return LineLine   (cast(WDLine)i1, cast(WDLine)i2);
		else if (t2 == typeid(WDXLine))  return LineXLine  (cast(WDLine)i1, cast(WDXLine)i2);
//		else if (t2 == typeid(WDRay))    return LineRay    (i1, i2);
		else if (t2 == typeid(WDCircle)) return LineCircle (cast(WDLine)i1, cast(WDCircle)i2);
		else if (t2 == typeid(WDArc))    return LineArc    (cast(WDLine)i1, cast(WDArc)i2);
	}

	else if (t1 == typeid(WDXLine))
	{
		if      (t2 == typeid(WDLine))   return LineXLine   (cast(WDLine)i2, cast(WDXLine)i1);
		else if (t2 == typeid(WDXLine))  return XLineXLine  (cast(WDXLine)i1, cast(WDXLine)i2);
//		else if (t2 == typeid(WDRay))    return XLineRay    (i1, i2);
		else if (t2 == typeid(WDCircle)) return XLineCircle (cast(WDXLine)i1, cast(WDCircle)i2);
		else if (t2 == typeid(WDArc))    return XLineArc    (cast(WDXLine)i1, cast(WDArc)i2);
	}
//	else if (t1 == typeid(WDRay))
//	{
//		if      (t2 == typeid(WDLine))   return RayLine   (i1, i2);
//		else if (t2 == typeid(WDXLine))  return RayXLine  (i1, i2);
//		else if (t2 == typeid(WDRay))    return RayRay    (i1, i2);
//		else if (t2 == typeid(WDCircle)) return RayCircle (i1, i2);
//		else if (t2 == typeid(WDArc))    return RayArc    (i1, i2);
//	}
	else if (t1 == typeid(WDCircle))
	{
		if      (t2 == typeid(WDLine))   return LineCircle   (cast(WDLine)i2, cast(WDCircle)i1);
		else if (t2 == typeid(WDXLine))  return XLineCircle  (cast(WDXLine)i2, cast(WDCircle)i1);
//		else if (t2 == typeid(WDRay))    return CircleRay    (i1, i2);
		else if (t2 == typeid(WDCircle)) return CircleCircle (cast(WDCircle)i1, cast(WDCircle)i2);
		else if (t2 == typeid(WDArc))    return CircleArc    (cast(WDCircle)i1, cast(WDArc)i2);
	}
	else if (t1 == typeid(WDArc))
	{
		if      (t2 == typeid(WDLine))   return LineArc   (cast(WDLine)i2, cast(WDArc)i1);
		else if (t2 == typeid(WDXLine))  return XLineArc  (cast(WDXLine)i2, cast(WDArc)i1);
//		else if (t2 == typeid(WDRay))    return ArcRay    (i1, i2);
		else if (t2 == typeid(WDCircle)) return CircleArc (cast(WDCircle)i2, cast(WDArc)i1);
		else if (t2 == typeid(WDArc))    return ArcArc    (cast(WDArc)i1, cast(WDArc)i2);
	}

	DPair[] empty;
	return empty;
}

WDTuple!(DPair, double) intersection_nearest (WDItem i1, WDItem i2, DPair target)
{
	auto points = intersections_all (i1, i2);

	// find the nearest point from target
	DPair pn;
	double dn = double.infinity;

	foreach  (p; points)
	{
		auto d = pdist (p, target);
		if (!(d > dn)) 		// true, if dn is NaN
		{
			dn = d;
			pn = p;
		}
	}

	return wdTuple (pn, dn);
}