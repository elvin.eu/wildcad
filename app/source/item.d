import cairo;
import wcw;

import auxiliary;
import settings;
import painter;
import enums;
import intersection;

void item();

class WDItem
{
private:
	static int _count;

public:

	static int count () {return _count;}

	this ()
	{
		_count++;
	}

	abstract WDItem clone();

	cstring info () const
	{
		return toString;
	}

	override string toString () const {return "Item";}

	// draws an anchor grip at pos
	static void drawGrip (Painter p, CairoMatrix m, DPair pos, bool drag, double density)
	{
		double mx, my;
		int g = Settings.gripSize/2;
		m.transform_point (pos.x, pos.y, mx, my);
		int ix = int_round (mx);
		int iy = int_round (my);

		if (drag)
		{
			p.setColor (Settings.canvasColor.mix(Settings.dragColor, density));
			p.drawRectangle (ix-g, iy-g, g+g, g+g);
		}
		else
		{
			p.setColor (Settings.canvasColor.mix(Settings.gripColor, density));
			p.drawRectangle (ix-g, iy-g, g+g, g+g);
		}
	}

	// draws the item onto the screen
	// only WDReference currently needs the density parameter <d_>
	abstract void draw (Painter p, CairoMatrix matrix, double density=1.0);

	// true, if item supports submitted snapmode
	bool snapsTo (WDItemSnap s) const
	{
		return false;
	}

	// requested snap point nearest to <pos> if it exists or DPair(NAN,NAN)
	WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from=DPair()) const
	{
		return wdTuple (DPair(), double.infinity);
	}
/*
	// returns (1) grip point nearest to <pos>, (2) its distance to <pos>
	// and (3) the object's distance to <pos>
	// or (DPair(NAN,NAN), inf, inf) if no grip point is available
	WDTuple!(DPair, double, double) snapToGrip (DPair pos) const
	{
		return wdTuple (DPair(), double.infinity, double.infinity);
	}
*/

	/// draws all grips (draggable or not) of an item
	void drawGrips (Painter p, CairoMatrix m, double den=1.0) {}

	/// return nearest grip of an object, if there is any
	WDTuple!(DPair, double) snapToGrip (DPair target) const
	{
		return wdTuple (DPair(), double.infinity);
	}

	void clearGrips () {}

	/// toggles ((un-)makes draggable) the grip at target, if it exists
	bool toggleGrip (DPair target) {return false;}

	/// toggles ((un-)makes draggable) all existing grips in window
	bool toggleGrips (DRect window) {return false;}

	/// true, if component fully (<fi>=true) or partially (<fi>=false) included in <r>
	/// used for window selections
	bool inWindow (DRect window, bool entirely) const {return false;}

	/// returns a rectangle that fully includes the (finite) item
	/// used to calculate the drawing size
	DRect extents () const {return DRect();}

	/// checks, if a given point is element of the item
	/// used by the trimming function
	bool encloses (DPair p) const {return false;}

	/// returns the endpoints of a line or an arc
	DPair[] trimmingPoints() const {return [];}

	/// move item by distance vector d
	void move   (DPair d) {}

	/// mirror item at line a+n*d
	void mirror (DPair a, DPair d) {}

	/// rotate item at center c with angle a (degree)
	void rotate (DPair c, double a) {}

	/// scale item at centerpoint c with scale s
	void scale  (DPair c, double s) {}

	void move_grips   (DPair d) {}
	void mirror_grips (DPair a, DPair d) {}
	void rotate_grips (DPair c, double a) {}
	void scale_grips  (DPair c, double s) {}


	/// offset item with distance d. s is a point at the side where the item shall be offset to
	/// offsetting is not possible with all types of components: currently only lines and arcs
	bool offset  (double d, DPair s) {return false;}

	/// only for lines (and arcs?)
	bool fit (DPair from, DPair to) {return false;}

	/// check for circular references. Only used in WDReference
	bool checkForCircular (int ttl) {return false;}
}

/*******************************************************************************

helper functions for item lists

*******************************************************************************/

// find the Item with grip nearest to target that is at most <radius> away
WDTuple!(WDElement!WDItem, DPair, double) findNearestGrip (WDList!WDItem list, DPair target, double radius)
{
//	double itemdist = double.infinity;
	double dist = double.infinity;
	DPair pos;
	WDElement!WDItem item = new WDElement!WDItem (null);


	foreach (e; list[])
	{
		// first is position of grip
		// second is distance to grip
		WDTuple!(DPair, double) p = e.snapToGrip (target);

whereamif ("items %s", list);
whereamif ("sgrip %s", p);

		if (p.second <= radius && p.second < dist)
		{
			dist = p.second;
			pos = p.first;
			item = e;
		}
	}
	return wdTuple(item, pos, dist);
}

// find the Item nearest to target that is at most <radius> away and supports SnapMpde
WDTuple!(WDElement!WDItem, double) findNearestItem (WDList!WDItem list, DPair target, double radius, WDItemSnap mode)
{
	double dist = double.infinity;
	WDElement!WDItem item = new WDElement!WDItem (null);


	foreach (e; list[])
	{
		if (e.snapsTo (mode))
		{
			auto p = e.snapToItem (WDItemSnap.NEAREST, target);
			if (p.second <= radius && p.second < dist)
			{
				dist = p.second;
				item = e;
			}
		}
	}
	return wdTuple(item, dist);
}

// find all Items that are at most <radius> away and support SnapMpde
WDTuple!(WDElement!WDItem, DPair, double)[] findAllItems (WDList!WDItem list, DPair target, double radius, WDItemSnap mode)
{
	WDTuple!(WDElement!WDItem,DPair,double)[] items;

	foreach (e; list[])
	{
		if (e.snapsTo (mode))
		{
			WDTuple!(DPair, double) n = e.snapToItem (WDItemSnap.NEAREST, target);
			if (n.second <= radius)
			{
				items ~= wdTuple(e, n.first, n.second); // store distance to pointer
			}
		}
	}
	return items;
}

// find all Items that are at most <radius> away and support SnapMpde
WDTuple!(WDElement!WDItem, DPair, double)[] findAllGrips (WDList!WDItem list, DPair target, double radius)
{
	WDTuple!(WDElement!WDItem,DPair,double)[] items;

	foreach (e; list[])
	{
		WDTuple!(DPair, double) n = e.snapToItem (WDItemSnap.GRIP, target);
		if (n.second <= radius)
		{
			items ~= wdTuple(e, n.first, n.second); // store distance to pointer
		}
	}
	return items;
}

// return all items that are either entirely or partially inside window
WDElement!WDItem[] findItemsInWindow (WDList!WDItem list, DRect window, bool entirely)
{
	WDElement!WDItem[] rects;
	foreach (e; list[]) if (e.inWindow (window, entirely)) rects ~= e;
	return rects;
}
