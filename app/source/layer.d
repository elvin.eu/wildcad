import std.math;
import std.algorithm;
import std.container;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import line;
import painter;

/*******************************************************************************

Layer

*******************************************************************************/

class WDLayer : WDNamedObject
{
	mixin no;

private:
	const(char)[] _name;
	uint   _id;
	int    _prio;

	wcw.Color _color;
	WDLayerAttribute _attribute;

	WDList!WDItem _items;    // deselected items
	WDList!WDItem _selected; // selected items

public:
	this (const(char)[] nm=null, int prio=0)
	{
		no_init (nm, prio);

		_items = new WDList!WDItem;
		_selected = new WDList!WDItem;
	}

	override string toString () {return _name.idup;}

	WDList!WDItem items () {return _items;}
	WDList!WDItem selected () {return _selected;}

    bool empty () const {return _items.empty && _selected.empty;}

    cstring info () const
    {
		cstring text;
		if      (isHidden) text ~= "h ";
		else if (isFrozen) text ~= "f ";
		else if (isLocked) text ~= "l ";
		else               text ~= "  ";

		text ~= _color.toString ~ " " ~ _name;
		return text;
	}

	void update (const WDLayer from)
	{
		setAttribute (from.attribute);
		setColor (from.color);
	}

	void setColor (Color c) {_color = Color.rgb(c);}
    Color  color () const {return _color;}

	void setAttribute (WDLayerAttribute a) {_attribute = a & WDLayerAttribute.MASK;}	// mask out ACTIVE
	WDLayerAttribute attribute () const {return _attribute & WDLayerAttribute.MASK;}

	void hide ()   {setAttribute (WDLayerAttribute.HIDDEN);}
	void freeze () {setAttribute (WDLayerAttribute.FROZEN);}
	void lock ()   {setAttribute (WDLayerAttribute.LOCKED);}
	void enable () {setAttribute (WDLayerAttribute.NONE);}

    bool isHidden () const  {return (attribute == WDLayerAttribute.HIDDEN);}
	bool isFrozen () const  {return (attribute == WDLayerAttribute.FROZEN);}
    bool isLocked () const  {return (attribute == WDLayerAttribute.LOCKED);}
    bool isEnabled () const {return (attribute == WDLayerAttribute.NONE);}

	bool isSelectable () const {return isEnabled();}
	bool isSnapable () const {return isEnabled() || isLocked();}
	bool isVisible () const {return !isHidden();}

	bool addItem (WDItem item)
	{
		_items ~= item;
		return true;
	}

	bool clearItems ()
	{
		if (!isSelectable) return false;

		_items.clear;
		return true;
	}

	bool eraseSelected ()
	{
		if (!isSelectable) return false;

		_selected.clear;
		return true;
	}

//    bool clearAll() {clearItems(); eraseSelected(); return true;}

	bool drawItem (Painter painter, CairoMatrix matrix, WDItem o, double density=1.0)
	{
		// does o belong to this layer?
		if (find!("a.payload==b")(_items[], o).empty &&
			find!("a.payload==b")(_selected[], o).empty) return false;
		if (isHidden()) return true;

		painter.save ();
		painter.setColor (Settings.canvasColor.mix(_color, density));
		o.draw (painter, matrix, density);
		painter.restore();
		return true;
	}

	void drawAllItems (Painter painter, CairoMatrix matrix, double density=1.0)
	{
		if (isHidden()) return;

		painter.save ();
		painter.setColor (Settings.canvasColor.mix(_color, density));
		foreach (i; _items) i.draw (painter, matrix, density);
		painter.restore();
	}

	void drawAllSelected (Painter painter, CairoMatrix matrix, double density=1.0)
	{
		if (isHidden()) return;

		painter.save ();
		painter.setColor (Settings.canvasColor.mix(_color, density));
		foreach (i; _selected) i.draw (painter, matrix, density);
		painter.restore();
	}

	bool drawGrips (Painter p, CairoMatrix m, WDItem o, double den)
	{
		if (find!("a.payload==b")(_items[], o).empty &&
			find!("a.payload==b")(_selected[], o).empty) return false;
		if (isHidden()) return true;

		o.drawGrips (p, m, den);
		return true;
	}

	void drawAllGrips (Painter p, CairoMatrix m, double den)
	{
		if (isHidden()) return;
		foreach (e; _selected) e.drawGrips (p,m,den);
	}

	void selectItem (WDElement!WDItem e)
	{
		if (!isSelectable) return;

		e.remove;
		_selected ~= e;
	}

	void deselectItem (WDElement!WDItem e)
	{
		e.remove;
		e.clearGrips();
		_items ~= e;
	}

    bool deselectAllItems ()
	{
        if (_selected.empty) return false;

		foreach (i; _selected[]) i.clearGrips();
		_items.splice(_selected);
		return true;
	}

    bool selectAllItems ()
	{
        if (_items.empty || !isSelectable) return false;

		_selected.splice(_items);
		return true;
	}

//    // these functions originally belong to WDItems and are not needed
//    // by WDLayers. But as WDBlocks are Components (pointed to by WDReference)
//    // and WDBlocks are WDLayerTables, we have to implement these functions
//    // here (Question: what about moving them to WDBlock?)

	WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		WDItem item = null;

		foreach (i; _items[])
		{
			if (i.snapsTo (snapmode))
			{
				auto s = i.snapToItem (WDItemSnap.NEAREST, pointer, from);
				if (s.second < dist)
				{
					dist = s.second;
					item = i;
				}
			}
		}
		foreach (i; _selected[])
		{
			if (i.snapsTo (snapmode))
			{
				auto s = i.snapToItem (WDItemSnap.NEAREST, pointer, from);
				if (s.second < dist)
				{
					dist = s.second;
					item = i;
				}
			}
		}

		if (item) return item.snapToItem (snapmode, pointer, from);
		else      return wdTuple(snap, dist);
	}

	bool snapsTo (WDItemSnap s) const
	{
		foreach (i; _items)    if (i.snapsTo (s)) return true;
		foreach (i; _selected) if (i.snapsTo (s)) return true;
		return false;
	}

	DRect extents (bool selected) const
	{
		DRect rect;
		if (isHidden()) return rect;

		foreach (i; _selected) rect = rect.united (i.extents());
		if (selected) return rect;

		foreach (i; _items) rect = rect.united (i.extents());
		return rect;
	}

	bool checkForCircular (int ttl)
	{
		bool rc = false;

		foreach (item; _items)
		{
			if (item.checkForCircular(ttl)) rc = true;
		}
		foreach (item; _selected)
		{
			if (item.checkForCircular(ttl)) rc = true;
		}

		return rc;

	}


}
