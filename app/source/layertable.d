import std.math;

import wcw;
import cairo;

import layer;
import auxiliary;
import enums;
import namedobject;
import item;
//import settings;
//import line;
import painter;
import intersection;

/*******************************************************************************

LayerTable, also Block

*******************************************************************************/

alias WDBlock = WDLayerTable;

class WDLayerTable : WDNamedObject, WDNamedList!WDLayer
{
	mixin no;
	mixin nt!WDLayer;

private:
	// for WDNamedObject
	string _name;
	uint   _id;
	int    _prio;

	// for WDNamedList
	WDLayer[] _list;
	cstring _defaultname;

	// for WDLayerTable
	WDLayer _activelayer = null;
	bool _pageshidden = false;
	bool _circular = false;      // block only
	DPair _base = DPair(0.0, 0.0);

public:
	this (const(char)[] n, int prio=0)
	{
		no_init (n, prio);
		nt_init ("default");
	}

	override string toString ()
	{
		return ("list " ~ nt_info).dup;
	}

	void setBase (DPair b) {_base = b;}
	DPair base () const {return _base;}

	WDItem[] selectedItems ()
	{
		WDItem[] items;

		foreach (layer; _list)
		{
			foreach (item; layer.selected)
			{
				items ~= item;
			}
		}
		return items;
	}


//	void updateLayer(const WDLayer &l);
//	WDLayer &pageLayer ();
	bool drawItem (Painter p, CairoMatrix m, WDItem o, double d)
	{
		foreach_reverse (l; _list) if (l.drawItem (p, m, o, d)) return true;
		return false;
	}

	void drawAllItems (Painter painter, CairoMatrix matrix, double density)
	{
		// draw last layer first, first layer last
		// so, the first layer will be on top of all the others
		foreach_reverse (i; _list) i.drawAllItems (painter, matrix, density);
	}

	void drawAllSelected (Painter painter, CairoMatrix matrix, double density)
	{
		// draw last layer first, first layer last
		// so, the first layer will be on top of all the others
		foreach_reverse (i; _list) i.drawAllSelected (painter, matrix, density);
	}

	bool drawGrips (Painter p, CairoMatrix m, WDItem o, double d)
	{
		foreach (layer; _list)	if (layer.drawGrips (p, m, o, d)) return true;
		return false;
	}

	void drawAllGrips (Painter p, CairoMatrix m, double d)
	{
		foreach_reverse (layer; _list) layer.drawAllGrips (p, m, d);
	}

    // add item to layer; returns true when the item was added
    // a nonexisting layer is silently created
    WDLayer addItemToLayer (WDItem item, cstring layer)
    {
		auto l = find (layer);
		if (l is null) l = create (layer);
		if (l is null) return null;

		l.addItem (item);
		return l;
	}

    // add item to layer; returns true when the item was added
    // a nonexisting layer is silently created
    WDLayer addItemToLayer (WDItem item, WDLayer layer)
    {
		auto l = find (layer);
		if (l is null) return null;

		l.addItem (item);
		return l;
	}

	// return currently active layer
	WDLayer activeLayer ()
	{
		// either return activelayer (if existing)
		if (find(_activelayer)) return _activelayer;

		// or the first layer in the table
		if (_list.length > 0) _activelayer = _list[0];
		else                  _activelayer = create ();

		return _activelayer;
	}

//    // set parameters of a layer

	WDLayer updateLayer (cstring name, const WDLayer layer)
	{
		return updateLayer (name, layer.attribute, layer.color);
	}

	WDLayer updateLayer (cstring name, WDLayerAttribute attr, Color color)
	{
		WDLayer layer;
		if ((layer = find (name)) is null) return null;

		layer.setAttribute (attr);
		layer.setColor (color);
		return layer;
	}

	// make a layer active
	WDLayer activateLayer (cstring name)
	{
		WDLayer layer;
		if ((layer = find(name)) is null) return null;

		_activelayer = layer;
		return layer;
	}


	WDLayer setLayerAttribute (cstring name, WDLayerAttribute a)
	{
		WDLayer layer;
		if ((layer = find(name)) is null) return null;

		layer.setAttribute (a);
		return layer;
	}

	WDLayer hideLayer (cstring name)
	{
		WDLayer layer;
		if ((layer = find (name)) is null) return null;

		layer.hide ();
		return layer;
	}

	WDLayer freezeLayer (cstring name)
	{
		WDLayer layer;
		if ((layer = find (name)) is null) return null;

		layer.freeze ();
		return layer;
	}

	WDLayer lockLayer (cstring name)
	{
		WDLayer layer;
		if ((layer = find (name)) is null) return null;

		layer.lock ();
		return layer;
	}

//	WDLayer enableLayer (cstring name)
//	{
//		WDLayer layer;
//		if ((layer = find (name)) is null) return null;

//		layer.enable ();
//		return layer;
//	}

	WDLayer setLayerColor (cstring name, Color color)
	{
		WDLayer layer;
		if ((layer = find (name)) is null) return null;

		layer.setColor (color);
		return layer;
	}

	WDLayer setLayerName (cstring name, cstring newname)
	{
		return rename (name, newname);
	}

	WDLayer moveLayer (cstring name, WDDirection d)
	{
		return move (name, d);
	}

	DPair itemSnap (WDItemSnap snap, double radius, DPair target, DPair from)
	{
		WDTuple!(WDElement!WDItem, DPair, double)[] items;
		WDTuple!(WDElement!WDItem, DPair, double)[] grips;

		WDTuple!(DPair, double)[] snaps;

		foreach (layer; _list)
		{
			if (!layer.isSnapable()) continue;

			items ~= layer.items.findAllItems (target, radius, snap);
			items ~= layer.selected.findAllItems (target, radius, snap);
		}

		if (items.length == 1)
		{

			auto s = items[0].first.snapToItem (snap&~(WDItemSnap.INTERSECTION|WDItemSnap.GRIP), target, from);
			snaps ~= wdTuple (s.first, s.second);
		}
		else if (items.length > 1)
		{
			// find the two nearest items
			WDItem i0, i1;
			double d0=double.infinity, d1;

			foreach (i; items)
			{
				if (i.third < d0)
				{
					d1 = d0;
					i1 = i0;
					d0 = i.third;
					i0 = i.first;
				}
				else if (i.third < d1)
				{
					d1 = i.third;
					i1 = i.first;
				}
			}

			// the nearest item is now in i0, the second nearest item in i1
			auto s = i0.snapToItem (snap&~(WDItemSnap.INTERSECTION|WDItemSnap.GRIP), target, from);
			snaps ~= wdTuple (s.first, s.second);

			if (snap & WDItemSnap.INTERSECTION)
			{
				snaps ~= intersection_nearest(i0, i1, target);
			}
		}

		// now add nearest grip to the list

		if (snap & WDItemSnap.GRIP)
		{
			foreach (layer; _list)
			{
				if (layer.isHidden || layer.isLocked) continue;

				grips ~= layer.selected.findAllGrips (target, radius);
			}

			DPair gn;
			double dn=double.infinity;

			foreach (g; grips)
			{
				if (g.third < dn)
				{
					gn = g.second;
					dn = g.third;
				}
			}
			snaps ~= wdTuple (gn, dn);
		}

		DPair sp;
		double sd = double.infinity;

		foreach (s; snaps)
		{
			if (s.second < sd)
			{
				sp = s.first;
				sd = s.second;
			}
		}

		return sp;
	}

	// select nearest item to <c> that is not farer away than <apr>
	WDItem selectItem (const DPair target, const double apr, const bool grips)
	{
		WDTuple!(WDElement!WDItem, double) found, nearest_item;
		WDLayer nearest_layer;

		nearest_item = wdTuple (new WDElement!WDItem(null), double.infinity);

		// check all layers for the nearest item
		foreach  (layer; _list)
		{
	        if (!layer.isSelectable) continue;

			found =  layer.items.findNearestItem (target, apr, WDItemSnap.ALL);
			if (found.second < nearest_item.second)
			{
				nearest_item = found;
				nearest_layer = layer;
			}
		}

		if (nearest_item.second <= apr)
		{
			nearest_layer.selectItem (nearest_item.first);
			return nearest_item.first;
		}

		return null;
	}

	WDItem toggleGrip (const DPair target, const double apr)
	{
		WDTuple!(WDElement!WDItem, DPair, double) found, nearest_item;
		WDLayer nearest_layer;

		nearest_item = wdTuple (new WDElement!WDItem(null), DPair(), double.infinity);

		// check all layers for the nearest item
		foreach  (layer; _list)
		{
	        if (layer.isSelectable) continue;

			found =  layer.selected.findNearestGrip (target, apr);
			if (found.third < nearest_item.third)
			{
				nearest_item = found;
				nearest_layer = layer;
			}
		}

		if (nearest_item.third <= apr)
		{
			nearest_item.first.toggleGrip (nearest_item.second);
			return nearest_item.first;
		}

		return null;
	}

	WDItem[] toggleGrips (const DRect window)
	{
		WDItem[] items;

		// toggle all grips in window
		foreach  (layer; _list)
		{
	        if (layer.isFrozen || layer.isHidden) continue;

			foreach (item; layer.selected)
			{
				if (item.toggleGrips (window)) items ~= item;
			}
		}

		return items;
	}

	// deselect nearest item to <c> that is not farer away than <apr>
	WDItem deselectItem (DPair target, double apr)
	{
		WDTuple!(WDElement!WDItem, double) n0, nearest;
		nearest = WDTuple!(WDElement!WDItem, double)(new WDElement!WDItem(null), double.infinity);
		WDLayer nearest_layer;

		// check all layers for the nearest item
		foreach  (layer; _list)
		{
			n0 =  layer.selected.findNearestItem (target, apr, WDItemSnap.ALL);
			if (n0.second < nearest.second)
			{
				nearest = n0;
				nearest_layer = layer;
			}
		}

		if (nearest.second <= apr)
		{
			nearest_layer.deselectItem (nearest.first);
			return nearest.first;
		}
		return null;
	}

	WDItem[] selectItems (DRect window, bool entirely, bool grips)
	{
		WDItem [] array;

		// check all layers for items
		foreach (layer; _list)
		{
			if (!layer.isSelectable) continue;

			auto items = findItemsInWindow (layer.items, window, entirely);
			foreach (item; items)
			{
				layer.selectItem (item);
				array ~= (item);
			}
		}

		return array;
	}

	WDItem[] deselectItems (DRect window, bool entirely)
	{
		WDItem [] array;

		// check all layers for items
		foreach (layer; _list)
		{
			if (!layer.isSelectable) continue;

			auto items = findItemsInWindow (layer.selected, window, entirely);
			foreach (item; items)
			{
				layer.deselectItem (item);
				array ~= (item);
			}
		}

		return array;
	}

//    // returns a vector of pointers to all selected items
//    vec_pc_type   selectedItems ();

//    // select all --- to be replaced by std::vector<WDItem*> selectItems ();
    bool selectAllItems ()
    {
		bool u = false;
		// check all layers for selected items
		foreach (layer; _list)
		{
			if (!layer.isSelectable) continue;
			u |= layer.selectAllItems();
		}
		return u;
	}

//    deselect all --- to be replaced by WDItem[] deselectAllItems?
	bool deselectAllItems (cstring name = "")
	{
		bool u = false;
		// check all layers for selected items
		foreach (layer; _list)
		{
			if (name == "" || name == layer.name)
			{
				layer.deselectAllItems();
			}
		}
		return u;
	}

	cstring info () const
	{
		cstring list;
		const WDLayer a = _activelayer;

		foreach (i; _list)
		{
			if (a==i) list ~= "*";
			else      list ~= " ";
			list ~= i.info;
			list ~= "\n";
		}
		return list;
	}


	bool eraseSelected ()
	{
		foreach (layer; _list)
		{
			if (!layer.isSelectable()) continue;
			layer.eraseSelected();
		}
		return true;
	}


	WDTuple!(DPair, double) snapToItem (DPair pointer, WDItemSnap snapmode) const
	{
		WDTuple!(DPair, double) s;

		DPair snap;
		double dist = double.infinity;

		foreach (layer; _list)
		{
			s = layer.snapToItem (snapmode, pointer, DPair());	// xxx
			if (s.second < dist) {snap = s.first; dist = s.second;}
		}

		return wdTuple(snap, dist);
	}

	bool snapsTo (WDItemSnap s) const
	{
		foreach (layer; _list)
		{
			if (layer.snapsTo (s)) return true;
		}
		return false;
	}

	DRect extents (bool selected = false) const
	{
		DRect rect;


		foreach (l; _list) rect = rect.united (l.extents(selected));

		if (notReal(rect.x0) || notReal(rect.y0))
		{
			rect = DRect(DPair(0.0, 0.0), 100.0, 100.0);
		}
		else
		{
			if (rect.width  <= 0.0)
			{
				rect.x0   -= 50.0;
				rect.width = 100.0;
			}

			if (rect.height <= 0.0)
			{
				rect.y0    -= 50.0;
				rect.height = 100.0;
			}
		}

		return rect;
	}

	//    // Block functions:
	bool checkForCircular (int ttl)
	{
		bool rc = false;

		foreach (layer; _list) if (layer.checkForCircular(ttl)) rc = true;
		return rc;
	}

//    bool removeEmptyLayers ();

}
