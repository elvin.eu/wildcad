import std.conv;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDLine : WDItem
{
private:
	DPair _start, _mid, _end;
	bool  _grips, _gripe;

public:
	this (DPair s, DPair e)
	{
		init (s, e);
	}

	this (const WDLine l)
	{
		init (l.start, l.end);

		_grips = l._grips;
		_gripe = l._gripe;
	}

	void init (DPair s, DPair e)	// called by dimension
	{
		if (s.isReal()) _start = s; else _start = DPair(0,0);
		if (e.isReal()) _end = e; else _end = DPair(0,0);
		_mid = (_start+_end)/2;
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDLine (this);
	}

	DPair start () const {return _start;}
	DPair end ()  const {return _end;}

	override string toString () const {return "line @c1 " ~ pretty(_start) ~ " @c2 " ~ pretty (_end);}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		double ax, ay, bx, by;
		matrix.transform_point (_start.x, _start.y, ax, ay);
		matrix.transform_point (_end.x,   _end.y,   bx, by);
		p.drawLine (ax, ay, bx, by);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		with (WDItemSnap)
		{
		  return ((s & (NEAREST | END | MIDDLE | INTERSECTION | PERPENDICULAR)) != 0);
		}
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		DPair snap;
		double dist = double.infinity;

		if (snapmode & (WDItemSnap.NEAREST | WDItemSnap.INTERSECTION))
		{
			snap = lnear (_start, _end, pointer);
			dist = pdist (snap, pointer);
		}
		else
		{
			if (snapmode & WDItemSnap.MIDDLE)
			{
				snap = _mid;
				dist = pdist (_mid, pointer);
			}
			if (snapmode & (WDItemSnap.END | WDItemSnap.GRIP))
			{
				double pd;
				if ((pd=pdist(_start, pointer)) < dist)
				{
					snap = _start;
					dist = pd;
				}
				if ((pd=pdist(_end,   pointer)) < dist)
				{
					snap = _end;
					dist = pd;
				}
			}
			if (snapmode & WDItemSnap.PERPENDICULAR)
			{
				auto p = lsquare(_start, _end, from);
				double pd;
				if ((pd=pdist(p, pointer)) < dist)
				{
					snap = p;
					dist = pd;
				}
			}
		}
		return wdTuple (snap, dist);
	}

	override void drawGrips (Painter p, CairoMatrix m, double den)
	{
		drawGrip (p, m, _start, _grips, den);
		drawGrip (p, m, _end,   _gripe, den);
	}
/*
	override bool pickGrip  (DRect window)
	{
		clearGrips();

		auto s = window.contains (_start);
		auto e = window.contains (_end);

		_gripe = s && !e;
		_grips = e && !s;

		return _grips || _gripe;
	}
*/


	override void clearGrips ()
	{
		_grips = _gripe = false;
	}

	override WDTuple!(DPair, double) snapToGrip (DPair target) const
	{
		double ds = pdist (_start, target);
		double de = pdist (_end, target);

		if (ds < de) return wdTuple (cast(DPair) _start, ds);
		else         return wdTuple (cast(DPair) _end, de);
	}

	override bool toggleGrip  (DPair target)
	{
		bool t;
		if      (target == _start) {_grips = !_grips; t = true;}
		else if (target == _end)   {_gripe = !_gripe; t = true;}
		return t;
	}

	override bool toggleGrips (DRect window)
	{
		bool t;
		if (window.contains (_start)) {_grips = !_grips; t = true;}
		if (window.contains (_end))   {_gripe = !_gripe; t = true;}
		return t;
	}

	override bool inWindow (DRect r, bool entirely) const
	{
		if (r.contains (_start) && r.contains (_end))
		{
			return true;
		}

		if (entirely == false)
		{
			auto p1 = DPair (r.x0,r.y0);
			auto p2 = DPair (r.x1,r.y0);
			auto p3 = DPair (r.x1,r.y1);
			auto p4 = DPair (r.x0,r.y1);

			if (intersect_ll(_start, _end, p1, p2).isReal) return true;
			if (intersect_ll(_start, _end, p2, p3).isReal) return true;
			if (intersect_ll(_start, _end, p3, p4).isReal) return true;
			if (intersect_ll(_start, _end, p4, p1).isReal) return true;
		}
		return false;
	}

   	override DRect extents () const
	{
		return DRect (_start, _end);
	}

	override bool encloses (DPair p) const
	{
		if (!p.isReal()) return false;

		if (circa (lnear (_start, _end, p), p)) return true;
		else                                    return false;
	}

	override DPair[] trimmingPoints() const
	{
		return [_start, _end];
	}

	override void move (DPair d)
	{
		if (!d.isReal()) return;

		_start += d;
		_mid   += d;
		_end   += d;
	}

	override void move_grips (DPair d)
	{
		if (!d.isReal()) return;

		if (_grips) _start += d;
		if (_gripe) _end += d;
		_mid = (_start+_end)/2;
	}

	override void mirror (DPair a, DPair d)
	{
		if (!a.isReal()) return;
		if (!d.isReal()) return;

		auto start = 2*xnear (a, d, _start) - _start;
		auto end   = 2*xnear (a, d, _end)   - _end;

		if (!start.isReal() || !end.isReal()) return;

		_start = start;
		_end = end;
		_mid = (_start+_end)/2;
	}

	override void rotate (DPair c, double a)
	{
		if (!c.isReal()) return;
		if (!a.isReal()) return;

		_start = c + auxiliary.rotate(vector (c, _start), rad(a));
		_end   = c + auxiliary.rotate(vector (c, _end)  , rad(a));
		_mid = (_start+_end)/2;
	}


	override void rotate_grips (DPair c, double a)
	{
		if (!c.isReal()) return;
		if (!a.isReal()) return;

		if (_grips) _start = c + auxiliary.rotate(vector (c, _start), rad(a));
		if (_gripe) _end   = c + auxiliary.rotate(vector (c, _end)  , rad(a));
		_mid = (_start+_end)/2;
	}

	override void scale  (DPair c, double s)
	{
		if (!c.isReal()) return;
		if (!s.isReal()) return;

		_start = (_start-c)*s + c;
		_end   = (_end-c)*s + c;
		_mid = (_start+_end)/2;
	}

//	override void scale_grips  (DPair c, double s)
//	{
//		if (_grips) _start = (_start-c)*s + c;
//		if (_gripe) _end   = (_end-c)*s + c;
//		_mid = (_start+_end)/2;
//	}

	override bool offset  (double d, DPair s)
	{
		if (!d.isReal()) return false;
		if (!s.isReal()) return false;

		DPair p = xnear  (_start, _end-_start, s);
		DPair v = vector(p, s).unit * d;
		if (!v.isReal) return false;	// if s is on line, then v is undefined

		move (v);
		return true;
	}

	override bool fit (DPair from, DPair to)
	{
		if      (from == _start) _start = to;
		else if (from == _end)   _end   = to;
		else    return false;

		_mid = (_start+_end)/2;
		return true;
	}

	override bool checkForCircular (int ttl) {return false;}

}
