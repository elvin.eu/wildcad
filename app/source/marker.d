import std.conv;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDMarker : WDItem
{
private:
	DPair _base;
	int _size;
	int _type;

public:

	this (DPair base)
	{
		_base = base;
	}

	this (const WDMarker m)
	{
		_base = m._base;
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDMarker (this);
	}

	override string toString () const
	{
		return "marker @c1 " ~ pretty (_base);
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		// if _size = 0 use global marker size
		double r = (_size?_size:Settings().markerSize)/2;

		if (r == 0) return; // markers visible?

		double px, py;
		matrix.transform_point (_base.x, _base.y, px, py);

		// wtf - different marker types?
		if (_type == 1)
		{
			p.drawCircle (px, py, r);
		}
		else if (_type == 2)
		{
			whereami ("ahaaa! ein type 2 Marker");
			p.drawCircle (px, py, r);
		}
		else
		{
			p.drawCircle (px, py, r);
			p.drawLine (px, py-r, px, py+r);
			p.drawLine (px-r, py, px+r, py);
		}
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST|WDItemSnap.CENTER)) != 0);
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapsTo (snapmode))
		{
			snap = _base;
			dist = pdist (snap, pointer);
		}

		return wdTuple (snap, dist);
	}

	override bool inWindow (DRect r, bool entirely) const
	{
		return r.contains (_base);
	}

   	override DRect extents () const
	{
		return DRect (_base, 0, 0);
	}

//	override bool encloses (DPair p) const
//	{
//	}

//	override DPair[] intersect (WDItem c) const
//	{
//	}

	override DPair[] trimmingPoints() const
	{
		return [_base,];
	}

	override void move (DPair d)
	{
		_base += d;
	}

//	override void drag (DPair dist)
//	{
//	}

	override void mirror (DPair a, DPair d)
	{
		_base = 2*xnear (a, d, _base) - _base;
	}

	override void rotate (DPair c, double a)
	{
		_base = c + auxiliary.rotate(vector (c, _base), rad(a));
	}

	override void scale  (DPair c, double s)
	{
		_base = (_base-c)*s + c;
	}

//	override bool offset  (double d, DPair s)
//	{
//	}

//	override bool fit (DPair from, DPair to)
//	{
//	}

//	override bool checkForCircular (int ttl) {return false;}

}