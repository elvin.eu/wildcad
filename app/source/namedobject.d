import std.algorithm.searching;
import std.string;

import auxiliary;
import enums;

interface  WDNamedObject
{
//	string _name;
//	uint   _id;
//	int    _prio;

	void no_init (const(char)[] n, int prio);

	cstring name () const;
	int prio ();
	void setName (string n);
}

mixin template no()
{
	import std.string;
	import auxiliary;

public:
	void no_init (cstring n=null, int prio=0)
	{
		_prio = prio;
		setName (n);
	}

	cstring name () const {return _name;}
	int prio () const {return _prio;}

	void setName (cstring n)
	{
		cstring s = n.strip;

		if (s is null || s == "") _name = "unnamed";
		else                      _name = s.dup;
	}
}


interface WDNamedList(T)
{
//	T[] _list;
//	cstring _defaultname;		// default name for new NamedObjects

	void nt_init (string defaultname);
	cstring nt_info ();

	inout (T[]) list () inout;
	void erase ();
	T create (string name="", int prio=0);
	inout (T) find (cstring name) inout;
	inout (T) find (inout T t) inout;
	T rename (cstring name, cstring newname);
	T remove (cstring name);
	T move (cstring name, WDDirection d);
}

mixin template nt(T)
{
	import std.algorithm.searching;

	void nt_init (cstring defaultname)
	{
		if (defaultname == "") _defaultname = "unnamed";
		else                   _defaultname = defaultname;
	}

	cstring nt_info ()
	{
		cstring s;
		foreach (t; _list) s ~= t.toString ~ "\n";
		return s;
	}

	inout (T[]) list () inout {return _list;}

	void erase ()
	{
		_list.length = 0;
	}

	T create (cstring name="", int prio=0)
	{
		if (name == "") name = "unnamed";

		// create a unique name - if name exists already, prepend underscore
		while (canFind!("a.name==b")(_list, name)) name = "_" ~ name;
		auto t = new T (name);
		_list ~= t;
		return t;
	}

	inout (T) find (cstring name) inout
	{
		auto result = std.algorithm.find!("a.name==b")(_list, name);

		if (result.length == 0) return null;
		else                    return result[0];
	}

	inout (T) find (inout T t) inout
	{
		auto result = std.algorithm.find(_list, t);

		if (result.length == 0) return null;
		else                    return result[0];
	}

	T rename (cstring oldname, cstring newname)
	{
		auto o = find (oldname);
		auto n = find (newname);
		if (o is null || n !is null) return null;

		o.setName (newname);
		return o;
	}

	T remove (cstring name)
	{
		uint i;
		T removed;

		foreach (t; _list) if (t.name == name) break; else i++;

		if (i >= _list.length) return null;

		removed = _list[i];
		_list = _list [0..i] ~ _list [i+1..$];
		return removed;
	}

	T move (cstring name, WDDirection d)
	{
		size_t i;
		T moved;

		foreach (t; _list) if (t.name == name) break; else i++;

		if (i >= _list.length) return null;

		moved = _list[i];
		final switch (d)
		{
		case WDDirection.UP:
			if (i == 0) return null;

			_list[i] = _list[i-1];
			_list[i-1] = moved;
			return moved;

		case WDDirection.DOWN:
			if (i >= _list.length-1) return null;

			_list[i] = _list[i+1];
			_list[i+1] = moved;
			return moved;

		case WDDirection.FRONT:
			while (move (name, WDDirection.UP) !is null) {}
			return moved;

		case WDDirection.BACK:
			while (move (name, WDDirection.DOWN) !is null) {}
			return moved;
		}
	}
}
