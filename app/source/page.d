import auxiliary;
import painter;
import enums;
import item;

import cairo;

/////////////////////////////////////////////////////////////////////
// WDPage
/////////////////////////////////////////////////////////////////////

class WDPage : WDItem
{
private:
	DPair  _papersize;	// size in mm
	DPair  _origin;		// position of page in drawing
	double _scale;	// scale of drawing on paper

public:
	this (DPair size, double scale, DPair origin)
	{
		_papersize = size;
		_origin = origin;
		_scale = scale;
	}

	this (const WDPage p)
	{
		_papersize = p.papersize;
		_origin = p.origin;
		_scale = p.scale;
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDPage (this);
	}

	DPair papersize() const  {return _papersize;}
	DPair origin() const     {return _origin;}
	double scale() const     {return _scale;}

	override string toString () const
	{
		return "page";
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		double ax, ay, bx, by;
		matrix.transform_point (_origin.x, _origin.y, ax, ay);
		matrix.transform_point (_origin.x+_papersize.x/_scale, _origin.y+_papersize.y/_scale, bx, by);
		p.drawLine (ax, ay, bx, ay);
		p.drawLine (bx, ay, bx, by);
		p.drawLine (bx, by, ax, by);
		p.drawLine (ax, by, ax, ay);
	};
}