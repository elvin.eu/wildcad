// painter

import std.conv;
import std.exception;
import std.math;
import std.utf;

static import X11;
import cairo;
import wcw;

import auxiliary;
import enums;
import font;

struct Storables
{
	bool xor;
	Color color;
}

enum PainterMode
{
	CAIRO, XLIB
}

// currently, Painter uses 2 different contexts: one for Cairo and one for X11 (since Cairo

class Painter
{
private:
	X11.Display* _dsp;
	X11.Drawable _drw;
	X11.GC _gc;
	DRect _rect;

	Storables _store;
	Storables[] _stack;

	CairoContext _context;
	Widget _widget;

	PainterMode _mode;

public:
	this (CairoSurface s)
	{
		_mode = PainterMode.CAIRO;

		_context = new CairoContext (s);
		_context.set_line_width (1);
		_context.set_source_rgb (1.0, 1.0, 1.0);
	}

	this (Widget widget)
	{
		_mode =  PainterMode.XLIB;
		_widget = widget;

		// create X11 context
		auto xs = cast(CairoXlibSurface)(widget.window.surface);
		assert (xs);
		_dsp  = xs.get_display;
		assert (_dsp);
		_drw = xs.get_drawable;
		assert (_drw);
		_gc = X11.XCreateGC(_dsp, _drw, 0, null);
		assert (_gc);

		// the X11 drawable is the whole X11 window; therefore for all X11
		// drawing operations, coordinates must be transformed by r.x and r.y
		// and clipped to r's size

		IRect r = widget.mapToWindow (widget.rectangle);

		_rect.x0     = cast(double) r.x0;
		_rect.y0     = cast(double) r.y0;
		_rect.width  = cast(double) r.width;
		_rect.height = cast(double) r.height;

		X11.XRectangle[] clip = [X11.XRectangle(_rect.x0.to!short, _rect.y0.to!short, _rect.width.to!short, _rect.height.to!short)];
		X11.XSetClipRectangles (_dsp, _gc, 0, 0, clip.ptr, 1, 0);
	}

	~this ()
	{
		final switch (_mode)
		{
			case PainterMode.XLIB:
				X11.XFree (_gc);
			break;
			case PainterMode.CAIRO:
				destroy (_context);
			break;
		}
	}

	X11.Display* x11Display () {assert (_mode == PainterMode.XLIB); return _dsp;}
	X11.Drawable x11Drawing () {assert (_mode == PainterMode.XLIB); return _drw;}
	X11.GC x11Context () {assert (_mode == PainterMode.XLIB); return _gc;}

	Widget widget () {assert (_mode == PainterMode.XLIB); return _widget;}

	CairoContext cairoContext () {assert (_mode == PainterMode.CAIRO); return _context;}

	void save()
	{
		_stack ~= _store;
	}

	void restore ()
	{
		enforce (_stack.length);

		auto c = _stack[$-1].color;
		auto x = _stack[$-1].xor;

		if (c != _store.color) setColor (c);
		if (x != _store.xor)   setOperatorXOR (x);

		_store = _stack[$-1];
		_stack.length--;
	}

	void setOperatorXOR (bool set=true)
	{
		if (set == _store.xor) return;

		final switch (_mode)
		{
			case PainterMode.XLIB:
				if (set)
				{
					X11.XGCValues v = {X11.GXxor};
					X11.XChangeGC (_dsp, _gc, X11.GCFunction, &v);
					X11.XSetForeground (_dsp, _gc, _store.color ^ 0xffffff);
				}
				else
				{
					X11.XGCValues v = {X11.GXcopy};
					X11.XChangeGC (_dsp, _gc, X11.GCFunction, &v);
					X11.XSetForeground (_dsp, _gc, _store.color);
				}
			break;

			case PainterMode.CAIRO:
			break;
		}

		_store.xor = set;
	}

	bool operatorXOR ()
	{
		return _store.xor;
	}

	void setColor (Color c)
	{
		if (_store.color == c) return;
		_store.color = c;

		final switch (_mode)
		{
		case PainterMode.XLIB:
			if (_store.xor) X11.XSetForeground (_dsp, _gc, c.inverted);
			else            X11.XSetForeground (_dsp, _gc, c);
		break;

		case PainterMode.CAIRO:
			_context.set_source_rgb (c.red, c.green, c.blue);
		break;
		}
	}

	long color ()
	{
		return _store.color;
	}

//	void stroke ()
//	{
//		_context.stroke;
//	}

	void drawLine (DPair a, DPair b, CairoMatrix m)
	{
		double ax, ay, bx, by;
		m.transform_point (a.x, a.y, ax, ay);
		m.transform_point (b.x, b.y, bx, by);
		drawLine (ax, ay, bx, by);
	}

	void drawLine (double x0, double y0, double x1, double y1)
	{
		final switch (_mode)
		{
			case PainterMode.XLIB:
				x0 += _rect.x0;
				x1 += _rect.x0;
				y0 += _rect.y0;
				y1 += _rect.y0;

				if (clip_line (x0, y0, x1, y1, _rect.x0, _rect.y0, _rect.x1, _rect.y1) == false) return;
				X11.XDrawLine (_dsp, _drw, _gc, x0.int_round, y0.int_round, x1.int_round, y1.int_round);
			break;

			case PainterMode.CAIRO:
				_context.move_to (x0, y0);
				_context.line_to (x1, y1);
			break;
		}
	}

	void drawPoint (DPair a, CairoMatrix m)
	{
		double ax, ay;
		m.transform_point (a.x, a.y, ax, ay);
		drawPoint (ax, ay);
	}

	void drawPoint (double x, double y)
	{
		if (x < 0) return;
		if (y < 0) return;
		if (x > _rect.width)  return;
		if (y > _rect.height) return;

		X11.XDrawPoint (_dsp, _drw, _gc, (x+_rect.x0).int_round, (y+_rect.y0).int_round);
	}

	void drawCircle (DPair a, double r, CairoMatrix m)
	{
		double x, y, rx, ry;
		m.transform_point (a.x, a.y, x, y);
		m.transform_distance (r, r, rx, ry);
		drawCircle (x, y, rx);
	}

	void drawCircle (double x, double y, double radius)
	{
		if (radius < 0) return;

		double xmin = round(x-radius);
		double ymin = round(y-radius);


		double xr = xmin+_rect.x0;
		double yr = ymin+_rect.y0;

		if (xr > short.min && xr < short.max  &&
		    yr > short.min && yr < short.max &&
		    radius < ushort.max/2)
		{
			uint ud = uint_round(radius*2);
			int ix = int_round(xr);
			int iy = int_round(yr);

			X11.XDrawArc (_dsp, _drw, _gc, ix, iy, ud, ud, 0, 23040);
		}
		else
		{
			double xmax = round(x+radius);
			double ymax = round(y+radius);

			x = (xmin+xmax)/2;
			y = (ymin+ymax)/2;

			double rx = (xmax-xmin)/2;
			double ry = (xmax-xmin)/2;

			double x0 = x + rx;		// cos(0) = 1
			double y0 = y;			// sin(0) = 0

			double da = acos((radius-1)/radius);

			double x1, y1;

			for (double a=da; a<PI*2; a+=da)
			{
				x1 = x + rx*cos(a);
				y1 = y + ry*sin(a);

				drawLine (x0, y0, x1,  y1);

				x0 = x1;
				y0 = y1;
			}

			x1 = x+rx;
			y1 = y;
			drawLine (x0, y0, x1,  y1);
		}
	}

	void drawArc (DPair a, double r, double a0, double a1, CairoMatrix m)
	{
		double x, y, rx, ry;
		m.transform_point (a.x, a.y, x, y);
		m.transform_distance (r, r, rx, ry);
		drawArc (x, y, rx, a0, a1);
	}

	void drawArc (double x, double y, double r, double a0, double a1)
	{
		double xmin = round(x-r);
		double xmax = round(x+r);
		double ymin = round(y-r);
		double ymax = round(y+r);

		x = (xmin+xmax)/2;
		y = (ymin+ymax)/2;

		double rx = (xmax-xmin)/2;
		double ry = (xmax-xmin)/2;

		double x0 = x + rx*cos(a0);
		double y0 = y + ry*-sin(a0);

		double da = acos((r-1)/r);

		double x1, y1;

		if (a1 < a0) a1 += PI*2;

		for (double a=a0+da; a<a1; a+=da)
		{
			x1 = x + rx*cos(a);
			y1 = y + ry*-sin(a);

			drawLine (x0, y0, x1,  y1);

			x0 = x1;
			y0 = y1;
		}

		x1 = x + rx*cos(a1);
		y1 = y + ry*-sin(a1);

		drawLine (x0, y0, x1,  y1);
	}

	void fillRectangle (double x, double y, double w, double h)
	{
		X11.XFillRectangle (_dsp, _drw, _gc, (x+_rect.x0).int_round, (y+_rect.y0).int_round,w.int_round, h.int_round);
	}

	void drawRectangle (double x, double y, double w, double h)
	{
		X11.XDrawRectangle (_dsp, _drw, _gc, (x+_rect.x0).int_round, (y+_rect.y0).int_round, w.int_round, h.int_round);
	}

	// angle in RAD
	void drawText (WDFont font, cstring txt, double x, double y, double size, double angle)
	{
		size_t count = txt.toUCSindex (txt.length);
		if (count == 0) return;

		foreach (dchar chr; txt)
		{
			drawLetter (font, chr, x, y, size, angle);
		}
	}

	// angle in RAD
	void drawLetter (WDFont font, dchar chr, ref double x, ref double y, double size, double angle)
	{
		if (size < 0) size = -size;

		// scale factors for x and y:
		double sx = size/font.height;
		double sy = -size/font.height;

		assert (sx > 0);
		assert (sy < 0);

		CairoMatrix m;
		m.translate (x, y);
		m.rotate (-angle);
		m.scale (sx, sy);

		foreach (line; font.letter(chr).lines)
		{
			if (line.p.length < 4) continue;

			double xm, ym;
			m.transform_point (line.p[0], line.p[1], xm, ym);

			for (size_t i=2; i<line.p.length; i+=2)
			{
				double xn, yn;
				m.transform_point (line.p[i], line.p[i+1], xn, yn);
				drawLine (xm, ym, xn, yn);
				xm = xn;
				ym = yn;
			}
		}

		foreach (arc; font.letter(chr).arcs)
		{
			double ax, ay;
			m.transform_point (arc.x, arc.y, ax, ay);

			drawArc (ax, ay, arc.d*sx/2, arc.a0/180.0*PI+angle, arc.a1/180.0*PI+angle);
		}

		foreach (c; font.letter(chr).chars)
		{
			foreach (l; c.l)
			{
				double cx = x;
				double cy = y;
				drawLetter (font, l, cx, cy, size, angle);
			}
		}

		m.transform_point (font.width, 0, x, y);
	}
}
