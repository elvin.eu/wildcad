import std.math;
import std.conv;

import wcw;

import canvas;
import item;
import enums;
import auxiliary;
import line;
import interpreter;
import layer;
import reference;

class Pending
{
protected:
	cstring _message;

public:
	abstract bool execute ();
	bool exetemp() {return true;}
	bool success (cstring m = "OK") {_message = m; return true;}
	bool failure (cstring m = "ERROR") {_message = m; return false;}
	cstring message () {return _message;}
};

class Pending_Block : Pending
{
private:
	WDCanvas  _canvas;
	cstring _name;
	DPair _base;
	Pending_Action _action;

public:
	this (Pending_Action a, WDCanvas c, cstring n, DPair b=DPair(0.0,0.0))
	{
		_action = a;
		_canvas = c;
		_name = n;
		_base = b;
	}

	override bool execute()
	{
		if (_action == Pending_Action.CREATE)
		{
			if (_canvas.createBlock (_name, _base, false))
				return success ("block \"" ~ _name ~ "\" created");
			else
				return failure (_message = "Cannot create block \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.REMOVE)
		{
			if (_canvas.removeBlock (_name))
				return success ("block \"" ~ _name ~ "\" removed");
			else
				return failure ("cannot remove block \"" ~ _name ~ "\"");
		}

		if (_action == Pending_Action.SWITCH)
		{
			if (_canvas.activateBlock (_name))
				return success ("block \"" ~ _name ~ "\" is now active");
			else
				return failure (_message = "Cannot switch to block \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.RENAME)
		{
			if (_canvas.activeBlock is _canvas.activeLayer) failure ("no active block");

			if (_canvas.renameBlock (_canvas.activeBlock.name, _name))
				return success ("block renamed to \"" ~ _name ~ "\"");
			else
				return failure ("cannot rename block to \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.INSERT)
		{
			auto t = _canvas.blockTable();
			auto b = t.find(_name);

			if (b)
			{
				_canvas.addItemToActive (new WDReference (b, _base));
				return success();
			}
			else
			{
				return failure ("cannot find block \"" ~ _name ~ "\"");
			}
		}

		return failure ("unknown action " ~ _action.to!string);
	}

	override bool exetemp()
	{
		if (_action == Pending_Action.INSERT)
		{
			auto t = _canvas.blockTable();
			auto b = t.find(_name);

			if (b)
			{
				_canvas.addItemToTemporary (new WDReference (b, _base));
				return true;
			}
		}
		return true;
	}

}

class Pending_Canvas : Pending
{
private:
	WDCanvas _canvas;

public:
	this (WDCanvas c)
	{
		_canvas = c;
	}

	override bool execute ()
	{
		_canvas.activateCanvas();
		return success ("canvas is now active");
	}
}


class Pending_Close : Pending
{
private:
	WDCanvas _canvas;

public:
	this (WDCanvas c)
	{
		_canvas = c;
	}

	override bool execute ()
	{
		if (_canvas.closeDrawing ()) return success (_message = "drawing closed");
		else                         return failure (_message = "cannot close; drawing is modified");
	}
}

class Pending_Copy : Pending
{
private:
	WDCanvas _canvas;
	DPair _anchor;
	bool _cut;

public:
	this (WDCanvas c, DPair a, bool cut=false)
	{
		_canvas = c;
		_anchor = a;
		_cut    = cut;
	}

	override bool execute ()
	{
		if (_canvas.copyToClipboard (_anchor, _cut))
			return success();
		else
			return failure();
	}
}

class Pending_CopyUndo : Pending
{
private:
	WDCanvas _canvas;

public:
	this (WDCanvas c)
	{
		_canvas = c;
	}

	override bool execute ()
	{
		if (_canvas.undoToClipboard ())
			return success();
		else
			return failure();
	}
}

class Pending_Erase : Pending
{
private:
	WDCanvas  _canvas;

public:
	this (WDCanvas c)
	{
		_canvas = c;
	}

	override bool execute ()
	{
		if (_canvas.eraseSelected()) return success();
		else                         return failure();
	}
}

class Pending_Fit : Pending
{
private:
	WDCanvas _canvas;
	bool _extended;
	bool _trim;

public:
	this (WDCanvas canvas, bool trim, bool extended)
	{
		_canvas = canvas;
		_trim = trim;
		_extended = extended;
	}

	override bool execute ()
	{
		_canvas.fitItemToSelected (_trim, _extended);
		return success();
	}

	override bool exetemp ()
	{
		return execute ();
	}
}

class Pending_Grid : Pending
{
private:
	WDCanvas  _canvas;
	DPair _grid;

public:
	this (WDCanvas c, DPair g)
	{
		_canvas = c;
		_grid = g;
	}

	override bool execute ()
	{
		_canvas.setGrid (_grid);
		return success();
	}
}

class Pending_Item : Pending
{
private:
	WDCanvas _canvas;
	WDItem   _item;

public:
	this (WDCanvas c, WDItem i)
	{
		_canvas = c;
		_item = i;
	}

	override bool execute ()
	{
		_canvas.addItemToActive (_item);
		return success();
	}

	override bool exetemp ()
	{
//		_canvas.deselectAllItems();
		_canvas.addItemToTemporary (_item);
		return true;
	}
}

class Pending_Layer : Pending
{
private:
	Pending_Action _action;
	WDCanvas  _canvas;
	cstring _name;
	cstring _rename;
	Color  _color;
	WDDirection _direction;
	int _count;
	WDLayerAttribute _attribute;

public:
	this (Pending_Action a, WDCanvas c, const cstring  n, cstring r="")
	{
		import std.exception: enforce;
		enforce ((a == Pending_Action.CREATE) ||
				 (a == Pending_Action.REMOVE) ||
				 (a == Pending_Action.SWITCH) ||
				 (a == Pending_Action.RENAME));

		_action = a;
		_canvas = c;
		_name = n;
		_rename = r;
	}

	this (WDCanvas c, Color color)
	{
		_canvas = c;
		_action = Pending_Action.COLOR;
		_color = color;
	}

	this (WDCanvas c, WDDirection dir, int count)
	{
		_canvas = c;
		_action = Pending_Action.ARRANGE;
		_direction = dir;
		_count = count;
	}

	this (WDCanvas c, WDLayerAttribute attr)
	{
		_canvas = c;
		_action = Pending_Action.ATTRIBUTE;
		_attribute = attr;
	}

	override bool execute()
	{
		// does the layer exist?
		WDLayer cl;
		if (_name == "")
		{
			cl = _canvas.activeLayer();
			_name = cl.name;
		}
		else
		{
			cl = _canvas.activeBlock.find(_name);
		}

		if (_action == Pending_Action.CREATE)
		{
			if (_canvas.createLayer (_name))
				return success("layer \"" ~ _name ~ "\" is now active");
			else
				return failure("Cannot create layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.REMOVE)
		{
			if (_canvas.removeLayer (_name))
				return success("layer \"" ~ _name ~ "\" removed");
			else
				return failure("cannot remove layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.SWITCH)
		{
			if (_canvas.activateLayer (_name))
				return success ("layer \"" ~ _name ~ "\" is now active");
			else
				return failure ("Cannot activate layer \"" ~ _name ~ "\"");
		}
		if (_action == Pending_Action.CREATE)
		{
			if (_canvas.createLayer (_name))
				return success("layer \"" ~ _name ~ "\" is now active");
			else
				return failure("Cannot create layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.RENAME)
		{
			if (_canvas.renameLayer (_canvas.activeLayer.name, _rename))
				return success("layer \"" ~ _name ~ "\" renamed to \"" ~ _rename ~ "\"");
			else
				return failure("cannot rename layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.COLOR)
		{
			if (_canvas.setLayerColor (_name, _color))
				return success("color of layer \"" ~ _name ~ "\" changed");
			else
				return failure("cannot change color of layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.ARRANGE)
		{
			if (_canvas.moveLayer (_name, _direction, _count))
				return success ("layer \"" ~ _name ~ "\" moved");
			else
				return failure("cannot move layer \"" ~ _name ~ "\"");
		}
		else if (_action == Pending_Action.ATTRIBUTE)
		{
			if (_canvas.setLayerAttribute (_name, _attribute))
				return success("modified layer \"" ~ _name ~ "\" successfully");
			else
				return failure("could't modify layer \"" ~ _name ~ "\"");
		}
		return failure ("unknown layer command");
	}
}

class Pending_List : Pending
{
private:
	WDInterpreter  _inpr;
	WDInfo _info;

public:
	this (WDInterpreter ip, WDInfo i)
	{
		_inpr = ip;
		_info = i;
	}

	override bool execute()
	{
		_inpr.list (_info);
		return success();
	}
}

class Pending_Measure : Pending
{
private:
	WDCanvas _canvas;
	WDInterpreter _inpr;
	DPair[] _points;

public:
	this (WDCanvas ca, WDInterpreter ip, DPair[] pts)
	{
		_canvas = ca;
		_inpr = ip;
		_points = pts;
	}

	override bool execute ()
	{
		_inpr.measure (_points);
		return success();
	}

	override bool exetemp ()
	{
		if (_points.length < 2) return true;

		DPair p = _points[0];
		foreach (n; _points[1..$])
		{
			_canvas.addItemToTemporary (new WDLine (p, n));
			p = n;
		}
		return true;
	}

}

class Pending_Mirror  : Pending
{
private:
	WDCanvas _canvas;
	DPair _anchor;
	DPair _direction;

public:
	this (WDCanvas ca, DPair a, DPair d)
	{
		_canvas = ca;
		_anchor = a;
		_direction = d;
	}

	override bool execute ()
	{
		if (_canvas.mirrorSelected (_anchor, _direction))
			return success();
		else
			return failure();
	}

	override bool exetemp ()
	{
		return (_canvas.mirrorSelected (_anchor, _direction, true));
	}
}

class Pending_Move : Pending
{
private:
	WDCanvas _canvas;
	DPair _distance;
	int _count;

public:
	this (WDCanvas c, DPair d, int n)
	{
		_canvas = c;
		_distance = d;
		_count = n;
	}

	override bool execute ()
	{
		if (_canvas.moveSelected (_distance, _count))
			return success();
		else
			return failure();
	}

	override bool exetemp ()
	{
		return _canvas.moveSelected (_distance, _count, true);
	}
}

class Pending_Offset : Pending
{
private:
	WDCanvas _canvas;
	double _dist;
	DPair _side;
	int _count;

public:
	this (WDCanvas canvas, double dist, const DPair side, int count)
	{
		_canvas = canvas;
		_dist = dist;
		_side = side;
		_count = count;
	}

	override bool execute ()
	{
		if (_canvas.offsetSelected (_dist, _side, _count))
			return success();
		else
			return failure("could not offset all items");
	}

	override bool exetemp ()
	{
		return _canvas.offsetSelected (_dist, _side, _count, true);
	}
}

class Pending_Open : Pending
{
private:
	WDCanvas  _canvas;
	cstring _path;

public:
	this (WDCanvas c, cstring p="")
	{
		_canvas = c;
		_path = p;
	}

	override bool execute ()
	{

		if (_canvas.openFile (_path) == true)
			return success ("file '" ~ _path ~ "' successfully opened");
		else
			return failure ("error opening file '" ~ _path ~ "'");
	}
}

class Pending_Origin : Pending
{
private:
	WDCanvas  _canvas;
	DPair _origin;

public:
	this (WDCanvas c, DPair o)
	{
		_canvas = c;
		_origin = o;
	}

	override bool execute ()
	{
		_canvas.setOrigin (_origin);
		return success();
	}
};

class Pending_Ortho : Pending
{
private:
	WDCanvas  _canvas;
	bool _ortho;

public:
	this (WDCanvas c, bool o)
	{
		_canvas = c;
		_ortho = o;
	}

	override bool execute ()
	{
		_canvas.setOrthogonal(_ortho);
		return success();
	}
}

class Pending_Pan : Pending
{
private:
	WDCanvas _canvas;
	DPair _start, _end;

public:
	this (WDCanvas c, DPair p)
	{
		_canvas = c;
		_start = p;
		_end = p;
	}

	this (WDCanvas c, DPair s, DPair e)
	{
		_canvas = c;
		_start = s;
		_end = e;
	}

	override bool execute()
	{
		if (_start == _end) _canvas.screenPan (_start);
		else                _canvas.screenPan (_start, _end);
		return success();
	}

	override bool exetemp()
	{
		if (_start != _end)
		{
			_canvas.addItemToTemporary (new WDLine (_start, _end));
		}
		return true;
	}
};

class Pending_Paste : Pending
{
private:
	WDCanvas _canvas;
	DPair _anchor;

public:
	this (WDCanvas c, DPair a)
	{
		_canvas = c;
		_anchor = a;
	}

	override bool execute ()
	{
		if (_canvas.pasteFromClipboard (_anchor))
			return success();
		else
			return failure();
	}
}



class Pending_Print : Pending
{
private:
	WDCanvas _canvas;
	DPair _from, _to;
	cstring _device;

public:
	this (WDCanvas c, DPair f, DPair t, cstring d)
	{
		_canvas = c;
		_device = d;

		if (f.isReal && t.isReal)
		{
			_from = f;
			_to = t;
		}
	}

	override bool execute ()
	{
		_canvas.print (_from, _to, _device);
		return success();
	}

	override bool exetemp()
	{
		if (_from.isReal && _to.isReal)
		{
			auto fxty = DPair(_from.x,_to.y);
			auto txfy = DPair(_to.x,_from.y);

			_canvas.addItemToTemporary (new WDLine (_from, fxty));
			_canvas.addItemToTemporary (new WDLine (fxty, _to));
			_canvas.addItemToTemporary (new WDLine (_to, txfy));
			_canvas.addItemToTemporary (new WDLine (txfy, _from));
		}
		return true;
	}
}

class Pending_Quit : Pending
{
private:
	WDCanvas _canvas;

public:

	this (WDCanvas c)
	{
		_canvas = c;
	}

	override bool execute ()
	{
		_canvas.quit();
		return success();
	}
};

class Pending_Rotate : Pending
{
private:
	WDCanvas _canvas;
	DPair _center;
	double _angle;
	int _count;

public:
	this (WDCanvas cv, const DPair c, double a, int ct)
	{
		_canvas = cv;
		_center = c;
		_angle = a;
		_count = ct;
	}

	override bool execute ()
	{
		if (_canvas.rotateSelected (_center, _angle, _count))
			return success();
		else
			return failure();
	}

	override bool exetemp ()
	{
		return _canvas.rotateSelected (_center, _angle, _count, true);
	}
}
class Pending_Save : Pending
{
    private:
        WDCanvas _canvas;
        string _path;
        bool _as;

    public:
        this (WDCanvas c, cstring path, bool as)
        {
			_canvas = c;
			_path = path.idup;
			_as = as;
        }

        override bool execute ()
        {
			bool rc;

			if (_as && _path == "") rc = _canvas.saveFileDialog ();
			else                    rc = _canvas.saveFile (_path);

			if (rc) return success();
			else    return failure();
		}

};

class Pending_Scale : Pending
{
private:
	WDCanvas _canvas;
	DPair _base;
	double _factor;
	int _count;

public:
	this (WDCanvas canvas, DPair base, double factor, int count)
	{
		_canvas = canvas;
		_base = base;
		_factor = factor;
		_count = count;
	}

	override bool execute ()
	{
		if (_canvas.scaleSelected (_base, _factor, _count))
			return success();
		else
			return failure();
	}

	override bool exetemp ()
	{
		return _canvas.scaleSelected (_base, _factor, _count, true);
	}
}

class Pending_Snap : Pending
{
private:
	WDCanvas  _canvas;
	WDItemSnap _snap;

public:
	this (WDCanvas c, WDItemSnap s)
	{
		_canvas = c;
		_snap = s;
	}

	override bool execute ()
	{
		_canvas.setSnapMode (_snap);
		return success();
	}
}

//class Pending_View : Pending
//{
//private:
//	WDCanvas  _canvas;
//	cstring _name;
//	cstring _rename;
//	Pending_Action  _action;

//public:
//	this (Pending_Action a, WDCanvas c, cstring n, cstring r="")
//	{
//		_canvas = c;
//		_action = a;
//		_name = n;
//		_rename = r;
//	}

//	override bool execute ()
//	{
//		if  (_action == Pending_Action.SWITCH)
//		{
//			if (_canvas.activateView (_name))
//				return success ("changed to view \"" ~ _name ~ "\"");
//			else
//				return failure ("cannot find view \"" ~ _name ~ "\"");
//		}
//		else if (_action == Pending_Action.CREATE)
//		{
//			if (_canvas.createView (_name))
//				return success ("new view \"" ~ _name ~ "\" created");
//			else
//				return failure ("cannot create new view named \"" ~ _name ~ "\"");
//		}
//		else if (_action == Pending_Action.REMOVE)
//		{
//			if (_canvas.removeView (_name))
//				return success ("view \"" ~ _name ~ "\" removed");
//			else
//				return failure ("cannot find view  \"" ~ _name ~ "\"");
//		}
//		else if (_action == Pending_Action.UPDATE)
//		{
//			if (_canvas.updateView (_name))
//				return success ("view \"" ~ _name ~ "\" updated");
//			else
//				return failure ("cannot find view \"" ~ _name ~ "\"");
//		}
//		else if (_action == Pending_Action.RENAME)
//		{
//			if (_canvas.renameView (_name, _rename))
//				return success ("view \"" ~ _name ~ "\" renamed to \"" ~ _rename ~ "\"");
//			else
//				return failure ("cannot rename view \"" ~ _name ~ "\" to \"" ~ _rename ~ "\"");
//		}
//		return failure ("unknown action " ~ _action.to!string);
//	}
//}

class Pending_Zoom : Pending
{
private:
	WDCanvas _canvas;
	double _factor;
	DRect  _window;

public:
	this (WDCanvas c)
	{
		_canvas = c;
	}

	this (WDCanvas c, double f)
	{
		_canvas = c;
		_factor = f;
	}

	this (WDCanvas c, DRect w)
	{
		_canvas = c;
		_window = w;
	}

	override bool execute ()
	{
		if      (_factor.notReal == false) _canvas.zoomBy (_factor);
		else if (_window.notReal == false) _canvas.zoomRect (_window);
		else                             _canvas.zoomAll ();

		return success();
	}

	override bool exetemp()
	{
		if (!_window.notReal())
		{
			with (_window)
			{
				auto corner1 = DPair(x0, y0);
				auto corner2 = DPair(x1, y0);
				auto corner3 = DPair(x1, y1);
				auto corner4 = DPair(x0, y1);
				auto l1 = new WDLine (corner1, corner2);
				auto l2 = new WDLine (corner2, corner3);
				auto l3 = new WDLine (corner3, corner4);
				auto l4 = new WDLine (corner4, corner1);
				_canvas.addItemToTemporary (l1);
				_canvas.addItemToTemporary (l2);
				_canvas.addItemToTemporary (l3);
				_canvas.addItemToTemporary (l4);
			}
		}
		return true;
	}
}

/+

Pending_Page::Pending_Page (WDCanvas &c, const DPair &o, const DPair &s, double sc)
: _canvas (c),
  _origin (o),
  _size  (s),
  _scale  (sc)
{}

bool Pending_Page::execute()
{
	if (_canvas.addPage (_origin, _size, _scale))
		return success();
	else
		return failure();
}

bool Pending_Page::exetemp()
{
	DPair corner1 (_origin.x(),                   _origin.y());
	DPair corner2 (_origin.x()+_size.x()*_scale, _origin.y());
	DPair corner3 (_origin.x()+_size.x()*_scale, _origin.y()+_size.y()*_scale);
	DPair corner4 (_origin.x(),                   _origin.y()+_size.y()*_scale);

	AutoPtr<WDItem> aptr1 (new WDLine (corner1, corner2));
	AutoPtr<WDItem> aptr2 (new WDLine (corner2, corner3));
	AutoPtr<WDItem> aptr3 (new WDLine (corner3, corner4));
	AutoPtr<WDItem> aptr4 (new WDLine (corner4, corner1));

	_canvas.addToTemporary (aptr1);
	_canvas.addToTemporary (aptr2);
	_canvas.addToTemporary (aptr3);
	_canvas.addToTemporary (aptr4);
	return true;
}

Pending_Pick::Pending_Pick (WDCanvas &c, bool a)
: _canvas (c), _all (a)
{
}

bool Pending_Pick::execute()
{
	if (_all)
	{
		_canvas.selectAllItems ();
	}
	else
	{
		_canvas.deselectAllItems ();
	}
	return success();
}


bool Pending_Print::execute ()
{
	/*
	if (_canvas.print ()) return success();
	else                  return failure("You have to define a page first!");
*/
	return failure("printing is not implemented!");
}


bool Pending_Redraw::execute ()
{
	_canvas.draw(true);
	return success();
}


+/
