import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;


class WDPolyline : WDItem
{
private:
	WDPolylineType _type;
	WDTuple!(DPair, double)[] _points;

public:
	this (WDTuple!(DPair, double)[] p, WDPolylineType  t)
	{
		_points = p;
		_type = t;
	}

	this (const WDPolyline l)
	{
		_type = l._type;
		_points = l._points.dup;
	}

	override WDItem clone ()
	{
		return cast (WDItem) new WDPolyline (this);
	}

	override string toString () const
	{
		string s = "polyline @et " ~ pretty (_type);
		foreach (p; _points)
		{
			s ~= " @cn " ~ pretty (p.first);
			if (p.second!=0.0 && p.second<=1.0 && p.second>=-1.0) s ~= " @fb " ~ pretty (p.second);
		}
		return s;
	}

	// draws the item onto the screen
	// only WDReference currently needs the density parameter <d_>
	override void draw (Painter p, CairoMatrix matrix, double density=1.0)
	{
		void cra1a2 (DPair S, DPair E, double b, out DPair C, out double r, out double a1, out double a2)
		{
			if (b == 0.0) return;

			auto M = S + (E-S)/2;
			auto B = M + perpendicular (M, E)*(-b);
			auto T = S + (B-S)/2;
			auto P = T + perpendicular (S, B);

			C = intersect_xx (T, P, M, B);
			r = norm (C-B);

			if (b > 0) {a1 = arg (S-C); a2 = arg (E-C);}
			else       {a1 = arg (E-C); a2 = arg (S-C);}
		}

		if (_points.length < 2) return;

		DPair from = _points[0].first;
		double b = _points[0].second;
		DPair to;

		foreach (point; _points[1..$])
		{
			to = point.first;

			if (b == 0.0)
			{
				p.drawLine (from, to, matrix);
			}
			else
			{
				DPair C;
				double r, a1, a2;
				cra1a2 (from, to, b, C, r, a1, a2);
				p.drawArc (C, r, a1, a2, matrix);
			}
			from = point.first;
			b = point.second;
		}

		if (_type == WDPolylineType.CLOSED)
		{
			to = _points[0].first;

			if (b == 0.0)
			{
				p.drawLine (from, to, matrix);
			}
			else
			{
				DPair C;
				double r, a1, a2;
				cra1a2 (from, to, b, C, r, a1, a2);
				p.drawArc (C, r, a1, a2, matrix);
			}
		}
	}
}
