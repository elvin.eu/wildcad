import std.conv;
import std.math: PI;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;

class WDRay : WDItem
{
private:
	DPair _start;
	DPair _direction = DPair (1.0, 0.0);

public:
	this (DPair a, DPair d)
	{
		if (a.isReal()) _start = a; else _start = DPair (0.0, 0.0);
		if (d.isReal && (d != DPair(0.0, 0.0))) _direction = d; else _direction = DPair(1.0, 0.0);
	}

	this (DPair a, double d)
	{
		if (a.isReal()) _start = a; else _start = DPair (0.0, 0.0);
		if (d.isReal()) _direction = vector (rad(d)); else _direction = DPair(1.0, 0.0);

	}

	this (const WDRay r)
	{
	  _start     = r._start;
	  _direction = r._direction;
	}

	override WDItem clone () {return new WDRay (this);}

	DPair start () const {return _start;}
	DPair direction () const {return _direction;}

	override string toString () const
	{
		return "ray @c1 "~ pretty(_start) ~
			" @c2 " ~ pretty (_direction.unit);
	}


	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		CairoMatrix inverted = matrix.inverted;
		// get viewport size in world coordinates

		double l, t, r, b;

		inverted.transform_point (0.0, 0.0, l, t);
		inverted.transform_point (p.widget.size.x, p.widget.size.y, r, b);

		double x1, y1, x2, y2;
		if (_direction.y == 0) // ray horizontal?
		{
			x2 = _direction.x<0 ? l : r;
			y2 = _start.y;
		}
		else if (_direction.x == 0) // ray vertical?
		{
			x2 = _start.x;
			y2 = _direction.y<0 ? b : t;
		}
		else // neither nor
		{
			double xa, yb;

			if (_direction.y > 0) xa = _start.x+(_direction.x/_direction.y)*(t-_start.y);
			else                  xa = _start.x+(_direction.x/_direction.y)*(b-_start.y);

			if (_direction.x > 0) yb = _start.y+(_direction.y/_direction.x)*(r-_start.x);
			else                  yb = _start.y+(_direction.y/_direction.x)*(l-_start.x);

			if (l<=xa && xa<=r)
			{
				x2 = xa;
				y2 = _direction.y<0 ? b : t;
			}
			else
			{
				x2 = _direction.x<0? l : r;
				y2 = yb;
			}
		}

		matrix.transform_point (_start.x, _start.y, x1, y1);
		matrix.transform_point (x2, y2, x2, y2);
		p.drawLine (x1, y1, x2, y2);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST|WDItemSnap.END|WDItemSnap.INTERSECTION|WDItemSnap.PERPENDICULAR)) != 0);
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapmode & (WDItemSnap.NEAREST | WDItemSnap.INTERSECTION))
		{
			snap = rnear (_start, _direction, pointer);
			dist = pdist (snap, pointer);
		}
		else
		{
			if (snapmode & WDItemSnap.END)
			{
				snap = _start;
				dist = pdist (_start, pointer);
			}
			if (snapmode & WDItemSnap.PERPENDICULAR)
			{
				auto p = rsquare(_start, _direction, from);
				double pd;
				if ((pd=pdist(p, pointer)) < dist) {dist = pd; snap = p;}

			}
		}
		return wdTuple (snap, dist);
	}

	override bool inWindow (DRect r, bool entirely) const
	{
		if (entirely == false)
		{
			DPair p1 = rnear (_start, _direction, DPair (r.x0, r.y0));
			DPair p2 = rnear (_start, _direction, DPair (r.x0, r.y1));
			DPair p3 = rnear (_start, _direction, DPair (r.x1, r.y1));
			DPair p4 = rnear (_start, _direction, DPair (r.x1, r.y0));

			if (r.contains (p1) || r.contains (p2) || r.contains (p3) || r.contains (p4))
			{
				return true;
			}
		}

		return false;
	}

   	override DRect extents () const
	{
		    return DRect (_start, 0, 0);
	}

	override bool encloses (DPair p) const
	{
		if (circa (rnear (_start, _direction, p), p)) return true;
		else                                          return false;
	}

	override DPair[] trimmingPoints() const
	{
		return [_start];
	}

	override void move (DPair d)
	{
		_start += d;
	}

	override void mirror (DPair a, DPair d)
	{
		_start = 2*xnear (a, d, _start) - _start;
		_direction = vector(normalize!PI(2*arg(d)-arg(_direction)));
	}

	override void rotate (DPair c, double a)
	{
		_start = c + auxiliary.rotate(vector (c, _start), rad(a));
		_direction = vector(normalize!PI(rad(a)+arg(_direction)));
	}

	override void scale  (DPair c, double s)
	{
		_start  = (_start-c)*s + c;
	}

	override bool offset  (double d, DPair s)
	{
		if (!d.isReal()) return false;
		if (!s.isReal()) return false;

		DPair p = xnear  (_start, _direction, s);
		DPair v = vector (p, s).unit * d;

		if (!v.isReal()) return false;
		move (v);
		return true;
	}

	override bool fit (DPair from, DPair to)
	{
		if (from != _start) return false;

		_start = to;
		return true;
	}
}
