import std.conv;
import std.algorithm;
import std.math: PI;

import cairo;
import wcw;

import auxiliary;
import settings;
import painter;
import enums;
import item;
import layertable;

class WDReference : WDItem
{
private:
	WDBlock _block;
	DPair _base;
	DPair _scale;
	double  _angle = 0.0;

public:
	this (WDBlock block, DPair base, DPair scale=DPair(1.0,1.0), double angle=0.0)
	{
		_block = block;
		_base = base;

		_scale = scale;
		_angle = angle;
	}

	this (WDReference r)
	{
		_block = r._block;
		_base  = r._base;
		_scale = r._scale;
		_angle = r._angle;
	}

	inout (WDBlock) block () inout {return _block;}

	override WDItem clone()
	{
		return new WDReference (this);
	}

	override string toString () const
	{
		string s = "reference @sn " ~ pretty (_block.name) ~ " @c1 " ~ pretty (_base);
		if (_scale != DPair(1.0,1.0)) s ~= " @ns " ~ pretty (_scale);
		if (_angle != 0.0) s ~= " @a1 " ~ pretty (_angle);
		return s;
	}

	override void draw (Painter painter, CairoMatrix matrix, double density=1.0)
	{
		matrix.translate (_base.x, _base.y);
		matrix.scale (_scale.x, _scale.y);
		matrix.rotate (_angle);

		_block.drawAllItems   (painter, matrix, density);
		_block.drawAllSelected(painter, matrix, density);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		if (_block.snapsTo (s)) return true;
		return false;
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		DPair snap;
		double dist = double.infinity;

		// this matrix transforms pointer block to world
		CairoMatrix toworld;
		toworld.translate (_base.x, _base.y);
		toworld.scale (_scale.x, _scale.y);
		toworld.rotate (-_angle);

		// this matrix transforms pointer world to block
		CairoMatrix toblock = toworld.inverted;

		// transform <c> into the block's coordinate system
		double dx, dy;
		toblock.transform_point (pointer.x, pointer.y, dx, dy);
		// do the operation in the block's coordinate system
		auto bsnap = _block.snapToItem (DPair (dx, dy), snapmode);
		// return value is a point in the block's coordinate system:
		// transform it back to the world coordinate sysetm
		double wx, wy;
		toworld.transform_point (bsnap.first.x, bsnap.first.y, wx, wy);
		snap.x = wx;
		snap.y = wy;

		dist = pdist (pointer, snap);
		return wdTuple(snap, dist);
	}

//	bool inWindow (DRect window, bool entirely) const {return false;}

	override DRect extents () const
	{
		// get the block extents
		DRect e = _block.extents ();
		// and convert into world coordinates

		// this matrix transforms from block to world
		CairoMatrix toworld;
		toworld.translate (_base.x, _base.y);
		toworld.scale (_scale.x, _scale.y);
		toworld.rotate (-_angle);

		double x, y, w, h;
		toworld.transform_point (e.x0,  e.y0, x, y);
		toworld.transform_distance (e.width, e.height, w, h);
		DRect r =  DRect (DPair(x, y), w, h);

		return r;
	}

//	// checks, if a given point is element of the item
//	// used by the trimming function
//	bool encloses (DPair p) const {return false;}

//	// return a vector of all intersection points between <this> and <c>
//	DPair[] intersect (WDItem c) const {return [];}

//	// returns the endpoints of a line or an arc
//	DPair[] trimmingPoints() const {return [];}


	override void move (DPair d)
	{
		_base += d;
	}

//	void drag   (DPair d) {}

	override void mirror (DPair a, DPair d)
	{
		_base = 2*xnear (a, d, _base) - _base;
		_scale.y = -_scale.y;
		_angle = deg(normalize!PI(2*arg(d)-rad(_angle)));
	}

	override void rotate (DPair c, double a)
	{
		_base = c + .rotate (vector (c, _base), rad(a));
	}

	override void scale (DPair c, double s)
	{
		_base  = (_base-c)*s + c;
		_scale = _scale * s;
	}

//	// offset is not possible with all types of components: only lines and arcs
//	bool offset  ( double d, DPair s) {return false;}
//	// only for lines (and arcs?)
//	bool fit (DPair from, DPair to) {return false;}

	override bool checkForCircular (int ttl)
	{
		ttl--;

		if (ttl <= 0) return true;

		return _block.checkForCircular(ttl);
	}
}