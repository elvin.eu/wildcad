// configuration settings

import wcw;

import auxiliary;


class WDSettings : Config
{
	// global application settings

	Color freeSnapColor;		// default aperture color
	Color itemSnapColor;		//         aperture color when snapping to end, mid etc
	Color gridSnapColor;		//         aperture color when snapping to grid
	Color gridColor;
	Color canvasColor;
	Color originColor;			// color of the (0.0) marker
	Color ribbonColor;			// ribbons are temporary objects
	Color leftPickColor;		// color for "partially in window" selection
	Color rightPickColor;		// color for "entire in window" selection
	Color gripColor;			// color of passive grip for dragging operations
	Color dragColor;			// color of active grip for dragging operations

	int apertureSize;
	int originSize;
	int markerSize;
	int gripSize;
	int precision;
	double selectDensity;
	double ribbonDensity;

	// default settings that are overwritten by drawing specific settings

	double textSize;
	double dimTextSize;
	int dimStyle;
	int dimLengthPrecision;
	int dimAnglePrecision;

//	double printDrawingScale;
//	double printPageWidth;
//	double printPageHeight;
//	double printPageScale;
//	int printPageUnit;

	this()
	{
		super ("wildcad");

		canvasColor = value!Color("canvasColor", "ffffff");

		freeSnapColor = value!Color("Global.FreeSnapColor",  "00ff00");
		itemSnapColor = value!Color("Global.ItemSnapColor",  "ff0000");
		gridSnapColor = value!Color("Global.GridSnapColor",  "0000ff");
		gridColor     = value!Color("Global.GridColor",      "888888");
		canvasColor   = value!Color("Global.CanvasColor",    "ffffff");
		originColor   = value!Color("Global.OriginColor",    "8080ff");
		ribbonColor   = value!Color("Global.RibbonColor",    "ff8800");
		leftPickColor = value!Color("Global.leftPickColor",  "ff0088");
		rightPickColor= value!Color("Global.rightPickColor", "8800ff");
		gripColor     = value!Color("Global.GripColor",      "ff4444");
		dragColor     = value!Color("Global.DragColor",      "4444ff");

		apertureSize  = value!int   ("Global.ApertureSize",   15);
		originSize    = value!int   ("Global.OriginSize",     15);
		markerSize    = value!int   ("Global.MarkerSize",     15);
		gripSize      = value!int   ("Global.GripSize",        8);
		precision     = value!int   ("Global.Precision",       6);
		selectDensity = value!double("Global.SelectDensity", 0.3);
		textSize      = value!double("Global.TextSize",      5.0);

		dimTextSize        = value!double("Dimension.TextSize",      5.0);
		dimLengthPrecision = value!int   ("Dimension.LengthPrecision", 1);
		dimAnglePrecision  = value!int   ("Dimension.AnglePrecision",  1);

//		printDrawingScale = value!double("Printing.DrawingScale", 1.0);
//		printPageWidth    = value!double("Printing.PageWidth",  297.0);
//		printPageHeight   = value!double("Printing.PageHeight", 210.0);
//		printPageScale    = value!double("Printing.PageScale",    1.0);
//		printPageUnit     = value!int   ("Printing.PageUnit",       0);

	}

	override void save ()
	{
		// need to copy all variables into the database
		store ("Global.Color.FreeSnap",  freeSnapColor);
		store ("Global.Color.ItemSnap",  itemSnapColor);
		store ("Global.Color.GridSnap",  gridSnapColor);
		store ("Global.Color.Grid",      gridColor);
		store ("Global.Color.Canvas",    canvasColor);
		store ("Global.Color.Origin",    originColor);
		store ("Global.Color.Ribbon",    ribbonColor);
		store ("Global.Color.LeftPick",  leftPickColor);
		store ("Global.Color.RightPick", rightPickColor);
		store ("Global.Color.Grip",      gripColor);

		store ("Global.Size.Text",       textSize);
		store ("Global.Size.Aperture",   apertureSize);
		store ("Global.Size.Origin",     originSize);
		store ("Global.Size.Marker",     markerSize);
		store ("Global.Size.Grip",       gripSize);
		store ("Global.Density.Select",  selectDensity);
		store ("Global.Density.Ribbon",  ribbonDensity);

		store("Dimension.TextSize",        dimTextSize);
		store("Dimension.Style",            dimStyle);
		store("Dimension.precision.Length", dimLengthPrecision);
		store("Dimension.precision.Angle",  dimAnglePrecision);

//		store("Printing.Drawing.Scale",    printDrawingScale);
//		store("Printing.Page.Width",       printPageWidth);
//		store("Printing.Page.Height",      printPageHeight);
//		store("Printing.Page.Scale",       printPageScale);
//		store("Printing.Page.Unit",        printPageUnit);

		super.save();
	}


	cstring info ()
	{
		cstring i;

		i.appendln ("CanvasColor ",   canvasColor);
		i.appendln ("FreeSnapColor ", freeSnapColor);
		i.appendln ("ItemSnapColor ", itemSnapColor);
		i.appendln ("GridSnapColor ", gridSnapColor);
		i.appendln ("GridColor ",     gridColor);
		i.appendln ("CanvasColor ",   canvasColor);
		i.appendln ("OriginColor ",   originColor);
		i.appendln ("RibbonColor ",   ribbonColor);
		i.appendln ("leftPickColor ", leftPickColor);
		i.appendln ("rightPickColor ",rightPickColor);
		i.appendln ("GripColor ",     gripColor);

		i.appendln ("ApertureSize ",  apertureSize);
		i.appendln ("OriginSize ",    originSize);
		i.appendln ("MarkerSize ",    markerSize);
		i.appendln ("GripSize ",      gripSize);
		i.appendln ("SelectDensity ", selectDensity);
		i.appendln ("TextSize ",      textSize);

		i.appendln ("Dimension.TextSize ",        dimTextSize);
		i.appendln ("Dimension.Style ",           dimStyle);
		i.appendln ("Dimension.LengthPrecision ", dimLengthPrecision);
		i.appendln ("Dimension.AnglePrecision ",  dimAnglePrecision);

//		i.appendln ("Printing.DrawingScale ", printDrawingScale);
//		i.appendln ("Printing.PageWidth ",    printPageWidth);
//		i.appendln ("Printing.PageHeight ",   printPageHeight);
//		i.appendln ("Printing.PageScale ",    printPageScale);
//		i.appendln ("Printing.PageUnit ",     printPageUnit);

		return i;
	}
}

private WDSettings globalSettings;

WDSettings Settings ()
{
	if (!globalSettings) globalSettings = new WDSettings;
	return globalSettings;
}