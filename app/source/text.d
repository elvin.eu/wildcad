import std.conv;
import std.utf;
import std.math.constants;

import wcw;
import cairo;

import auxiliary;
import enums;
import namedobject;
import item;
import settings;
import painter;
import font;

class WDText : WDItem
{
private:
	cstring _text;
	DPair _base;
	double _size;
	double _angle;
	WDTextAlign _align;

	DPair[4] text_bounds () const
	{
		double width = _size/defaultFont.height*defaultFont.width(_text);
		double bx, by;

		leftBottom (bx, by);
		CairoMatrix m;
		m.translate (bx, by);
		m.rotate (rad(_angle));

		double x0 = 0.0;
		double y0 = 0.0;
		double x1 = width;
		double y1 = 0.0;
		double x2 = width;
		double y2 = _size;
		double x3 = 0.0;
		double y3 = _size;

		m.transform_point (x0, y0, x0, y0);
		m.transform_point (x1, y1, x1, y1);
		m.transform_point (x2, y2, x2, y2);
		m.transform_point (x3, y3, x3, y3);

		DPair[4] bs = [DPair(x0,y0), DPair(x1,y1), DPair(x2,y2), DPair(x3,y3)];	
		return bs;
	}

public:
	this (cstring tx, DPair bs, double sz, double an, WDTextAlign al)
	{
		init (tx, bs, sz, an, al);
	}

	this (const WDText t)
	{
		_base = t._base;
		_size = t._size;
		_angle = t._angle;
		_align = t._align;
		_text = t._text;
	}

	void init (cstring tx, DPair bs, double sz, double an, WDTextAlign al)
	{
		_base = bs;
		_size = sz;
		_angle = an;
		_align = al;

		try
		{
			validate(tx);
			_text = tx;
		}
		catch (Throwable)
		{
			_text = "invalid UTF-8 sequence";
			whereamif ("%s: %s", _text, cast(ubyte[]) tx);
		}
	}

	double size() const {return _size;}
	DPair base() const {return _base;}
	double angle () const {return _angle;}
	WDTextAlign alignment () const { return _align;}
	cstring text () const {return _text;}

	override WDItem clone ()
	{
		return cast (WDItem) new WDText (this);
	}

	override string toString () const
	{
		string s = "text @st " ~ pretty (_text) ~
			" @c1 " ~ pretty (_base) ~
			" @fs " ~ pretty (_size);
		if (_angle != 0.0) s ~= " @a1 " ~ pretty (_angle);
		if (_align != WDTextAlign.DEFAULT) s ~= " @ea " ~ pretty (_align);
		return s;
	}

	private void leftBottom (ref double bx, ref double by) const
	{
		import std.math: sin, cos;

		bx = _base.x;
		by = _base.y;
		double s = sin(rad(_angle));
		double c = cos(rad(_angle));
		double w = _size*defaultFont.width(_text)/defaultFont.height;

		auto h = _align & WDTextAlign.HMASK;
		auto v = _align & WDTextAlign.VMASK;

		if (h == WDTextAlign.RIGHT)
		{
			bx -= w*c;
			by -= w*s;
		}
		else if (h == WDTextAlign.HCENTER)
		{
			bx -= w*c/2;
			by -= w*s/2;
		}

		if (v == WDTextAlign.TOP)
		{
			bx += _size*s;
			by -= _size*c;
		}
		else if (v == WDTextAlign.BASE)
		{
			bx += _size*defaultFont.base/defaultFont.height*s;
			by -= _size*defaultFont.base/defaultFont.height*c;
		}
		else if (v == WDTextAlign.VCENTER)
		{
			bx += _size*s/2;
			by -= _size*c/2;
		}
	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		double bx=_base.x, by=_base.y;
		leftBottom (bx, by);
		matrix.transform_point (bx, by);
		p.drawText (defaultFont, _text, bx, by, _size*matrix.yy, _angle/180*PI);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST|WDItemSnap.CARDINAL)) != 0);
	}


	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		auto b = text_bounds();
		auto snap = wdTuple(DPair(), double.infinity);

		if (snapmode & WDItemSnap.NEAREST)
		{
			WDTuple!(DPair, double)[4] snaps;
			snaps[0].first  = lnear (b[0], b[1], pointer);
			snaps[0].second = pdist (snaps[0].first, pointer);
			snaps[1].first  = lnear (b[1], b[2], pointer);
			snaps[1].second = pdist (snaps[1].first, pointer);
			snaps[2].first  = lnear (b[2], b[3], pointer);
			snaps[2].second = pdist (snaps[2].first, pointer);
			snaps[3].first  = lnear (b[3], b[0], pointer);
			snaps[3].second = pdist (snaps[3].first, pointer);

			foreach (s; snaps)
				if ((s.second < snap.second))
					snap = s;
		} 
		else if (snapmode & WDItemSnap.CARDINAL)
		{
			double[4] dists;
			dists[0] = pdist (b[0], pointer);
			dists[1] = pdist (b[1], pointer);
			dists[2] = pdist (b[2], pointer);
			dists[3] = pdist (b[3], pointer);

			foreach (i, d; dists)
				if ((d < snap.second))
					snap = wdTuple(b[i], d);
		}
		return snap;
	}

//	WDTuple!(DPair, double, double) snapToGrip (DPair pos) const
//	void drawGrips (Painter p, CairoMatrix m, double den=1.0) {}
//	bool toggleGrip (DPair g) {return false;}
//	bool pickGrip (DRect window) {return false;}
//	void clearGrips () {}

	override bool inWindow (DRect r, bool entirely) const
	{
		auto b = text_bounds();

		if (r.contains (b[0]) &&
		    r.contains (b[1]) &&
		    r.contains (b[2]) &&
		    r.contains (b[3])) return true;

		if (entirely == false)
		{
			auto r0 = DPair (r.x0,r.y0);
			auto r1 = DPair (r.x1,r.y0);
			auto r2 = DPair (r.x1,r.y1);
			auto r3 = DPair (r.x0,r.y1);

			if ((intersect_ll (b[0], b[1], r0, r1).isReal) ||
			    (intersect_ll (b[0], b[1], r1, r2).isReal) ||
			    (intersect_ll (b[0], b[1], r2, r3).isReal) ||
			    (intersect_ll (b[0], b[1], r3, r0).isReal) ||
			    (intersect_ll (b[1], b[2], r0, r1).isReal) ||
			    (intersect_ll (b[1], b[2], r1, r2).isReal) ||
			    (intersect_ll (b[1], b[2], r2, r3).isReal) ||
			    (intersect_ll (b[1], b[2], r3, r0).isReal) ||
			    (intersect_ll (b[2], b[3], r0, r1).isReal) ||
			    (intersect_ll (b[2], b[3], r1, r2).isReal) ||
			    (intersect_ll (b[2], b[3], r2, r3).isReal) ||
			    (intersect_ll (b[2], b[3], r3, r0).isReal) ||
			    (intersect_ll (b[3], b[0], r0, r1).isReal) ||
			    (intersect_ll (b[3], b[0], r1, r2).isReal) ||
			    (intersect_ll (b[3], b[0], r2, r3).isReal) ||
			    (intersect_ll (b[3], b[0], r3, r0).isReal)) return true;
		}
		return false;
	}

	override DRect extents () const
	{
		import std.algorithm: min, max;

		auto b = text_bounds();		double dist = double.infinity;
		DPair snap;

		double minx = min (b[0].x, b[1].x, b[2].x, b[3].x);
		double maxx = max (b[0].x, b[1].x, b[2].x, b[3].x);
		double miny = min (b[0].y, b[1].y, b[2].y, b[3].y);
		double maxy = max (b[0].y, b[1].y, b[2].y, b[3].y);

		return DRect (DPair(minx, miny), DPair(maxx, maxy));
	}

	override bool encloses (DPair p) const
	{
		return false;
	}

//	DPair[] intersect (WDItem c) const {return intersections (cast(WDItem)this, c);}
//	DPair[] trimmingPoints() const {return [];}

	override void move   (DPair d)
	{
		_base += d;
	}

//	void drag   (DPair d) {move (d);}
//	void mirror (DPair a, DPair d) {}

	override void rotate (DPair c, double a)
	{
		CairoMatrix m;
		m.translate (c.x, c.y);
		m.rotate (rad(a));
		m.transform_point (_base.x, _base.y, _base.x, _base.y);
		_angle = normalize!180 (_angle+a);
	}

	override void scale  (DPair c, double s)
	{
		CairoMatrix m;
		m.translate (c.x, c.y);
		m.scale (s, s);
		m.transform_point (_base.x, _base.y, _base.x, _base.y);
		_size *= s;
	}

//	bool offset  ( double d, DPair s) {return false;}
//	bool fit (DPair from, DPair to) {return false;}
//	bool checkForCircular (int ttl) {return false;}

}