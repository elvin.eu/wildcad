import std.stdio;
import std.typecons;
import std.math;
import std.conv;
import std.string;
import std.algorithm;
import std.regex;

import wcw;

import interpreter;
import enums;
import auxiliary;
import item;

// file contains those classes:
// Token, End, Symbol, Name, Integer, RGB, Decimal, Couple, Coordinate, Fork, Directory

enum ParseRC {
	FALSE,
	TRUE,
	END,    // p. ended, End token reached
	COORD,  // mouse coordinate read in Coord token
	PICK
};

void delegate() current_command;

class Token
{
	cstring _type;
	cstring _name;

	cstring _lbr;
	cstring _rbr;

	this (cstring type, cstring name="")
	{
		_type = type;
		_name = name;
	}

	cstring type () const {return _type;}
	cstring name () const {return _name;}
	cstring hint () const
	{
		return (_lbr?_lbr:"") ~ _name ~ (_rbr?_rbr:"");
	}

	bool temp_coord () {return false;}

	abstract Token add (Token token);

	// if text can be parsed, return TRUE
	// if parsing is substituted, return substitution code (END, COORD, PICK)
	// else return FALSE
	abstract ParseRC can_parse (WDInterpreter inpr);

	// call can_parse and accept only TRUE
	abstract ParseRC parse (WDInterpreter inpr);

	Token store (ref Token t)
	{
		t = this;
		return this;
	}

//	Token end (void delegate() del=null)
//	{
//		if (del == null) del = current_command;
//		return add (new End (del));
//	}

	Token end ()
	{
		return add (new End (current_command));
	}

//	Token pick (cstring name, void delegate() del=null)
//	{
//		if (del == null) del = current_command;
//		return add (new Pick (name, del));
//	}

	Token pick (cstring name)
	{
		return add (new Pick (name, current_command));
	}

	Token entry (cstring name, uint s=0, void delegate() del=null)
	{
		current_command = del;
		return add (new Symbol (name, s));
	}

	Token symbol (cstring name, uint s=0)
	{
		return add (new Symbol (name, s));
	}

	Token name (cstring n)
	{
		return add (new Name (n));
	}

	Token integer (cstring name)
	{
		return add (new Integer (name));
	}

	Token rgb (cstring name)
	{
		return add (new RGB (name));
	}

	Token decimal (cstring name)
	{
		return add (new Decimal (name));
	}

	Token couple (cstring name)
	{
		return add (new Couple (name));
	}

	Token coordinate (cstring name, WDCoordMode mode=WDCoordMode.NONE)
	{
		return add (new Coordinate (name, mode, current_command));
	}

	void fork (ref Token t)
	{
		t = add (new Fork ());
	}
}

class End : Token
{
private:
	void delegate() cmd;

public:
	this (void delegate() c)
	{
		super ("End", "[ENTER]");
		cmd = c;
	}

	override Token add (Token t)
	{
		assert (0);
	}

	override ParseRC can_parse (WDInterpreter inpr)
	{
		if (inpr.word == "")
		{
			if (inpr.temporary) return ParseRC.END;
			else                return ParseRC.TRUE;
		}
		return ParseRC.FALSE;
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else if (cmd && !inpr.executed())
		{
			cmd ();
			inpr.setExecuted(true);
		}
		return ParseRC.TRUE;
	}
}

class Symbol : Token
{
	static WDTuple!(cstring, cstring)[] _values;

	Token _next;
	uint _shortcut;

	this (cstring name, uint s=0)
	{
		super ("symbol", name);
		_shortcut = s;
	}

	static WDTuple!(cstring, cstring)[] values () {return _values;}

	static void clear () {_values.length = 0;}

	static bool getValue (cstring name)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				return true;
			}
		}
		return false;
	}

	static bool getValue (cstring name, ref bool value)
	{
		value = getValue (name);
		return value;
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	override ParseRC can_parse (WDInterpreter inpr)
	{
		if (match (inpr.word, name, _shortcut))
		{
			return ParseRC.TRUE;
		}
		else
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else
		{
			_values ~= wdTuple (_name, _name);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}


class Pick : Token
{
private:
	void delegate() cmd;
	Token _next;

public:
	this (cstring name, void delegate() c)
	{
		cmd = c;
		_lbr = "[";
		_rbr = "]";
		super ("pick", name);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	override ParseRC can_parse (WDInterpreter inpr)
	{
		inpr.canvas.setMouseHint (WDMouseMode.PICK);

		if (inpr.canvas.pickedItem) return ParseRC.PICK;
		else                        return ParseRC.FALSE;
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else // ParseRC.PICK
		{
			if (cmd && !inpr.executed())
			{
				cmd();
				inpr.setExecuted(true);
			}

			inpr.setHint (hint);
			return ParseRC.TRUE;
		}
	}
}

class Name : Token
{
	static WDTuple!(cstring, cstring)[] _values;

	Token _next;

	this (cstring name)
	{
		super ("Name", name);
		_lbr = "\"";
		_rbr = "\"";
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, cstring)[] values () {return _values;};

	static bool getValue (cstring name, ref cstring value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list		// remove entry from list
				value = v.second;
				return true;
			}
		}
		return false;
	}

	static cstring getValue (cstring name)
	{
		cstring value;
		if (getValue (name, value)) return value;
		else                        throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	cstring _text;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		cstring w = inpr.word.strip_quotes;

		if (w.length == 0) return ParseRC.FALSE;

		_text = w;
		return ParseRC.TRUE;
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else
		{
			_values ~= wdTuple (_name, _text);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class Integer : Token
{
	static WDTuple!(cstring, int)[] _values;

	Token _next;

	this (cstring name)
	{
		super ("Integer", name);
		_lbr = "[";
		_rbr = "]";
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, int)[] values () {return _values;};

	static bool getValue (cstring name, ref int value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				value = v.second;
				return true;
			}
		}
		return false;
	}

	static int getValue (cstring name)
	{
		int value;
		if (getValue (name, value)) return value;
		else                        throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	int _int;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		try
		{
			_int = inpr.word.to!int;
			return ParseRC.TRUE;
		}
		catch (Throwable)
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;

		}
		else
		{
			_values ~= wdTuple (_name, _int);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class RGB : Token
{
	static WDTuple!(cstring, uint)[] _values;

	Token _next;

	this (cstring name)
	{
		super ("RGB", name);
		_lbr = "[";
		_rbr = "]";
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, uint)[] values () {return _values;};

	static bool getValue (cstring name, ref uint value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				value = v.second;
				return true;
			}
		}
		return false;
	}

	static uint getValue (cstring name)
	{
		uint value;
		if (getValue (name, value)) return value;
		else                        throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	uint _color;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		try
		{
			_color = argb_from_string (inpr.word);
			return ParseRC.TRUE;
		}
		catch (Throwable)
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else
		{
			_values ~= wdTuple (_name, _color);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class Decimal : Token
{
private:
	static WDTuple!(cstring, double)[] _values;
	Token _next;

public:

	this (cstring name)
	{
		super ("Decimal", name);
		_lbr = "[";
		_rbr = "]";
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, double)[] values () {return _values;};

	static bool getValue (cstring name, ref double value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				value = v.second;
				return true;
			}
		}
		return false;
	}

	static double getValue (cstring name)
	{
		double value;
		if (getValue (name, value)) return value;
		else                        throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	double _float;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		try
		{
			_float = inpr.word.to!double;
			return ParseRC.TRUE;
		}
		catch (Throwable)
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else
		{
			_values ~= wdTuple (_name, _float);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class Couple : Token
{
private:
	static WDTuple!(cstring, DPair)[] _values;

	Token _next;

public:

	this (cstring name)
	{
		super ("Couple", name);
		_lbr = "(";
		_rbr = ")";
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, DPair)[] values () {return _values;};

	static bool getValue (cstring name, ref DPair value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				value =  v.second;
				return true;
			}
		}
		return false;
	}

	static DPair getValue (cstring name)
	{
		DPair value;
		if (getValue (name, value)) return value;
		else                        throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	DPair _pair;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		try
		{
			auto rex = ctRegex!("^(.+)(,)(.+)$");
			auto match = matchFirst (inpr.word, rex);
			_pair = DPair(match[1].to!double, match[3].to!double);
			return ParseRC.TRUE;
		}
		catch (Throwable)
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		if (can_parse (inpr) == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else
		{
			_values ~= wdTuple (_name, _pair);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class Coordinate : Token
{
public:
	static WDTuple!(cstring, DPair)[] _values;

	Token _next;
	void delegate() cmd;
	bool _base;
	bool _ortho;
	bool _free;

public:

	this (cstring name, WDCoordMode mode=WDCoordMode.NONE, void delegate() del=null)
	{
		super ("Coordinate", name);
		_lbr = "(";
		_rbr = ")";
		_ortho = (mode & WDCoordMode.ORTHO) != 0;
		_base =  (mode & WDCoordMode.BASE) != 0;
		_free =  (mode & WDCoordMode.FREE) != 0;
		cmd = del;
	}

	static void clear () {_values.length = 0;}

	static WDTuple!(cstring, DPair)[] values () {return _values;};

	static bool getValue (cstring name, ref DPair value)
	{
		foreach (size_t i, v; _values)
		{
			if (v.first == name)
			{
				_values = _values[0..i] ~ _values [i+1..$];		// remove entry from list
				value = v.second;
				return true;
			}
		}
		return false;
	}

	static DPair getValue (cstring name)
	{
		DPair value;
		if (getValue (name, value)) return value;
		throw new Exception (("no value '" ~ name ~ "' found").idup);
	}

	override Token add (Token t)
	{
		_next = t;
		return t;
	}

	DPair _pair;

	override ParseRC can_parse (WDInterpreter inpr)
	{
		if (_free == false) inpr.canvas.snapOn (true);

		// if no coordinate, try mouse tracking
		if ((inpr.word == "") && inpr.temporary && inpr.mousecoord)
		{
			auto p = inpr.canvas.worldPos;

			// Check, if we're in orthogonal mode
			if ((_ortho)  && inpr.canvas().orthogonal())
			{
				auto r = inpr.basepoint;

				// what's greater, dx or dy?
				if (abs(p.x-r.x) > abs(p.y-r.y)) _pair = DPair (p.x, r.y);
				else                             _pair = DPair (r.x, p.y);
			}
			else
			{
				_pair = p;
			}

			return ParseRC.COORD;
		}

		// else

		try
		{
			auto rex = ctRegex!("^([@#])?(.+)([,°])(.+)$");
			auto match = matchFirst (inpr.word, rex);
			auto d1 = match[2].to!double;
			auto d2 = match[4].to!double;
			DPair p;

			if (match[3] == "°")
			{
				p.x = cos(rad(d1))*d2;
				p.y = sin(rad(d1))*d2;
			}
			else
			{
				p.x = d1;
				p.y = d2;
			}

			if      (match[1] == "@") p += inpr.basepoint;
			else if (match[1] == "#") {} // absolute coordinates
			else                      p += inpr.canvas.activeView.origin;

			// Check, if we're in orthogonal mode
			if ((_ortho)  && inpr.canvas().orthogonal())
			{
				auto r = inpr.basepoint;

				// what's greater, dx or dy?
				if (abs(p.x-r.x) > abs(p.y-r.y)) _pair = DPair (p.x, r.y);
				else                             _pair = DPair (r.x, p.y);
			}
			else
			{
				_pair = p;
			}

			return ParseRC.TRUE;
		}
		catch (Throwable)
		{
			return ParseRC.FALSE;
		}
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_next, "Token '" ~ _name ~ "' has no next");

		auto cp = can_parse (inpr);

		if (cp == ParseRC.FALSE)
		{
			inpr.setHint (hint);
			return ParseRC.FALSE;
		}
		else if (cp == ParseRC.COORD)
		{
			inpr.setMousecoord (false);
			inpr.canvas().setMouseHint (WDMouseMode.COORD); // tell console that coordinate is wanted!

//			if (_base) inpr.setRelative (_pair);	-- this is unneccessary!
			_values ~= wdTuple (_name, _pair);
			if (cmd && !inpr.executed())
			{
				cmd();
				inpr.setExecuted(true);
			}

			inpr.setHint (hint);
			return ParseRC.TRUE;
		}
		else
		{
			if (_base) inpr.setBasepoint (_pair);
			_values ~= wdTuple (_name, _pair);
			inpr.read_next();
			return _next.parse (inpr);
		}
	}
}

class Fork : Token
{
	Token[] _tokens;

	this ()
	{
		super ("Fork");
	}

	private this (cstring type, cstring name)
	{
		super (type, name);
	}

	override Token add (Token t)
	{
		_tokens ~= t;
		return t;
	}

	override cstring hint () const
	{
		string h;
		foreach (t; _tokens)
		{
			assert (t, _name ~ " '" ~ type ~ "' contains 'null' token");
			h ~= t.hint ~ " ";
		}
		return h;
	}

	override ParseRC can_parse (WDInterpreter inpr)
	{
		assert (_tokens.length > 0, _type ~ " contains no tokens");

		foreach (t; _tokens)
		{
			assert (t, _name ~ " '" ~ type ~ "' contains 'null' token");

			if (t.can_parse (inpr) == ParseRC.TRUE) return ParseRC.TRUE;
		}
		return ParseRC.FALSE;
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_tokens.length > 0);

		foreach (t; _tokens)
		{
			assert (t, _name ~ " '" ~ type ~ "' contains 'null' token");

			auto cp = t.can_parse (inpr);

			if (cp == ParseRC.TRUE)
			{
				return t.parse (inpr);
			}
			else if (cp == ParseRC.COORD)
			{
				auto prc = t.parse (inpr);
				inpr.setHint (hint);
				return prc;
			}
			else if (cp == ParseRC.PICK)
			{
				auto prc = t.parse (inpr);
				inpr.setHint (hint);
				return prc;

			}
		}

		inpr.setHint (hint);
		return ParseRC.FALSE;
	}
}

class Directory : Fork
{
private:
	static cstring _lastcommand;

public:
	this ()
	{
		super ("Directory", "");
	}

	override ParseRC parse (WDInterpreter inpr)
	{
		assert (_tokens.length > 0);

		Symbol.clear;
		Name.clear;
		RGB.clear;
		Integer.clear;
		Decimal.clear;
		Coordinate.clear;

		// Search directory reverse, so that newer definitions overwrite older ones.
		// This could be used in future to extend the directory
		foreach_reverse (t; _tokens)
		{
			assert (t, _name ~ " '" ~ type ~ "' contains 'null' token");

			if (t.can_parse (inpr))
			{
				_lastcommand = t.name;
				return t.parse (inpr);
			}
		}

		return ParseRC.FALSE;
	}

	static cstring lastCommand () {return _lastcommand;}
}
