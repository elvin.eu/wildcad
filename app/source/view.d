// view

import std.string;

import wcw;

import namedobject;
import canvas;
import enums;
import auxiliary;

class WDView : WDNamedObject
{
private:
	const (char)[] _name;
	uint   _id;
	int    _prio;

	WDItemSnap _snapmode = WDItemSnap.NONE;
	DRect  _window  = DRect (DPair(-5.0, -5.0), 110, 110);	// the current zoom window
	DPair  _grid    = DPair (1.0, 1.0);
	DPair  _origin  = DPair (0.0, 0.0);

public:
	mixin no;

	this (const(char)[] nm, int prio=0)
	{
		no_init (nm, prio);
	}

	cstring info () const
	{
		return _name;
	}

	WDItemSnap snapMode () const  {return _snapmode;}
	void setSnapMode (WDItemSnap s) {_snapmode=s;}

	DRect window () const {return _window;}
	void setWindow (DRect w)
	{
		assert (w.notReal == false);
		_window = w;
	}

	Pair!double grid () const {return _grid;}
	void setGrid (Pair!double g)
	{
		// negative grids are converted to positive
		// 0.0 grids are assumed as 1.0
		if      (g.x == 0.0) g.x = 1.0;
		else if (g.x < 0.0) g.x = -g.x;
		if      (g.y == 0.0) g.y = 1.0;
		else if (g.y < 0.0) g.y = -g.y;
		_grid = g;
	}

	Pair!double origin () const {return _origin;}
	void setOrigin (Pair!double o) {_origin=o;}

	void update (const WDView v)
	{
		_snapmode = v.snapMode();
		_window   = v.window();
		_grid     = v.grid();
		_origin   = v.origin();
	}

}

class WDViewTable : WDNamedList!WDView
{
mixin nt!WDView;

private:
	WDView  _activeView;

	WDView[] _list;
	const(char)[] _defaultname;		// default name for new NamedObjects


public:

	this ()
	{
		nt_init ("new view");
		_activeView = new WDView ("initial view");
	}

	cstring info ()
	{
		string s = " Views:";
		foreach (view; _list) s ~= "\n " ~ view.info;
		return s;
	}

	inout (WDView) activeView () inout
	{
		return _activeView;
	}

	bool update (const(char)[] name, WDView from)
	{
		auto view = find (name);
		if (view is null) return false;

		view.update (from);
		return true;
	}

	bool activate (const(char)[] name)
	{
		auto result = find (name);
		if (result is null) return false;

		_activeView.update (result);
		_activeView.setName (result.name);
		return true;
	}
}
