import std.stdio;
import std.regex;
import std.conv;
import std.string;
import std.math;

import auxiliary;
import enums;
import canvas;
import layer;
import layertable;
//import settings;
//import view;

import arc;
import curve;
import circle;
import dimension;
import line;
import marker;
import polyline;
import ray;
import reference;
import text;
import xline;

import wcw.auxiliary;
import wcw.color;

class WDFReader
{
public:
	static bool read  (File f, WDCanvas cv)
	{
		bool rc = false;

		try
		{
			auto r = new WDFReader (f, cv);
			r.canvas();
			rc = r.parse ();
			r.canvas();
		}
		catch (Throwable)
		{}

		return rc;
	}

	static bool read  (mstring t,  DPair to, WDCanvas cv)
	{
		bool rc = false;

		try
		{
			auto r = new WDFReader (t, to, cv);
			r.canvas();
			rc = r.parse ();
			r.canvas();
		}
		catch (Throwable)
		{}

		return rc;
	}

private:
	WDCanvas _canvas;
	WDBlock _activeBlock;
	WDLayer _activeLayer;

	File _file;
	mstring _text;

	cstring _line;
	size_t _pos, _oldpos;
	cstring _word, _oldword;
	int _position;

	DPair _anchor = DPair (0, 0);
	cstring _activeLayerName;

	this (File f, WDCanvas cv)
	{
		_canvas = cv;
		_file = f;
	}

	this (char[] t, DPair to, WDCanvas cv)
	{
		_canvas = cv;
		_text = t;
		_anchor = to;
	}

	bool parse ()
	{
		bool rc = true;

		if (_file.isOpen())
		{
			auto txt = _file.byLine();
			int lnr = 0;

			foreach (ln; txt)
			{
				lnr++;
				if (!parseLine (ln))
				{
					rc = false;
					writefln ("Error reading line %s: %s", lnr, _line);
				}
			}
		}
		else
		{
			auto txt = _text.splitLines();
			int lnr = 0;

			foreach (ln; txt)
			{
				lnr++;
				if (!parseLine (ln))
				{
					rc = false;
					writefln ("Error reading line %s: %s", lnr, _line);
				}
			}
		}

		_canvas.activateLayer (_activeLayerName);
		return rc;
	}

	bool parseLine (mstring ln)
	{
		bool rc = true;

		_line = ln.strip;
		_pos = 0;
		next();

//			if      (symbol ("arbitrary")) rc = arbitrary ();
		if      (symbol ("anchor"))    rc = anchor ();
		else if (symbol ("arc"))       rc = arc ();
		else if (symbol ("block"))     rc = block ();
		else if (symbol ("curve"))   rc = curve ();
		else if (symbol ("canvas"))    rc = canvas ();
		else if (symbol ("circle"))    rc = circle ();
		else if (symbol ("dimension")) rc = dimension ();
		else if (symbol ("grid"))      rc = grid ();
		else if (symbol ("line"))      rc = line ();
		else if (symbol ("layer"))     rc = layer ();
		else if (symbol ("marker"))    rc = marker ();
		else if (symbol ("origin"))    rc = origin ();
//			else if (symbol ("pan"))       rc = pan ();
//			else if (symbol ("page"))      rc = page ();
//		else if (symbol ("polyline"))  rc = polyline ();
//			else if (symbol ("precision")) rc = precision ();
		else if (symbol ("ray"))       rc = ray ();
		else if (symbol ("reference")) rc = reference ();
//			else if (symbol ("size"))      rc = size ();
//			else if (symbol ("snap"))      rc = snap ();
		else if (symbol ("text"))      rc = text ();
		else if (symbol ("view"))      rc = view ();
		else if (symbol ("xline"))     rc = xline ();
		else if (symbol ("zoom"))      rc = zoom ();
		else if (comment ())           rc = true;
		else if (!_word.empty())       rc = false;
		else                           rc = false;

		return rc;
	}

	void next ()
	{
		_oldpos = _pos;
		_oldword = _word;
		_word = auxiliary.nextWord (_line, _pos);
	}

	void rewind ()
	{
		_pos = _oldpos;
		_word = _oldword;
	}

	bool comment ()
	{
		import std.algorithm: startsWith;

	    if (_word.empty || _word.startsWith ("#"))
		{
			_word = "";
			return true;
		}

		return false;
	}

	bool symbol (string s)
	{
		if (_word != s) return false;

		next();
		return true;
	}

	bool parameter (string a)
	{
		return symbol ("@" ~ a);
	}

	bool coordinate (string sym, ref DPair d)
	{
		if (!parameter (sym)) return false;

		int i=0; while (_word[i] != ' ') ++i;
		int j=i; while (_word[j] == ' ') ++j;

		cstring x = _word [0..i];
		cstring y = _word [j..$];

		try
		{
			d.x = _word [0..i].to!double;
			d.y = _word [j..$].to!double;
			next();
			return true;
		}
		catch (Throwable)
		{
			rewind;
			return false;
		}
	}


	bool floating (string sym, ref double f)
	{
		if (!parameter (sym)) return false;

		try
		{
			f = _word.to!double;
			next();
			return true;
		}
		catch (Throwable)
		{
			rewind;
			return false;
		}
	}


	bool argb (string sym, ref Color c)
	{
		if (!parameter (sym)) return false;

		uint u = 0xff000000;
		try
		{
			c = argb_from_string (_word);
			next();
			return true;
		}
		catch (Throwable)
		{
			rewind;
			return false;
		}
	}

	bool name (string sym, ref cstring s)
	{
		if (!parameter (sym)) return false;

		s = _word.strip_quotes.dup;
		next();
		return true;
	}

//	bool attribute (string sym, ref WDItemSnap ra)
//	{
//		if (!parameter (sym)) return false;

//		foreach (char c; _word)
//		{
//			if      (c == 'n') ra |= WDItemSnap.NEAREST;
//			else if (c == 'e') ra |= WDItemSnap.END;
//			else if (c == 'm') ra |= WDItemSnap.MIDDLE;
//			else if (c == 'c') ra |= WDItemSnap.CENTER;
//			else if (c == 'q') ra |= WDItemSnap.CARDINAL;
//			else if (c == 'i') ra |= WDItemSnap.INTERSECTION;
//			else return false;
//		}
//		next();
//		return true;
//	}

	bool attribute (string sym, ref WDTextAlign ra)
	{
		if (!parameter (sym)) return false;

		WDTextAlign h, v;

		foreach (char c; _word)
		{
			if      (c == 'l') h = WDTextAlign.LEFT;
			else if (c == 'r') h = WDTextAlign.RIGHT;
			else if (c == 'h') h = WDTextAlign.HCENTER;
			else if (c == 't') v = WDTextAlign.TOP;
			else if (c == 's') v = WDTextAlign.BASE;
			else if (c == 'b') v = WDTextAlign.BOTTOM;
			else if (c == 'v') v = WDTextAlign.VCENTER;
			else                   return false;
		}
		ra = h|v;
		next();
		return true;
	}

	bool attribute (string sym, ref WDDimensionType dt)
	{
		if (!parameter (sym)) return false;

		if      (_word == "h") dt = WDDimensionType.HORIZONTAL;
		else if (_word == "v") dt = WDDimensionType.VERTICAL;
		else if (_word == "a") dt = WDDimensionType.ALIGNED;
		else                   return false;

		next();
		return true;
	}

	bool attribute (string sym, ref WDPolylineType pt)
	{
		if (!parameter (sym)) return false;

		if      (_word == "o") pt = WDPolylineType.OPEN;
		else if (_word == "c") pt = WDPolylineType.CLOSED;
		else                   return false;

		next();
		return true;
	}

	bool attribute (string sym, ref WDLayerAttribute la)
	{
		if (!parameter (sym)) return false;

		la = WDLayerAttribute.NONE;
		foreach (char c; _word)
		{
			if      (c == 'a') la |= WDLayerAttribute.ACTIVE;
			else if (c == 'h') la |= WDLayerAttribute.HIDDEN;
			else if (c == 'f') la |= WDLayerAttribute.FROZEN;
			else if (c == 'l') la |= WDLayerAttribute.LOCKED;
		}

		next();
		return true;
	}


//	bool arbitrary ()
//	{
//		bool l, a;
//		DPair d0, d1;
//		double f0, f1, f2;

//		for (;;)
//		{
//			if (symbol ("line") && coordinate (d0) && coordinate (d1))
//			{
//				_canvas.addItemToActive (new WDLine (d0, d1));
//			}
//			else if (symbol ("arc") && coordinate (d0) && floating (f0) && floating (f1) && floating (f2))
//			{
//				_canvas.addItemToActive (new WDArc (d0, f0, f1, f2));
//			}
//			else
//			{
//				return false;
//			}

//			if (!symbol (";")) return rc;
//		}
//	}

	bool anchor ()
	{
		DPair ca;
		if (coordinate ("ca", ca)) _anchor -= ca;
		return true;
	}

	bool arc ()
	{
		bool rc = true;
		DPair cc;
		double fr, a1, a2;

		while (_word != "")
		{
			if ((coordinate ("cc", cc) ||
				floating ("fr", fr) ||
				floating ("a1", a1) ||
				floating ("a2", a2)) == false)
			{
				rc = false;
				next;
			}
		}

		if (cc.notReal || fr.notReal || a1.notReal || a2.notReal) return false;

		auto item = new WDArc (cc, fr, a1, a2);
		item.move(_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool block ()
	{
		bool rc = true;
		cstring sn;

		while (_word != "")
		{
			if (name ("sn", sn) == false)
			{
				rc = false;
				next;
			}
		}
		if (sn == "") return false;

		if (_canvas.createBlock (sn, DPair(0,0)) is null)
		{
			_canvas.activateLayer (sn);
		}

		_activeBlock = _canvas.activeBlock;
		_activeLayer = _canvas.activeLayer;
		return rc;
	}

	bool curve ()
	{
		bool rc = true;
		DPair c1, c2, d1, d2;

		while (_word != "")
		{
			if ((coordinate ("c1", c1) ||
				 coordinate ("c2", c2) ||
				 coordinate ("d1", d1) ||
				 coordinate ("d2", d2)) == false)
			{
				rc = false;
				next;
			}
		}

		if (c1.notReal || c2.notReal || d1.notReal || d2.notReal ) return false;
		auto item = new WDCurve (c1, c2, d1, d2);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool canvas ()
	{
		auto c = _canvas.activateCanvas();
		_activeBlock = _canvas.activeBlock;
		_activeLayer = _canvas.activeLayer;
		return c;
	}

	bool circle ()
	{
		bool rc = true;
		DPair cc;
		double fr;

		while (_word != "")
		{
			if ((coordinate ("cc", cc) ||
				floating ("fr", fr)) == false)
			{
				rc = false;
				next;
			}
		}
		if (cc.notReal || fr.notReal) return false;

		auto item = new WDCircle (cc, fr);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

//	bool color () {whereami; return false;}


	bool dimension ()
	{
		bool rc = true;
		WDDimensionType et = WDDimensionType.ALIGNED;
		DPair c1, c2, cl, tc1;
		cstring tst;
		double tfs, ta1;
		WDTextAlign tea;

		while (_word != "")
		{
			if ((attribute ("et", et) ||
				coordinate ("c1", c1) ||
				coordinate ("c2", c2) ||
				coordinate ("cl", cl) ||
				coordinate ("tc1", tc1) ||
				attribute ("tea", tea) ||
				floating ("tfs", tfs) ||
//				floating ("ta1", ta1) ||
				name ("tst", tst)) == false)
			{
				rc = false;
				next;
			}
		}

		if (c1.notReal || c2.notReal || cl.notReal) return false;
		if (tc1.notReal) tc1 = c1;
		if (tfs.notReal) tfs = 5;
//		if (ta1.notReal) ta1 = 0;

		auto item = new WDDimension (et, c1, c2, cl, tfs, tst);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool grid()
	{
		bool rc = true;
		DPair cs;

		while (_word != "")
		{
			if (coordinate ("cs", cs) == false)
			{
				rc = false;
				next;
			}
		}

		if (cs.notReal) return false;

		_canvas.setGrid (cs);
		return rc;
	}

	bool layer ()
	{
		bool rc = true;
		cstring sn;
		WDLayerAttribute ea;
		Color xc;

		while (_word != "")
		{
			if ((name ("sn", sn) || 				// layer name
				argb ("xc", xc) || 					// color
				attribute ("ea", ea)) == false)		// attributes (active, hidden, frozen, locked)
			{
				rc = false;
				next;
			}
		}

		if (sn == "") return false;

		if (ea & WDLayerAttribute.ACTIVE) _activeLayerName = sn;

		if (_canvas.createLayer (sn))
		{
			_canvas.setLayerColor (sn, xc);
			_canvas.setLayerAttribute (sn, ea);
		}
		else
		{
			_canvas.activateLayer (sn);
		}

		_activeBlock = _canvas.activeBlock;
		_activeLayer = _canvas.activeLayer;
		return rc;
	}

	bool line ()
	{
		bool rc = true;
		DPair c1, c2;

		while (_word != "")
		{
			if ((coordinate ("c1", c1) ||
				coordinate ("c2", c2)) == false)
			{
				rc = false;
				next;
			}
		}

		if (c1.notReal || c2.notReal) return false;

		auto item = new WDLine (c1, c2);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool marker ()
	{
		bool rc = true;
		DPair c1;

		while (_word != "")
		{
			if (coordinate ("c1", c1) == false)
			{
				rc = false;
				next;
			}
		}
		if (c1.notReal) return false;

		auto item = new WDMarker (c1);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool origin ()
	{
		bool rc = true;
		DPair c1;

		while (_word != "")
		{
			if (coordinate ("c1", c1) == false)
			{
				rc = false;
				next;
			}
		}
		if (c1.notReal) return false;

		_canvas.setOrigin (c1);
		return rc;
	}

	bool pan () {whereami; return false;}
	bool page () {whereami; return false;}

	bool polyline ()
	{
		WDTuple!(DPair, double)[] points;

		DPair cn;
		double fb;
		WDPolylineType et;

		bool a, c, f;
		bool rc = true;

		attribute ("et", et);

		while (_word != "")
		{
			c = coordinate ("cn", cn);
			f = floating ("fb", fb);

			if (c)
			{
				if (fb.isNaN) fb = 0.0;
				if (cn.isReal && fb.isReal) points ~= wdTuple(cn, fb);
			}
			else
			{
				rc = false;
				next;
			}
		}

		auto item = new WDPolyline (points, et);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool precision () {whereami; return false;}

	bool ray ()
	{
		bool rc = true;
		DPair c1, c2;

		while (_word != "")
		{
			if ((coordinate ("c1", c1) ||
				coordinate ("c2", c2)) == false)
			{
				rc = false;
				next;
			}
		}
		if (c1.notReal || c2.notReal) return false;

		auto item = new WDRay (c1, c2);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool reference ()
	{
		bool rc = true;
		cstring sn;
		DPair c1;
		DPair cs = DPair (1,1);
		double a1 = 0;

		while (_word != "")
		{
			if ((name ("sn", sn) ||
				coordinate ("c1", c1) ||
				coordinate ("cs", cs) ||
				floating ("a1", a1)) == false)
			{
				rc = false;
				next;
			}
		}

		if (sn == "" || c1.notReal) return false;

		auto block = _canvas.findBlock (sn);
		if (block is null) return false;

		auto item = new WDReference (block, c1, cs, a1);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

//	bool snap ()
//	{
//		bool rc = true;
//		WDItemSnap ea;

//		while (_word != "")
//		{
//			if (attribute ("ea", ea) == false)
//			{
//				rc = false;
//				next;
//			}
//		}

//		_canvas.setSnapMode (ea);
//		return rc;
//	}

//	bool size () {whereami; return false;}

	bool text ()
	{
		bool rc = true;
		cstring st;
		DPair c1;
		double ns = 5.0;
		double a1 = 0.0;
		WDTextAlign ea;

		while (_word != "")
		{
			if ((coordinate ("c1", c1) ||
				floating ("fs", ns) ||
				floating ("a1", a1) ||
				attribute ("ea", ea) ||
				name ("st", st)) == false)
			{
				rc = false;
				next;
			}
		}

		if (c1.notReal || st == "") return false;

		auto item = new WDText (st, c1, ns, a1, ea);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool view() {whereami; return false;}

	bool xline()
	{
		bool rc = true;
		DPair c1, c2;

		while (_word != "")
		{
			if ((coordinate ("c1", c1) ||
				coordinate ("c2", c2)) == false)
			{
				rc = false;
				next;
			}
		}
		if (c1.notReal || c2.notReal) return false;

		auto item = new WDXLine (c1, c2);
		item.move (_anchor);
		_activeBlock.addItemToLayer (item, _activeLayer);
		return rc;
	}

	bool zoom () {whereami; return false;}
}
