import std.stdio;
import std.range;

import canvas;
import layertable;
import settings;
import auxiliary;
import layer;
import view;

class WDFWriter
{
public:
	static bool write  (File f, WDCanvas cv)
	{
		auto w = new WDFWriter (f, cv);

		f.writeln ("# WDF Version 0.1");

//		w.writeGlobals      ();
		w.writeViews        ();
		w.writeCreateLayers ();
		w.writeBlockItems   ();
		w.writeCanvasItems  ();

		return true;
	}

private:
	WDCanvas _canvas;
	File    _file;

	this (File f, WDCanvas cv)
	{
		_file = f;
		_canvas = cv;
	}

	void writeViews  ()
	{
		foreach (view; _canvas.viewTable.list)
		{
			_file.writeln();
			_file.write ("view @sn ", pretty (view.name),
				" @r1 ", pretty (view.window),
				" @c1 ", pretty (view.origin),
				" @cs ", pretty (view.grid),
				" @es ", pretty (view.snapMode));

		}

		WDView active = _canvas.activeView;
		_file.writeln();
		_file.writeln ("origin @c1 ", pretty (active.origin));
		_file.writeln ("grid @cs ",  active.grid());
		_file.writeln ("snap ",  pretty (active.snapMode));
	}

//	void writeGlobals ()
//	{
//		_file.writeln ("size text ",        Settings.textSize);
//		_file.writeln ("size dimtext ",     Settings.dimTextSize);
//		_file.writeln ("precision angle ",  Settings.dimAnglePrecision);
//		_file.writeln ("precision length ", Settings.dimLengthPrecision);
//	}

	void writeCreateLayers ()
	{
		_file.writeln;
		foreach (layer; _canvas.layerTable.list)
		{
			_file.write ("layer @sn ", pretty(layer.name), " @xc ", pretty (layer.color));

			WDLayer active = _canvas.activeBlock().activeLayer();
			cstring attr;

			if (layer == active) attr ~= "a";
			if      (layer.isHidden) attr ~= "h";
			else if (layer.isFrozen) attr ~= "f";
			else if (layer.isLocked) attr ~= "l";
			if (attr != "") attr = " @ea " ~ attr;

			_file.writeln (attr);
		}
	}

//	void writeCreateBlocks ()
//	{
//		_file.writeln;
//		foreach (block; _canvas.blockTable.list)
//		{
//			_file.writeln ("block @sn ", pretty (block.name));
//		}
//	}

	void writeBlockItems ()
	{
		foreach (block; _canvas.blockTable.list)
		{
			// don't write empty blocks
			if (block.list.empty) continue;
			_file.writeln;
			_file.writeln ("block @sn ", pretty (block.name));
			writeLayerItems__ (block);
		}
	}

	void writeCanvasItems ()
	{
		_file.writeln;
		 _file.writeln ("canvas");
		writeLayerItems__ (_canvas.layerTable);
	}

	void writeLayerItems__ (WDLayerTable layertable)
	{
		foreach (layer; layertable.list)
		{
			if (layer.empty) continue;

			_file.writeln;
			_file.writeln ("layer @sn ", pretty (layer.name));
			_file.writeln;

			// first for not selected items
			foreach (item; layer.items[])
			{
				_file.writeln (item.toString);
			}
			// then for selected items
			foreach (item; layer.selected[])
			{
				_file.writeln (item.toString);
			}
		}
	}
}
