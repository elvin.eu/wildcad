import std.stdio;
import std.conv;
import std.math;

import wcw;

import icons;
import canvas;
import console;
import interpreter;
import settings;
import view;
import enums;

void main (string [] args)
{
	App (args);
	auto _wildcad = new WildCAD ();
	_wildcad.show;

	if (App.parameters.length == 2)
	{
		_wildcad._canvas.openFile (App.parameters[1]);
	}

	App.run;
	App.destroy;
}

class WildCAD : Window
{
	WDCanvas  _canvas;
	WDConsole _console;
	WDInterpreter _interpreter;
	HorizontalWidget _infobar;
	Label _infox, _infoy, _infogrid;
	Label _status;

	bool _final;
	bool _modified;
	int _newfilecount;
	const (char)[] _filename;
	const (char)[] _currenttext;


	ToolButton _tool_gripmode;
	ToolButton _tool_orthogonal;
	ToolButton _tool_snap_end;
	ToolButton _tool_snap_middle;
	ToolButton _tool_snap_center;
	ToolButton _tool_snap_cardinal;
	ToolButton _tool_snap_nearest;
	ToolButton _tool_snap_intersection;
	ToolButton _tool_snap_perpendicular;
	ToolButton _tool_snap_tangent;
	ToolButton _tool_select_layer;
	ToolButton _tool_select_view;
	ToolButton _tool_select_block;

	this ()
	{
		new VerticalLayout (this);

		auto wildcad_img = new Image (WD_wildcad_png);
		setApplicationIcon (wildcad_img);


		auto menubar = new MenuBar (this);

		auto menu_file   = new Menu ("File", menubar);
		auto item_open   = new MenuItem ("Open", menu_file);
		auto item_save   = new MenuItem ("Save", menu_file);
		auto item_saveas = new MenuItem ("SaveAs", menu_file);

//		separator needed
		auto item_print = new MenuItem ("Print", menu_file);
//		separator needed
		auto item_close = new MenuItem ("Close", menu_file);
		auto item_quit  = new MenuItem ("Quit", menu_file);

		item_open.clicked.connect (&act_open);
		item_save.clicked.connect (&act_save);
		item_saveas.clicked.connect (&act_saveAs);
		item_print.clicked.connect (&act_print);
		item_close.clicked.connect (&act_close);
		item_quit.clicked.connect (&act_quit);

		auto menu_edit  = new Menu ("Edit", menubar);
		auto item_cut   = new MenuItem ("cut",   menu_edit);
		auto item_copy  = new MenuItem ("copy",  menu_edit);
		auto item_paste = new MenuItem ("paste", menu_edit);

		item_cut.clicked.connect (&act_cut);
		item_copy.clicked.connect (&act_copy);
		item_paste.clicked.connect (&act_paste);

		auto menu_view    = new Menu ("View", menubar);
		auto item_canvas  = new MenuItem ("Canvas", menu_view);
		auto item_zoom    = new MenuItem ("Zoom", menu_view);
		auto item_pan     = new MenuItem ("Pan", menu_view);
		auto item_measure = new MenuItem ("Measure", menu_view);

		item_canvas.clicked.connect (&act_canvas);
		item_zoom.clicked.connect (&act_zoom);
		item_pan.clicked.connect (&act_pan);
		item_measure.clicked.connect (&act_measure);

		auto menu_draw      = new Menu ("Draw", menubar);
		auto item_arc       = new MenuItem ("Arc", menu_draw);
		auto item_circle    = new MenuItem ("Circle", menu_draw);
		auto item_curve     = new MenuItem ("Curve", menu_draw);
		auto item_dimension = new MenuItem ("Dimension", menu_draw);
		auto item_line      = new MenuItem ("Line", menu_draw);
		auto item_marker    = new MenuItem ("Marker", menu_draw);
		auto item_ray       = new MenuItem ("Ray", menu_draw);
		auto item_rectangle = new MenuItem ("Rectangle", menu_draw);
		auto item_text      = new MenuItem ("Text", menu_draw);
		auto item_xline     = new MenuItem ("Xline", menu_draw);

		item_arc.clicked.connect (&act_arc);
		item_curve.clicked.connect (&act_curve);
		item_circle.clicked.connect (&act_circle);
		item_dimension.clicked.connect (&act_dimension);
		item_line.clicked.connect (&act_line);
		item_marker.clicked.connect (&act_marker);
		item_ray.clicked.connect (&act_ray);
		item_rectangle.clicked.connect (&act_rectangle);
		item_text.clicked.connect (&act_text);
		item_xline.clicked.connect (&act_xline);

		auto menu_modify      = new Menu ("Modify", menubar);
		auto item_pickall     = new MenuItem ("Pick All", menu_modify);
		auto item_picknothing = new MenuItem ("Pick Nothing", menu_modify);
		auto item_erase       = new MenuItem ("Erase", menu_modify);
//		separator needed
		auto item_origin = new MenuItem ("Origin", menu_modify);
		auto item_grid = new MenuItem ("Grid", menu_modify);
//		separator needed
		auto item_reference = new MenuItem ("Reference", menu_modify);
		auto item_offset    = new MenuItem ("Offset", menu_modify);
		auto item_extend    = new MenuItem ("Extend", menu_modify);
		auto item_mirror    = new MenuItem ("Mirror", menu_modify);
		auto item_move      = new MenuItem ("Move", menu_modify);
		auto item_rotate    = new MenuItem ("Rotate", menu_modify);
		auto item_scale     = new MenuItem ("Scale", menu_modify);
		auto item_trim      = new MenuItem ("Trim", menu_modify);

		item_pickall.clicked.connect (&act_pickall);
		item_picknothing.clicked.connect (&act_picknothing);
		item_erase.clicked.connect (&act_erase);
		item_origin.clicked.connect (&act_origin);
		item_grid.clicked.connect (&act_grid);
		item_reference.clicked.connect (&act_reference);
		item_offset.clicked.connect (&act_offset);
		item_extend.clicked.connect (&act_extend);
//		item_drag.clicked.connect (&act_drag);
		item_mirror.clicked.connect (&act_mirror);
		item_move.clicked.connect (&act_move);
		item_rotate.clicked.connect (&act_rotate);
		item_scale.clicked.connect (&act_scale);
		item_trim.clicked.connect (&act_trim);

		auto menu_settings = new Menu ("Settings", menubar);

		auto item_preferences = new MenuItem ("Preferences", menu_settings);
		auto item_page = new MenuItem ("Page", menu_settings);
		auto item_list = new MenuItem ("List", menu_settings);

		item_preferences.clicked.connect (&act_preferences);
		item_page.clicked.connect (&act_page);
		item_list.clicked.connect (&act_list);

		auto toolbar = new ToolBar (this);

		auto redraw_img      = new Image (WD_redraw_png);
		auto zoom_in_img     = new Image (WD_zoom_in_png);
		auto zoom_out_img    = new Image (WD_zoom_out_png);
		auto zoom_all_img    = new Image (WD_zoom_png);
		auto pan_left_img    = new Image (WD_pan_left_png);
		auto pan_right_img   = new Image (WD_pan_right_png);
		auto pan_up_img      = new Image (WD_pan_up_png);
		auto pan_down_img    = new Image (WD_pan_down_png);
		auto grid_half_img   = new Image (WD_grid_half_png);
		auto grid_double_img = new Image (WD_grid_double_png);

		auto gripmode_img                   = new Image (WD_gripmode_png);
		auto gripmode_pressed_img           = new Image (WD_gripmode_pressed_png);
		auto orthogonal_img                 = new Image (WD_orthogonal_png);
		auto orthogonal_pressed_img         = new Image (WD_orthogonal_pressed_png);
		auto snap_end_img                   = new Image (WD_snap_end_png);
		auto snap_end_pressed_img           = new Image (WD_snap_end_pressed_png);
		auto snap_middle_img                = new Image (WD_snap_middle_png);
		auto snap_middle_pressed_img        = new Image (WD_snap_middle_pressed_png);
		auto snap_center_img                = new Image (WD_snap_center_png);
		auto snap_center_pressed_img        = new Image (WD_snap_center_pressed_png);
		auto snap_cardinal_img              = new Image (WD_snap_cardinal_png);
		auto snap_cardinal_pressed_img      = new Image (WD_snap_cardinal_pressed_png);
		auto snap_nearest_img               = new Image (WD_snap_nearest_png);
		auto snap_nearest_pressed_img       = new Image (WD_snap_nearest_pressed_png);
		auto snap_intersection_img          = new Image (WD_snap_intersection_png);
		auto snap_intersection_pressed_img  = new Image (WD_snap_intersection_pressed_png);
		auto snap_perpendicular_img         = new Image (WD_snap_perpendicular_png);
		auto snap_perpendicular_pressed_img = new Image (WD_snap_perpendicular_pressed_png);
		auto snap_tangent_img               = new Image (WD_snap_tangent_png);
		auto snap_tangent_pressed_img       = new Image (WD_snap_tangent_pressed_png);
		auto select_layer_img               = new Image (WD_select_layer_png);
		auto select_view_img                = new Image (WD_select_view_png);
		auto select_block_img               = new Image (WD_select_block_png);

		auto tsize = IPair (20, 20);
		auto tool_redraw      = new ToolButton (redraw_img.scaledTo(tsize), toolbar);
		auto tool_zoom_in     = new ToolButton (zoom_in_img.scaledTo(tsize), toolbar);
		auto tool_zoom_out    = new ToolButton (zoom_out_img.scaledTo(tsize), toolbar);
		auto tool_zoom_all    = new ToolButton (zoom_all_img.scaledTo(tsize), toolbar);
		auto tool_pan_left    = new ToolButton (pan_left_img.scaledTo(tsize), toolbar);
		auto tool_pan_right   = new ToolButton (pan_right_img.scaledTo(tsize), toolbar);
		auto tool_pan_up      = new ToolButton (pan_up_img.scaledTo(tsize), toolbar);
		auto tool_pan_down    = new ToolButton (pan_down_img.scaledTo(tsize), toolbar);
		auto tool_grid_half   = new ToolButton (grid_half_img.scaledTo(tsize), toolbar);
		auto tool_grid_double = new ToolButton (grid_double_img.scaledTo(tsize), toolbar);


		_tool_gripmode           = new ToolButton (gripmode_img.scaledTo(tsize),
		                                           gripmode_pressed_img.scaledTo(tsize), toolbar);
		_tool_orthogonal         = new ToolButton (orthogonal_img.scaledTo(tsize),
		                                           orthogonal_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_end           = new ToolButton (snap_end_img.scaledTo(tsize),
		                                           snap_end_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_middle        = new ToolButton (snap_middle_img.scaledTo(tsize),
		                                           snap_middle_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_center        = new ToolButton (snap_center_img.scaledTo(tsize),
		                                           snap_center_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_cardinal      = new ToolButton (snap_cardinal_img.scaledTo(tsize),
		                                           snap_cardinal_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_nearest       = new ToolButton (snap_nearest_img.scaledTo(tsize),
		                                           snap_nearest_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_intersection  = new ToolButton (snap_intersection_img.scaledTo(tsize),
		                                           snap_intersection_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_perpendicular = new ToolButton (snap_perpendicular_img.scaledTo(tsize),
		                                           snap_perpendicular_pressed_img.scaledTo(tsize), toolbar);
		_tool_snap_tangent       = new ToolButton (snap_tangent_img.scaledTo(tsize),
		                                           snap_tangent_pressed_img.scaledTo(tsize), toolbar);

		_tool_select_layer = new ToolButton (select_layer_img.scaledTo(tsize), toolbar);
		_tool_select_view  = new ToolButton (select_view_img.scaledTo(tsize), toolbar);
		_tool_select_block = new ToolButton (select_block_img.scaledTo(tsize), toolbar);

		tool_redraw.clicked.connect      (&act_redraw);
		tool_zoom_in.clicked.connect     (&act_zoom_in);
		tool_zoom_out.clicked.connect    (&act_zoom_out);
		tool_zoom_all.clicked.connect    (&act_zoom_all);
		tool_pan_left.clicked.connect    (&act_pan_left);
		tool_pan_right.clicked.connect   (&act_pan_right);
		tool_pan_up.clicked.connect      (&act_pan_up);
		tool_pan_down.clicked.connect    (&act_pan_down);
		tool_grid_half.clicked.connect   (&act_grid_half);
		tool_grid_double.clicked.connect (&act_grid_double);

		_tool_gripmode.clicked.connect           (&act_gripmode);
		_tool_orthogonal.clicked.connect         (&act_orthogonal);
		_tool_snap_end.clicked.connect           (&act_snap_end);
		_tool_snap_middle.clicked.connect        (&act_snap_middle);
		_tool_snap_center.clicked.connect        (&act_snap_center);
		_tool_snap_cardinal.clicked.connect      (&act_snap_cardinal);
		_tool_snap_nearest.clicked.connect       (&act_snap_nearest);
		_tool_snap_intersection.clicked.connect  (&act_snap_intersection);
		_tool_snap_perpendicular.clicked.connect (&act_snap_perpendicular);
		_tool_snap_tangent.clicked.connect       (&act_snap_tangent);
		_tool_select_layer.clicked.connect       (&act_select_layer);
		_tool_select_view.clicked.connect        (&act_select_view);
		_tool_select_block.clicked.connect       (&act_select_block);

		_canvas  = new WDCanvas (this);
		_interpreter = new WDInterpreter (_canvas);

		_infobar = new HorizontalWidget(this);
//		_infobar.setAlignment (Align.LEFT);
		_infox = new Label ("x: ", _infobar);
		_infox.addFrame();
		_infox.setFixSize (8.em, 0);

		_infoy = new Label ("y: ", _infobar);
		_infoy.addFrame();
		_infoy.setFixSize (8.em, 0);

		_infogrid = new Label ("grid: ", _infobar);
		_infogrid.setMinSize (16.em, 0);
		_infogrid.addFrame();

		_console = new WDConsole (this);
		_console.setMaxSize (-1, 12.em);

		_status = new Label ("OK", this);
		_status.addFrame();
		_status.setMaxSize (-1, 0);

		_canvas.userWantsToQuit.connect    (&act_quit),
		_canvas.mousePressed.connect       (&writeToConsole);
		_canvas.mouseMoved.connect         (&updateMouse);
		_canvas.gridChanged.connect        (&updateGrid);
		_canvas.rightButtonPressed.connect (&_console.enter);
//		_canvas.escapeButton.connect (&_console.escape);
		_canvas.fileNameChanged.connect   (&setFileName);
		_canvas.drawingModified.connect   (&setModified);
		_canvas.snapModeChanged.connect   (&updateSnapAction);
		_canvas.orthogonalChanged.connect (&updateOrthogonalAction);

		_canvas.setGrid (_canvas.grid);			// dirty trick; updates _infogrid

		_console.lineChanged.connect (&callInterpreterTemporary);
		_console.lineEntered.connect (&callInterpreterFinal);
		_console.lineEscaped.connect (&lineEscaped);
		_console.shortcut.connect    (&shortcut);

		setFocusWidget (_console);

		_interpreter.notice.connect           (&_console.appendText);
		_interpreter.basepointChanged.connect (&_canvas.setBasepoint);
	}

	void callInterpreterTemporary()
	{
		if (!_final)
		{
			_interpreter.parse (_currenttext, true);
			_status.setCaption (_interpreter.hint());
			_interpreter.exetemp();
		}
	}

	void callInterpreterTemporary(const(char)[] line)
	{
		_currenttext = line;
		callInterpreterTemporary();
	}

	void callInterpreterFinal(const(char)[] line)
	{
		_final = true;
		if (!_interpreter.parse (line) ||
			!_interpreter.execute())
		{
			writeln ("Interpreter beeps!");
			App.beep();
		}
		_console.setLastCommand (_interpreter.lastCommand());
		_status.setCaption (_interpreter.hint());
		// because Cursor is already on a new line it has to be interpreted
		_final = false;
	}

	void lineEscaped(const(char)[] line)
	{
		_canvas.deselectAllItems();
	}


	void shortcut (ModifierKey m, KeySymbol s)
	{
		if      (s == 'a') _canvas.selectAllItems();
		else if (s == 'A') _canvas.deselectAllItems();
		else if (s == 's') _canvas.saveFile();

		else if (s == KeySymbol.DELETE) _canvas.eraseSelected();
	}

	void writeToConsole (DPair p)
	{
		_console.appendText (" " ~ p.x.to!string ~ "," ~ p.y.to!string ~ " ");
	}

	void updateMouse(DPair p)
	{
		callInterpreterTemporary ();
		_infox.setCaption ("x: " ~ p.x.to!string);
		_infoy.setCaption ("y: " ~ p.y.to!string);
	}

	void updateGrid (DPair g)
	{
		_infogrid.setCaption ("grid: " ~ g.x.to!string ~", " ~ g.y.to!string);
	}

	override void setCaption (const(char)[] text)
	{
		string caption = "WildCad";
		if (text != "") caption ~= " - " ~ text;

		super.setCaption(caption);
	}

	void setCaption ()
	{
		if (_modified) setCaption (_filename ~ " (modified)");
		else           setCaption (_filename);
	}

	void setFileName (cstring text)
	{
		_filename = text;
		setCaption ();
	}

	void setModified (bool m)
	{
		_modified = m;
		setCaption ();
	}

	void updateOrthogonalAction (bool o)
	{
		_tool_orthogonal.setPressed (o);
	}

	void updateSnapAction (WDItemSnap s)
	{
		_tool_snap_nearest.setPressed       ((s & WDItemSnap.NEAREST) != 0);
		_tool_snap_end.setPressed           ((s & WDItemSnap.END) != 0);
		_tool_snap_middle.setPressed        ((s & WDItemSnap.MIDDLE) != 0);
		_tool_snap_center.setPressed        ((s & WDItemSnap.CENTER) != 0);
		_tool_snap_perpendicular.setPressed ((s & WDItemSnap.CARDINAL) != 0);
		_tool_snap_intersection.setPressed  ((s & WDItemSnap.INTERSECTION) != 0);
		_tool_snap_perpendicular.setPressed ((s & WDItemSnap.PERPENDICULAR) != 0);
		_tool_snap_tangent.setPressed       ((s & WDItemSnap.TANGENT) != 0);
	}


// menu actions

	// : at beginning means start new command line (this is neccessary for all commands)

	void act_open () {_canvas.openFileDialog ();}
	void act_save () {_canvas.saveFile ();}
	void act_saveAs () {_canvas.saveFileDialog ();}
	void act_print () {_console.appendText (":print ");}
	void act_close () {_canvas.closeDrawing();}
	void act_quit ()
	{
		if (_canvas.closeDrawing()) App.quit();
	}

	void act_pickall ()
	{
		_canvas.selectAllItems();
	}

	void act_picknothing ()
	{
		_canvas.deselectAllItems();
	}

	void act_canvas () {_canvas.activateCanvas();}
	void act_zoom () {_console.appendText (":zoom ");}
	void act_pan () {_console.appendText (":pan ");}
	void act_measure () {_console.appendText (":measure ");}

	void act_cut () {_console.appendText (":cut ");}
	void act_copy () {_console.appendText (":copy ");}
	void act_paste () {_console.appendText (":paste ");}

	void act_arc () {_console.appendText (":arc ");}
	void act_curve () {_console.appendText (":curve ");}
	void act_circle () {_console.appendText (":circle");}
	void act_dimension () {_console.appendText (":dimension ");}
	void act_line () {_console.appendText (":line ");}
	void act_marker () {_console.appendText (":marker ");}
	void act_ray () {_console.appendText (":ray ");}
	void act_rectangle () {_console.appendText (":rectangle ");}
	void act_text () {_console.appendText (":text ");}
	void act_xline () {_console.appendText (":xline ");}

	void act_erase () {_console.appendText (":erase ");}
	void act_origin () {_console.appendText (":origin ");}
	void act_grid () {_console.appendText (":grid ");}
	void act_reference () {_console.appendText ("");}
	void act_offset () {_console.appendText (":offset ");}
	void act_extend () {_console.appendText (":extend ");}
//	void act_drag () {_console.appendText (":drag ");}
	void act_mirror () {_console.appendText (":mirror ");}
	void act_move () {_console.appendText (":move ");}
	void act_rotate () {_console.appendText (":rotate ");}
	void act_scale () {_console.appendText (":scale ");}
	void act_trim () {_console.appendText (":trim ");}

	void act_preferences () {_console.appendText ("");}
	void act_page () {_console.appendText ("");}
	void act_list () {_console.appendText (":list ");}

// toolbox actions

	void act_redraw ()     {_canvas.draw(true);}
	void act_zoom_in ()    {_canvas.zoomBy (sqrt(2.0));}
	void act_zoom_out ()   {_canvas.zoomBy (sqrt(0.5));}
	void act_zoom_all ()   {_canvas.zoomAll();}
	void act_pan_left ()   {_canvas.screenPan (-0.33, 0.0);}
	void act_pan_right ()  {_canvas.screenPan (+0.33, 0.0);}
	void act_pan_up ()     {_canvas.screenPan (0.0, +0.33);}
	void act_pan_down ()   {_canvas.screenPan (0.0, -0.33);}
	void act_grid_half()   {_canvas.setGrid (_canvas.grid/2);}
	void act_grid_double() {_canvas.setGrid (_canvas.grid*2);}

	void act_gripmode ()   {_canvas.setGripMode(_tool_gripmode.isPressed);}
	void act_orthogonal () {_canvas.setOrthogonal(_tool_orthogonal.isPressed);}
	void act_snap_end()           {_canvas.addSnapMode (WDItemSnap.END, _tool_snap_end.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_middle()        {_canvas.addSnapMode (WDItemSnap.MIDDLE, _tool_snap_middle.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_center()        {_canvas.addSnapMode (WDItemSnap.CENTER, _tool_snap_center.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_cardinal()      {_canvas.addSnapMode (WDItemSnap.CARDINAL, _tool_snap_cardinal.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_nearest()       {_canvas.addSnapMode (WDItemSnap.NEAREST, _tool_snap_nearest.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_intersection()  {_canvas.addSnapMode (WDItemSnap.INTERSECTION, _tool_snap_intersection.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_perpendicular() {_canvas.addSnapMode (WDItemSnap.PERPENDICULAR, _tool_snap_perpendicular.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_snap_tangent()       {_canvas.addSnapMode (WDItemSnap.TANGENT, _tool_snap_tangent.isPressed?WDFlagOp.ADD:WDFlagOp.DEL);}
	void act_select_layer () {_console.appendText ("");}
	void act_select_view ()  {_console.appendText ("");}
	void act_select_block () {_console.appendText ("");}
}
