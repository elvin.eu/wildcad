import std.conv;
import std.math: PI;

import cairo;
import wcw;

import auxiliary;
import settings;
import painter;
import enums;
import item;

class WDXLine : WDItem
{
private:
	DPair _start, _direction;

public:
	this (DPair a, DPair d)
	{
		if (a.isReal()) _start = a; else _start = DPair (0.0, 0.0);
		if (d.isReal && (d != DPair(0.0, 0.0))) _direction = d; else _direction = DPair(1.0, 0.0);
	}

	this (DPair a, double d)
	{
		if (a.isReal()) _start = a; else _start = DPair (0.0, 0.0);
		if (d.isReal()) _direction = vector (rad(d)); else _direction = DPair(1.0, 0.0);

	}

	this (const WDXLine l)
	{
	  _start    = l._start;
	  _direction = l._direction;
	}

	override WDItem clone () {return new WDXLine (this);}

	DPair start () const {return _start;}
	DPair direction () const {return _direction;}

	override string toString () const
	{
		return "xline @c1 "~ pretty(_start) ~
			" @cv " ~ pretty (_direction.unit);

	}

	override void draw (Painter p, CairoMatrix matrix, double density)
	{
		CairoMatrix inverted = matrix.inverted;
		// get viewport size in world coordinates

		double l, t, r, b;

		inverted.transform_point (0.0, 0.0, l, t);
		inverted.transform_point (p.widget.size.x, p.widget.size.y, r, b);

		double x1, y1, x2, y2;
		if (_direction.y == 0) // xline horizontal?
		{
			x1 = l;
			y1 = y2 = _start.y;
			x2 = r;
		}
		else if (_direction.x == 0) // xline vertical?
		{
			x1 = x2 = _start.x;
			y1 = b;
			y2 = t;
		}
		else // neither nor
		{
			double xa, xb, yc, yd;

			// fit line into horizontal borders
			xa = _start.x + (_direction.x/_direction.y)*(b-_start.y);
			xb = _start.x+(_direction.x/_direction.y)*(t-_start.y);

			// fit line into vertical borders
			yc = _start.y+(_direction.y/_direction.x)*(l-_start.x);
			yd = _start.y+(_direction.y/_direction.x)*(r-_start.x);

			// take those endpoints, that are on the viewport border and draw the line
			// first check possibilities for x1,y1

			// does line touch lower horizontal border?
			if      (l<=xa && xa<=r) {x1 = xa; y1 =  b;}
			// does it touch the upper?
			else if (l<=xb && xb<=r) {x1 = xb; y1 =  t;}
			else                     {x1 =  l; y1 = yc;}

			// then check for x2,y2
			// does it touch the right border?
			if      (b<=yd && yd<=t) {x2 =  r; y2 = yd;}
			// or the left one?
			else if (b<=yc && yc<t)  {x2 =  l; y2 = yc;}
			else                     {x2 = xb; y2 =  t;}
		}

		matrix.transform_point (x1, y1, x1, y1);
		matrix.transform_point (x2, y2, x2, y2);
		p.drawLine (x1, y1, x2, y2);
	}

	override bool snapsTo (WDItemSnap s) const
	{
		return ((s & (WDItemSnap.NEAREST|WDItemSnap.INTERSECTION|WDItemSnap.INTERSECTION|WDItemSnap.PERPENDICULAR)) != 0);
	}

	override WDTuple!(DPair, double) snapToItem (WDItemSnap snapmode, DPair pointer, DPair from) const
	{
		double dist = double.infinity;
		DPair snap;

		if (snapmode & (WDItemSnap.NEAREST | WDItemSnap.INTERSECTION))
		{
			snap = xnear (_start, _direction, pointer);
			dist = pdist (snap, pointer);
		}
		else
		{
			if (snapmode & WDItemSnap.PERPENDICULAR)
			{
				snap = xnear(_start, _direction, from);
				dist = pdist(snap, pointer);
			}
		}
		return wdTuple (snap, dist);
	}


	override bool inWindow (DRect r, bool entirely) const
	{
		if (entirely == false)
		{
			DPair p1 = xnear (_start, _direction, DPair (r.x0, r.y0));
			DPair p2 = xnear (_start, _direction, DPair (r.x0, r.y1));
			DPair p3 = xnear (_start, _direction, DPair (r.x1, r.y1));
			DPair p4 = xnear (_start, _direction, DPair (r.x1, r.y0));

			if (r.contains (p1) || r.contains (p2) || r.contains (p3) || r.contains (p4))
			{
				return true;
			}
		}
		return false;
	}

   	override DRect extents () const
	{
		return DRect (_start, 0, 0);
	}

	override bool encloses (DPair p) const
	{
		if (!p.isReal()) return false;

		if (circa (xnear (_start, _direction, p), p)) return true;
		else                                           return false;
	}

//	DPair[] trimmingPoints() const;

	override void move   (DPair d)
	{
		if (!d.isReal()) return;

		if (!d.isReal()) return;
		_start += d;
	}

	override void mirror (DPair a, DPair d)
	{
		if (!a.isReal()) return;
		if (!d.isReal()) return;

		auto start = 2*xnear (a, d, _start) - _start;
		auto dir = vector(normalize!PI(2*arg(d)-arg(_direction)));

		if (!start.isReal || !dir.isReal()) return;

		_start = start;
		_direction = dir;
	}

	override void rotate (DPair c, double a)
	{
		if (!c.isReal()) return;
		if (!a.isReal()) return;

		_start = c + auxiliary.rotate(vector (c, _start), rad(a));
		_direction = vector(normalize!PI(rad(a)+arg(_direction)));
	}

	override void scale  (DPair c, double s)
	{
		if (!c.isReal()) return;
		if (!s.isReal()) return;

		_start  = (_start-c)*s + c;
	}

	override bool offset  ( double d, DPair s)
	{
		if (!d.isReal()) return false;
		if (!s.isReal()) return false;

	    DPair p = xnear  (_start, _direction, s);
		DPair v = vector (p, s).unit * d;
		if (!v.isReal()) return false;

		move (v);
		return true;
	}

//	bool fit (DPair from, DPair to);

}
